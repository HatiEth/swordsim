/*
 * main.cpp
 *
 *  Created on: 31.10.2013
 *      Author: Hati
 */

#include "Utility.h"
#include "window/Window.h"
#include "ErrorFileLogger.h"
#include <Box2D/Box2D.h>
#include <render/Shader.h>
#include <string.h>
#include <components/Component.h>
#include <components/HealthComponent.h>

#include <libpng/png.h>
#include <zlib/zlib.h>


int main(int argc, char** argv) {



	eth::ErrorFileLogger logger("logfile.txt");
	KE_SetErrorLogger(&logger);

	Window w;
	w.create(argc, argv);






	unsigned char header[8];
	png_sig_cmp(header, 0, 0);


//	eth::ShaderHandle handle = eth::createShader("orthogonal.frag");
//	eth::ShaderHandle handle2 = eth::createShader("orthogonal.vert");
//	// do something here
//	eth::accessShader(handle);
//	eth::accessShader(handle2);
//
//	eth::deleteShader(handle2);
//	eth::deleteShader(handle);
//
//
//	eth::ShaderHandle handle3 = eth::createShader("base.vert");
//	eth::accessShader(handle3);

	w.start();


	return 0;
}


