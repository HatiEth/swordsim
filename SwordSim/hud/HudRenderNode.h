/*
 * HudRenderNode.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef HUDRENDERNODE_H_
#define HUDRENDERNODE_H_

#include <render/Texture.h>
#include <entity/Entity.h>
#include <components/TransformComponent.h>


struct HudType {
	enum EType {
		Bar, Text
	};
};

/**
 * Contains informations on how to draw hud specific data
 */
struct HudRenderNode {
	HudType::EType _type;
	Texture _texture;
	int& _value;
	EntityInfo& _entity;
	TransformComponent* _transform;
	bool _relativeTransform;

	HudRenderNode(HudType::EType type, Texture tex, int& watchValue, EntityInfo& watchedEntity, TransformComponent* transform, bool relativeTransform)
		: _type(type), _texture(tex), _value(watchValue), _entity(watchedEntity), _transform(transform), _relativeTransform(relativeTransform)
	{
	}
};

#endif /* HUDRENDERNODE_H_ */
