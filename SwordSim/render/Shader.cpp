/*
 * Shader.cpp
 *
 *  Created on: Nov 2, 2013
 *      Author: Hati
 */

#include "Shader.h"
#include <Memory.h>

GLenum parseShaderType(const char* filePath) {
	char* ext = std::strrchr(filePath, '.');
	KE_Assert(ext != nullptr, "Extension failure, no file extension found");
	++ext; //increment by 1 to skip '.'
	if (eth::fastStrEq(ext, "frag")) {
		return GL_FRAGMENT_SHADER;
	}
	if (eth::fastStrEq(ext, "vert")) {
		return GL_VERTEX_SHADER;
	}
	if (eth::fastStrEq(ext, "geom")) {
		return GL_GEOMETRY_SHADER;
	}
	KE_Assert(false,
			"This part of code is unreachable, something went incredible wrong");
	return GLenum(0);
//	return (void)nullptr; // fix return warning
}

namespace eth {

/**
 * @todo On refactoring, increase cache-performance
 */
struct glStateInformation {
	typedef std::tr1::unordered_map<GLuint, eth::StringMap<GLint>::Type> UniformMap;
	typedef std::tr1::unordered_map<GLuint, eth::StringMap<GLint>::Type> AttributeMap;

	/// pre-cached uniform & attribute locations
	UniformMap _uniformLocations;
	AttributeMap _attributeLocations;

	/// hold's handles and GL internal id's of shaders

} g_glState;

///@todo No cached file name here, yet
const ShaderHandle createShader(const char* filePath) {
	File file;
	readFile(filePath, file); ///@todo buffer read to avoid memory allocation every creation here

	GLenum shaderType = parseShaderType(filePath);

	// memPtr -> indexOffset
	ShaderHandle handle;

	GLuint shaderId = glCreateShader(shaderType);

	glShaderSource(shaderId, 1, (const GLchar**) &file.content, NULL);
	glCompileShader(shaderId);

	GLint success = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

		///@todo use placement memory allocation here later
		char* log = new char[maxLength];
		glGetShaderInfoLog(shaderId, maxLength, &maxLength, (GLchar*) log);

		KE_Notify("Failed to compile shader(%s): %s", filePath, log);

		delete[] log; ///@bug memory fragmentation
	}

	delete[] file.content;
	delete[] file.path;

	handle._id = shaderId;

	return handle;
}

void deleteShader(const ShaderHandle& shader) {
	glDeleteShader(shader._id);

}

const ProgramHandle createProgram() {
	ProgramHandle handle;

	handle._id = glCreateProgram();

	return handle;
}

void deleteProgram(const ProgramHandle& program) {
	glDeleteProgram(program._id);
}

GLint programAttributeLocation(const ProgramHandle& program,
		const char* attributeName, bool forceUpdate) {
	GLint returnLocation = -1;
	// hard update!
	if (forceUpdate) {
		returnLocation = glGetAttribLocation(program._id, attributeName);

		g_glState._attributeLocations[program._id][attributeName] =
				returnLocation;
	} else {
		// caching here..
		// attrib cache knows program?
		auto _internal0 = g_glState._attributeLocations.find(
				program._id);

		bool noListedProgram = _internal0
				== g_glState._attributeLocations.end();

		if (noListedProgram) {
			returnLocation = glGetAttribLocation(program._id, attributeName);

			g_glState._attributeLocations[program._id][attributeName] =
					returnLocation;

		} else {
			// program known
			auto _internal = _internal0->second.find(attributeName);
			bool noListedAttrib = _internal == _internal0->second.end();
			if (noListedAttrib) {
				returnLocation = glGetAttribLocation(program._id,
						attributeName);

				g_glState._attributeLocations[program._id][attributeName] =
						returnLocation;

			} else {
				returnLocation = _internal->second;
			}
		}
	}
	KE_Assert(returnLocation != -1, "Failure in attribute lookup (%s, %d)!",
			attributeName, program._id);
	return returnLocation;
}
///@todo better structure.. total mess here
///@todo increase performance - heavy table lookup
GLint programUniformLocation(const ProgramHandle& program,
		const char* uniformName, bool forceUpdate) {
	GLint returnLocation = -1;
	// hard update!
	if (forceUpdate) {
		returnLocation = glGetUniformLocation(program._id, uniformName);

		g_glState._uniformLocations[program._id][uniformName] =
				returnLocation;
	} else {
		// caching here..
		// uniform cache knows program?
		auto pIdIt = g_glState._uniformLocations.find(program._id);

		bool noListedProgram = pIdIt
				== g_glState._uniformLocations.end();

		if (noListedProgram) {
			returnLocation = glGetUniformLocation(program._id, uniformName);

			g_glState._uniformLocations[program._id][uniformName] =
					returnLocation;

		} else {
			// program known
			auto _internal = pIdIt->second.find(uniformName);
			bool noListedUniform = _internal == pIdIt->second.end();
			if (noListedUniform) {
				returnLocation = glGetUniformLocation(program._id, uniformName);

				g_glState._uniformLocations[program._id][uniformName] =
						returnLocation;

			} else {
				returnLocation = _internal->second;
			}
		}
	}
	if (returnLocation == -1) {
		KE_CustomWarning("Failure in uniform %s (%d) lookup!", uniformName,
				program._id);
	}
	return returnLocation;
}

void linkProgram(const ProgramHandle& program) {
	glLinkProgram(program._id);

	GLint success = 0;
	glGetProgramiv(program._id, GL_LINK_STATUS, &success);
	if (success == GL_FALSE) {
		GLint maxLength = 0;
		glGetProgramiv(program._id, GL_INFO_LOG_LENGTH, &maxLength);

		///@todo use placement memory allocation here later
		char* log = new char[maxLength];
		glGetProgramInfoLog(program._id, maxLength, &maxLength, (GLchar*) log);

		KE_Notify("Failed to link program(%d): %s", program._id, log);

		delete[] log; ///@bug memory fragmentation
	}
}

void attachShader(const ProgramHandle& program, const ShaderHandle& shader) {
	glAttachShader(program._id, shader._id);
}



void freeProgram(const ProgramHandle& prog) {
	KE_Notify("Freeing program %u: ", prog._id);
	GLint attachedShaderCount = 0;
	glGetProgramiv(prog._id, GL_ATTACHED_SHADERS, &attachedShaderCount);
	GLuint attachedShaders[attachedShaderCount];
	glGetAttachedShaders(prog._id, attachedShaderCount, 0, attachedShaders);

	for(int i=0;i<attachedShaderCount;++i) {
		KE_Notify(" Flagged shader %u for removal", attachedShaders[i]);
		glDeleteShader(attachedShaders[i]);
	}

}

void enableProgram(const ProgramHandle& program) {
	glUseProgram(program._id);
}

} /* namespace eth */

