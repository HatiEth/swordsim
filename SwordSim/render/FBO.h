/*
 * FBO.h
 *
 *  Created on: 07.11.2013
 *      Author: Hati
 */

#ifndef FBO_H_
#define FBO_H_
#include "Texture.h"
#include <glm/glm.hpp>


struct SingleTextureFBO {
	GLuint fboId;
	GLuint textureId;
	GLuint depthStencilId;
};

SingleTextureFBO createFBO();
void clearFBO(SingleTextureFBO& fbo, const glm::vec4& clearColor);

template<size_t texturesPerBuffer>
struct DoubleBufferedFBO {
	/// Single fbo's
	GLuint buffers[2];
	/// Textures per FBO
	GLuint textures[texturesPerBuffer*2];
	/// FBO Stencil+Depth Buffers
	GLuint depthStencilId[2];
	/// current active buffer
	size_t activeBuffer;
};

template<size_t texturesPerBuffer>
void createDoubleBuffer(DoubleBufferedFBO<texturesPerBuffer>& doubleBufferedFBO);

template<size_t texturesPerBuffer>
void swapDoubleBuffer(DoubleBufferedFBO<texturesPerBuffer>& doubleBufferedFBO);

template<size_t texturesPerBuffer>
void clearDoubleBuffer(DoubleBufferedFBO<texturesPerBuffer>& doubleBufferedFBO,
		const glm::vec4& clearColor);

#include "DoubleBufferedFBO.h"

#endif /* FBO_H_ */
