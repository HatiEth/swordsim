/*
 * Texture.h
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <gl/glew.h>

struct Texture {
	GLuint textureId;
	int width, height;
};

#define DEFAULT_INTERNAL_FORMAT	GL_RGBA8
#define DEFAULT_INTERNAL_TYPE	GL_UNSIGNED_BYTE

struct TextureInfo {
	int width{800}, height{600};
	GLint internalFormat{DEFAULT_INTERNAL_FORMAT};
	GLint internalType{GL_UNSIGNED_BYTE};
};

Texture createTexture(const TextureInfo& texInfo = TextureInfo{});
Texture loadTextureFromPNG(const char* path, const TextureInfo& texInfo = TextureInfo{});
void fillTextureFromPNG(Texture& tex, const char* path, GLint internalFormat = DEFAULT_INTERNAL_FORMAT, GLint type = DEFAULT_INTERNAL_TYPE);

void setTextureData(Texture& tex, char* texData, GLint internalFormat = GL_RGBA8, GLint type = GL_UNSIGNED_BYTE);

namespace eth {
	void bindTexture2d(int texSlot, GLuint tex);
	void unbindTexture2d(int texSlot);
} /* eth */
#endif /* TEXTURE_H_ */
