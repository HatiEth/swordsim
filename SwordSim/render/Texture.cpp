/*
 * Texture.cpp
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#include "Texture.h"
#include <libpng/png.h>
#include <zlib/zlib.h>
#include <Utility.h>

Texture createTexture(const TextureInfo& texInfo) {
	Texture tex;
	memset(&tex, 0, sizeof(Texture));
	tex.height = texInfo.height;
	tex.width = texInfo.width;
	glGenTextures(1, &tex.textureId);
	glBindTexture(GL_TEXTURE_2D, tex.textureId);
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, texInfo.internalFormat, texInfo.width,
				texInfo.height, 0,
				GL_RGBA, texInfo.internalType, NULL);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	return tex;
}

Texture loadTextureFromPNG(const char* path, const TextureInfo& texInfo) {

	Texture tex = createTexture(texInfo);

	png_image image;
	memset(&image, 0, sizeof(png_image));
	image.version = PNG_IMAGE_VERSION;

	int err = !png_image_begin_read_from_file(&image, path);

	if (err || image.warning_or_error != 0) {
		KE_Notify("libpng error occured: %s", image.message);
	}

	image.format = PNG_FORMAT_RGBA;
	char* data = new char[PNG_IMAGE_SIZE(image)];

	err = !png_image_finish_read(&image, NULL, data,
			-PNG_IMAGE_ROW_STRIDE(image), NULL);
	if (err || image.warning_or_error != 0) {
		KE_Notify("libpng error occured: %s", image.message);
	}

	tex.width = image.width;
	tex.height = image.height;
	setTextureData(tex, data, texInfo.internalFormat, texInfo.internalType);

	KE_Notify("Loaded png %s (%u x %u)", path, tex.width, tex.height);

	delete[] data;

	return tex;
}

void setTextureData(Texture& tex, char* texData,
		GLint internalFormat/*= GL_RGBA8*/, GLint type/*=GL_UNSIGNED_BYTE*/) {
	KE_Assert(tex.textureId != 0, "TextureId invalid!");
	glBindTexture(GL_TEXTURE_2D, tex.textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex.width, tex.height, 0,
	GL_RGBA, GL_UNSIGNED_BYTE, texData);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void fillTextureFromPNG(Texture& tex, const char* path, GLint internalFormat,
		GLint type) {
	png_image image;
	memset(&image, 0, sizeof(png_image));
	image.version = PNG_IMAGE_VERSION;

	int err = !png_image_begin_read_from_file(&image, path);

	if (err || image.warning_or_error != 0) {
		KE_Notify("libpng error occured: %s", image.message);
	}

	image.format = PNG_FORMAT_RGBA;
	char* data = new char[PNG_IMAGE_SIZE(image)];

	err = !png_image_finish_read(&image, NULL, data,
			-PNG_IMAGE_ROW_STRIDE(image), NULL);
	if (err || image.warning_or_error != 0) {
		KE_Notify("libpng error occured: %s", image.message);
	}

	tex.width = image.width;
	tex.height = image.height;
	setTextureData(tex, data, internalFormat, type);

	KE_Notify("Filled texture (%u) png %s (%u x %u)", tex.textureId, path,
			tex.width, tex.height);

	delete[] data;
}

void eth::bindTexture2d(int texSlot, GLuint texture) {
	KE_Assert(texture != 0, "Invalid opengl texture!");
	glActiveTexture(GL_TEXTURE0+texSlot);
	glBindTexture(GL_TEXTURE_2D, texture);
}

void eth::unbindTexture2d(int texSlot) {
	glActiveTexture(GL_TEXTURE0+texSlot);
	glBindTexture(GL_TEXTURE_2D, 0);
}
