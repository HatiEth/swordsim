/*
 * Box2DDebugDraw.cpp
 *
 *  Created on: Nov 16, 2013
 *      Author: Hati
 */

#include "Box2DDebugDraw.h"
#include <GL/glew.h>
#include <Utility.h>

void Box2DDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount,
		const b2Color& color) {
	glColor4f(color.r, color.g, color.b, .50f);

	glBegin(GL_LINE_LOOP); {
		for(int i=0;i<vertexCount;++i) {
			glVertex2fv((GLfloat*)&vertices[i]);
		}
	} glEnd();
}

void Box2DDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount,
		const b2Color& color) {
	glColor4f(color.r*.5f, color.g*.5f, color.b *.5f, .70f);
	glBegin(GL_TRIANGLE_FAN); {
		for(int i=0;i<vertexCount;++i) {
			glVertex2fv((GLfloat*)&vertices[i]);
		}
	} glEnd();

	DrawPolygon(vertices, vertexCount, color);
}

void Box2DDebugDraw::DrawCircle(const b2Vec2& center, float32 radius,
		const b2Color& color) {
	const float32 k_segments = 16.0f;
	const float32 k_increment = 2.0f * b2_pi / k_segments;
	float32 theta = 0.0f;
	glColor4f(color.r, color.g, color.b, .50f);
	glBegin(GL_LINE_LOOP);
	for (int32 i = 0; i < k_segments; ++i) {
		b2Vec2 v = center + radius * b2Vec2(cosf(theta), sinf(theta));
		glVertex2f(v.x, v.y);
		theta += k_increment;
	}
	glEnd();
}

void Box2DDebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius,
		const b2Vec2& axis, const b2Color& color) {
	glColor4f(color.r, color.g, color.b, .70f);

	const float32 k_segments = 16.0f;
	const float32 k_increment = 2.0f * b2_pi / k_segments;
	float32 theta = 0.0f;

	glColor4f(0.5f * color.r, 0.5f * color.g, 0.5f * color.b, 0.5f);
	glBegin(GL_TRIANGLE_FAN);
	for (int32 i = 0; i < k_segments; ++i) {
		b2Vec2 v = center + radius * b2Vec2(cosf(theta), sinf(theta));
		glVertex2f(v.x, v.y);
		theta += k_increment;
	}
	glEnd();

	DrawCircle(center, radius, color);

	b2Vec2 p = center + radius * axis;
	glBegin(GL_LINES);
	glVertex2f(center.x, center.y);
	glVertex2f(p.x, p.y);
	glEnd();
}

void Box2DDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2,
		const b2Color& color) {
	glColor4f(color.r, color.g, color.b, .70f);
	glBegin(GL_LINES); {
		glVertex2fv((GLfloat*)&p1);
		glVertex2fv((GLfloat*)&p2);
	} glEnd();
}

void Box2DDebugDraw::DrawTransform(const b2Transform& xf) {
	b2Vec2 p1 = xf.p, p2;
	const float32 k_axisScale = 0.4f;
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(p1.x, p1.y);
	p2 = p1 + k_axisScale * xf.q.GetXAxis();
	glVertex2f(p2.x, p2.y);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(p1.x, p1.y);
	p2 = p1 + k_axisScale * xf.q.GetYAxis();
	glVertex2f(p2.x, p2.y);

	glEnd();
}
