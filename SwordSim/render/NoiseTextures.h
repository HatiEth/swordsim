/*
 * Noise.h
 *
 *  Created on: 26.12.2013
 *      Author: Hati
 */

#ifndef NOISETEXTURES_H_
#define NOISETEXTURES_H_
#include <random>
#include "Texture.h"

void generateWhiteNoise(Texture tex);
void generateColorNoise(Texture tex);
void generateFloatNoise(Texture tex);

#endif /* NOISETEXTURES_H_ */
