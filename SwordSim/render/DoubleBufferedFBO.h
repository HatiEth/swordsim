/*
 * DoubleBufferedFBO.h
 *
 *  Created on: 12.11.2013
 *      Author: Hati
 */
#ifndef DOUBLEBUFFERFBO_H_
#define DOUBLEBUFFERFBO_H_
#include "FBO.h"
#include <Utility.h>

template<size_t texturesPerBuffer>
static inline void createDoubleBuffer(
		DoubleBufferedFBO<texturesPerBuffer>& dbfbo) {
//	KE_Assert( sizeof(dbfbo.textures)/sizeof(GLuint) ==  )
	KE_Assert(texturesPerBuffer <= 15, "Maximum of color attachment reached!");

	dbfbo.activeBuffer = 0u;

	glGenTextures(texturesPerBuffer * 2, dbfbo.textures);

	glGenFramebuffers(2, dbfbo.buffers);
	glGenRenderbuffers(2, dbfbo.depthStencilId);

	// generate textures
	for (int i = 0; i < texturesPerBuffer * 2; ++i) {
		glBindTexture(GL_TEXTURE_2D, dbfbo.textures[i]);
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 800, 600, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	for (int i = 0; i < 2; ++i) {
		glBindRenderbuffer(GL_RENDERBUFFER, dbfbo.depthStencilId[i]);
		{
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800,
					600);
		}
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		GLenum err = glGetError();
		if (err != GL_NO_ERROR) {
			KE_Notify("[%s:%d] %s", __FILE__, __LINE__,
					(const char* )glewGetErrorString(err));
		}
	}

	for (int i = 0; i < 2; ++i) {

		glBindFramebuffer(GL_FRAMEBUFFER, dbfbo.buffers[i]);
		{
			for (int a = 0; a < texturesPerBuffer; ++a) {
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + a,
				GL_TEXTURE_2D, dbfbo.textures[i * texturesPerBuffer + a], 0);
			}
			GLenum err = glGetError();
			if (err != GL_NO_ERROR) {
				KE_Notify("[%s:%d] %s", __FILE__, __LINE__,
						(const char* )glewGetErrorString(err));
			}

			glFramebufferRenderbuffer(GL_FRAMEBUFFER,
					GL_DEPTH_STENCIL_ATTACHMENT,
					GL_RENDERBUFFER, dbfbo.depthStencilId[i]);
			err = glGetError();
			if (err != GL_NO_ERROR) {
				KE_Notify("[%s:%d] %s", __FILE__, __LINE__,
						(const char* )glewGetErrorString(err));
			}

			GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

			KE_Assert(status == GL_FRAMEBUFFER_COMPLETE, "%s (%d) [%d]",
					glewGetErrorString(status), status, i);

			glClearColor(0, 0, 0, 0);
			glClear(
					GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT
							| GL_STENCIL_BUFFER_BIT);

		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

template<size_t texturesPerBuffer>
static inline void swapDoubleBuffer(DoubleBufferedFBO<texturesPerBuffer>& db) {
	db.activeBuffer = (db.activeBuffer + 1) % 2;
}

template<size_t texturesPerBuffer>
static inline void clearDoubleBuffer(DoubleBufferedFBO<texturesPerBuffer>& dbf,
		const glm::vec4& clearColor) {
	for (int i = 0; i < 2; ++i) {
		glBindFramebuffer(GL_FRAMEBUFFER, dbf.buffers[i]);
		glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

#endif /* DOUBLEBUFFERFBO_H_ */
