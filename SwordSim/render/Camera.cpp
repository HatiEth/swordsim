#include "Camera.h"

Camera::Camera(void) :
		m_viewPort(0, 0), m_zoomFactor(0.0f) {

}

Camera::~Camera(void) {
}

KECode Camera::update(float dt) {
        float pixelZoom = (m_zoomFactor * 0.5f) * m_viewPort.x;
        glm::vec2 aspectedZoom = m_aspectRatio * pixelZoom;
	float d = m_viewPort.x / 2;

	float absoluteXZoom = m_zoomFactor * d;

	glm::mat4 newMat = glm::ortho<float>(0 + absoluteXZoom,
			m_viewPort.x - absoluteXZoom, 0 + absoluteXZoom * m_aspectRatio.y,
			m_viewPort.y - absoluteXZoom * m_aspectRatio.y);

//	glm::mat4 newMat = glm::ortho<float>(0,
//			m_viewPort.x, 0,
//			m_viewPort.y);

	newMat = glm::translate<float>(newMat, m_viewPort.x / 2, m_viewPort.y / 2,
			0);

	m_ViewProjMat = newMat;

	return KE_SUCCESS;
}

KECode Camera::zoom(float z) {
	//KE_Notify("Zooming!");
	// Can zoom -200% out and max 100% in (1px = fullscreen)
	m_zoomFactor = glm::clamp<float>(m_zoomFactor + z, -2,
			1.f - CAM_ZOOM_EPSILON);
//        m_zoomFactor+=z;
	KE_Notify("Camera zoom at %3.2f", m_zoomFactor);
	return KE_SUCCESS;
}

