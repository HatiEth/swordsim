/*
 * Noise.cpp
 *
 *  Created on: 26.12.2013
 *      Author: Hati
 */

#include <gl/glew.h>
#include <Utility.h>

#include <render/NoiseTextures.h>

///@todo placement new for generation, max_texture size (2000*2000*4 byte)?

static char noiseTexDataBlob[2000*2000*4];

void generateWhiteNoise(Texture tex) {
	using ColorInt = unsigned int;
	const size_t picSize = tex.width* tex.height;


	int particleCount = 100;
	ColorInt* randomData = new (noiseTexDataBlob) ColorInt[picSize];
	for (size_t i = 0; i < picSize; ++i) {
		unsigned int data = rand() % 256;
		randomData[i] = data | (data << 8) | (data << 16);
		randomData[i] |= 0xff000000;
	}

	glBindTexture(GL_TEXTURE_2D, tex.textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex.width, tex.height, 0,
	GL_RGBA, GL_UNSIGNED_BYTE, randomData);
}

void generateColorNoise(Texture tex) {
	using ColorInt = unsigned int;
	const size_t picSize = tex.width * tex.height;

	ColorInt* randomData = new (noiseTexDataBlob) ColorInt[picSize];
	for (size_t i = 0; i < picSize; ++i) {
		unsigned int data = rand() % 255; // MAX_RAND = 32767
		randomData[i + 0] = data;
		data = rand() % 255;
		randomData[i] |= data << 8;
		data = rand() % 255;
		randomData[i] |= data << 16;
		randomData[i] |= 0xff000000;
	}
	glBindTexture(GL_TEXTURE_2D, tex.textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex.width, tex.height, 0,
	GL_RGBA, GL_UNSIGNED_BYTE, randomData);

}

void generateFloatNoise(Texture tex) {
	const size_t picSize = tex.width * tex.height * 4;


	// signed float field
	float* randomData = new (noiseTexDataBlob) float[picSize];
	for (size_t i = 0; i < picSize * 4; i += 4) {
		float data = float((rand() / 32767.0f) - 0.5f) * 2.0f; // MAX_RAND = 32767
		randomData[i + 0] = data;
		data = float((rand() / 32767.0f) - 0.5f) * 2.0f; // MAX_RAND = 32767
		randomData[i + 1] = data;
		data = float((rand() / 32767.0f) - 0.5f) * 2.0f; // MAX_RAND = 32767
		randomData[i + 2] = data;
		randomData[i + 3] = 1.0f;
	}

	glBindTexture(GL_TEXTURE_2D, tex.textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex.width, tex.height, 0,
	GL_RGBA, GL_FLOAT, randomData);

	delete[] randomData;
}
