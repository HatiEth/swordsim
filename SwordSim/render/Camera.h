/*
 * SceneGraph.h
 *
 *  Created on: Jul 9, 2013
 *      Author: Hati
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "../Utility/ErrorHandling/ErrorHandling.h"

#define CAM_ZOOM_EPSILON 0.001f

/**	@brief Implements a simple (0,0) focused camera
 *** @details
 *** @todo implement proper zoom functionality
 **/
class Camera {
public:
	Camera(void);
	~Camera(void);

	/**	@brief Resizes view port for this camera updating internal used zoom
	 *** @details
	 *** @param
	 *** @return
	 **/
	KECode resize(int w, int h) {
		m_aspectRatio = glm::vec2(1, float(h) / w);
		m_viewPort = glm::vec2(w, h);

		return KE_SUCCESS;
	}

	KECode setZoom(float procentualVal) {
		m_zoomFactor = glm::clamp<float>(procentualVal, -2,
				1.f - CAM_ZOOM_EPSILON);
		return KE_SUCCESS;
	}

	KECode zoom(float z);
	float zoom() {
		return m_zoomFactor;
	}
	inline const float* valuePtr() const {
		return glm::value_ptr(m_ViewProjMat);
	}



	glm::vec2 transformWindowIntoCamSpace(const glm::vec2& p) {

		return (p - m_viewPort * .50f)
				* glm::vec2(1 - m_zoomFactor, -1 + m_zoomFactor);
	}

	KECode update(float dt);

	glm::vec2 viewport() {
		return m_viewPort;
	}

	glm::vec2 applyAspectRatio(const glm::vec2& direction) {
		return direction * m_aspectRatio;
	}

	float aspectRatio() const {
		return m_viewPort.x / m_viewPort.y;
	}

	glm::mat4 m_ViewProjMat;

	glm::vec2 m_aspectRatio;
	glm::vec2 m_viewPort;

	/// procentual value [-2.0,1.0] - default 0
	float m_zoomFactor;
};

#endif /* CAMERA_H_ */
