/*
 * Shader.h
 *
 *  Created on: Nov 2, 2013
 *      Author: Hati
 */

#ifndef SHADER_H_
#define SHADER_H_

#include <Utility.h>
#include <GL/glew.h>

namespace eth {

template<typename T>
struct glHandle {
	T _id;
};

/** Handle for a single shader object
 *
 **/
struct ShaderHandle;
struct ProgramHandle;

void freeProgram(const ProgramHandle&);

void attachShader(const ProgramHandle&, const ShaderHandle&);
/** Create a single shader from file **/
const ShaderHandle createShader(const char* filePath);

/** Delete a single shader where file was used to create **/
void deleteShader(const ShaderHandle& shader);

class ShaderHandle : protected eth::glHandle<GLuint> {
	friend const ShaderHandle createShader(const char*);
	friend void deleteShader(const ShaderHandle&);
	friend void attachShader(const ProgramHandle&, const ShaderHandle&);
};


/*** Program related stuff ***/

/** create a single shader program
 * @todo maybe add named programs?
**/
const ProgramHandle createProgram();

/** deletes a single shader program **/
void deleteProgram(const ProgramHandle& program);
GLint programAttributeLocation(const ProgramHandle& handle, const char* attributeName, bool forceUpdate = false);
GLint programUniformLocation(const ProgramHandle& handle, const char* uniformName, bool forceUpdate = false);
void linkProgram(const ProgramHandle&);
void enableProgram(const ProgramHandle&);


struct ProgramHandle : protected eth::glHandle<GLuint> {
	friend const ProgramHandle createProgram(void);
	friend void deleteProgram(const ProgramHandle&);
	friend GLint programAttributeLocation(const ProgramHandle&, const char*, bool);
	friend GLint programUniformLocation(const ProgramHandle&, const char*, bool);
	friend void linkProgram(const ProgramHandle&);
	friend void attachShader(const ProgramHandle&, const ShaderHandle&);
	friend void enableProgram(const ProgramHandle&);
	friend void freeProgram(const ProgramHandle&);
};



} /* namespace eth */



#endif /* SHADER_H_ */
