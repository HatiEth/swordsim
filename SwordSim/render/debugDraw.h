/*
 * debugDraw.h
 *
 *  Created on: Nov 5, 2013
 *      Author: Hati
 */

#ifndef DEBUGDRAW_H_
#define DEBUGDRAW_H_

#include <glm/glm.hpp>
#include <gl/glew.h>
#include "Camera.h"


static inline void drawRect(const glm::vec2& halfExtent) {
	glBegin(GL_TRIANGLE_STRIP); {
		glVertex2f(-halfExtent.x, -halfExtent.y);
		glVertex2f(-halfExtent.x, halfExtent.y);
		glVertex2f(halfExtent.x, -halfExtent.y);
		glVertex2f(halfExtent.x, halfExtent.y);
	} glEnd();
}

static inline void drawRectAt(const glm::vec2& halfExtent, const glm::vec2& at) {
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(at.x, at.y, 0);
	glBegin(GL_TRIANGLE_STRIP); {
		glVertex2f(-halfExtent.x, -halfExtent.y);
		glVertex2f(-halfExtent.x, halfExtent.y);
		glVertex2f(halfExtent.x, -halfExtent.y);
		glVertex2f(halfExtent.x, halfExtent.y);
	} glEnd();
	glPopMatrix();
}

/**
 *
 * @param halfExtent
 * @param rotation in Degree!
 */
static inline void drawOrientedRect(const glm::vec2& halfExtent, float rotation) {
	glPushMatrix();
	glLoadIdentity();
	glRotatef(rotation, 0, 0, 1);
	drawRect(halfExtent);
	glPopMatrix();
}

static inline void drawQuadAt( const float& halfExtent, const glm::vec2& at) {
	drawRectAt(glm::vec2(halfExtent), at);
}

static inline void drawQuad (const float& halfExtent) {
	drawRect(glm::vec2(halfExtent));
}

static inline void useCamera(const Camera& c) {
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(c.valuePtr());
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
}

static inline void displayTexture(GLuint texId, glm::vec2 halfExtent = glm::vec2(400, 300), glm::vec2 translate = glm::vec2(0,0)) {
	glColor4f(1,1,1,1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texId);
	glBegin(GL_TRIANGLE_STRIP);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(translate.x-halfExtent.x, translate.y-halfExtent.y);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(translate.x-halfExtent.x, translate.y+halfExtent.y);
		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(translate.x+halfExtent.x, translate.y-halfExtent.y);
		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(translate.x+halfExtent.x, translate.y+halfExtent.y);
	}
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

}

#endif /* DEBUGDRAW_H_ */
