/*
 * FBO.cpp
 *
 *  Created on: 07.11.2013
 *      Author: Hati
 */

#include <render/FBO.h>
#include <Utility.h>



SingleTextureFBO createFBO() {
	SingleTextureFBO fbo;

	/**
	 * Generate fbo with single texture & depth+stencil buffer here
	 */
	glGenTextures(1, &fbo.textureId);
	glBindTexture(GL_TEXTURE_2D, fbo.textureId);
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 800, 600, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glBindTexture(GL_TEXTURE_2D, 0);


	glGenRenderbuffers(1, &fbo.depthStencilId);
	glBindRenderbuffer(GL_RENDERBUFFER, fbo.depthStencilId);
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
	}

	glGenFramebuffers(1, &fbo.fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo.fboId);
	{

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, fbo.textureId, 0);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
		GL_RENDERBUFFER, fbo.depthStencilId);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		KE_Assert(status == GL_FRAMEBUFFER_COMPLETE, "%s",
				glewGetErrorString(status));

		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	KE_Notify("FBO Creation: %s",
			(const char* )glewGetErrorString(glGetError()));
	return fbo;
}

void clearFBO(SingleTextureFBO& fbo,
		const glm::vec4& clearColor) {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo.fboId);
	glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


