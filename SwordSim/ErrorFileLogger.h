/*
 * ErrorFileLogger.h
 *
 *  Created on: Jul 7, 2013
 *      Author: Hati
 */

#ifndef ERRORFILELOGGER_H_
#define ERRORFILELOGGER_H_

#include "Utility.h"
#include <iostream>
#include <fstream>

namespace eth {

class ErrorFileLogger: public KELogger {
protected:
	virtual void write(const char* str) {
		m_ostr << str << "\n";
		std::cout << str << std::endl;
		m_ostr.flush();
	}
	std::ofstream m_ostr;
public:
	ErrorFileLogger(const char* logFile) {
		m_ostr.open(logFile);
		if (!m_ostr) {
			KE_Error(KE_FAILURE_STR_STR,
					"ErrorFileLogger cannot be initialized. Stream failure: ",
					logFile);
		}
	}
	virtual ~ErrorFileLogger() {
		m_ostr.close();
	}
};

} /* namespace ke */
#endif /* ERRORFILELOGGER_H_ */
