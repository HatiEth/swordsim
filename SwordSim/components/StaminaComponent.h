/*
 * StaminaComponent.h
 *
 *  Created on: 03.12.2013
 *      Author: Hati
 */

#ifndef STAMINACOMPONENT_H_
#define STAMINACOMPONENT_H_

#include "Component.h"

struct StaminaComponent : public Component {
	int _value;
	int _maxValue;
};

#endif /* STAMINACOMPONENT_H_ */
