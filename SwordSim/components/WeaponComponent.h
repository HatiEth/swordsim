/*
 * WeaponComponent.h
 *
 *  Created on: Feb 16, 2014
 *      Author: Hati
 */

#ifndef WEAPONCOMPONENT_H_
#define WEAPONCOMPONENT_H_

#include <glm/glm.hpp>
#include "Component.h"

struct WeaponComponent : public Component {
	// attachment offset
	glm::vec2 attachmentOffset;
	// collision shape
};


#endif /* WEAPONCOMPONENT_H_ */
