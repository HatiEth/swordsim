/*
 * DestroyComponent.h
 *
 *  Created on: Feb 21, 2014
 *      Author: Hati
 */

#ifndef DESTROYCOMPONENT_H_
#define DESTROYCOMPONENT_H_

#include "../Component.h"

/**
 * Flags an entity for destruction
 */
struct DestroyComponent : public Component {};


#endif /* DESTROYCOMPONENT_H_ */
