/*
 * InternalComponents.cpp
 *
 *  Created on: Feb 21, 2014
 *      Author: Hati
 */

#include "../Components.h"
#include <parser/ParseInfo.h>
#include "InternalComponents.h"

#define INTERNAL_STRING(x) (#x)

REGISTER_COMPONENT(DestroyComponent, INTERNAL_STRING("Destroy"));
PARSE_COMPONENT(DestroyComponent) {

}
DEFINE_PARSE(DestroyComponent);
