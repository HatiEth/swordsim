/*
 * PositionComponent.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Hati
 */

#ifndef POSITIONCOMPONENT_H_
#define POSITIONCOMPONENT_H_
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "Component.h"

struct TransformComponent : public Component {
	glm::fvec2 position;
	float rotation;
	glm::fvec2 scaling;
};




#endif /* POSITIONCOMPONENT_H_ */
