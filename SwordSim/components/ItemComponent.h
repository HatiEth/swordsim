/*
 * ItemComponent.h
 *
 *  Created on: Feb 16, 2014
 *      Author: Hati
 */

#ifndef ITEMCOMPONENT_H_
#define ITEMCOMPONENT_H_

#include "Component.h"


/**
 * Items are per definition pick-up-ables
 */
struct ItemComponent : public Component {
	bool consumeable;
	int count;
	float pickupRadius;
};


#endif /* ITEMCOMPONENT_H_ */
