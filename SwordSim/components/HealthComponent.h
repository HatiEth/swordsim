/*
 * HealthComponent.h
 *
 *  Created on: 03.12.2013
 *      Author: Hati
 */

#ifndef HEALTHCOMPONENT_H_
#define HEALTHCOMPONENT_H_

#include "Component.h"
///@todo rename to GenericValueComponent (ie.: Stamina, Health are just renamed)
struct HealthComponent : public Component, public WatchableComponent {
	int _value;
	int _maxValue;
	//float _tick; //describes frequency of health regeneration (tick based)
	//int _tickRegeneration;
};

#endif /* HEALTHCOMPONENT_H_ */
