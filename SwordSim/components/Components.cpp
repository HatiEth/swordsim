/*
 * Components.cpp
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#include <parser/ParseInfo.h>
#include "Components.h"



REGISTER_COMPONENT(TransformComponent, "Transform");
template<>
void initComponent(TransformComponent* comp) { ///< overwriting to set initial scaling to 1,1 instead of 0,0
	static glm::vec2 defaultScalingVector = glm::vec2(1,1);
	memset(comp, 0, sizeof(TransformComponent));
	comp->scaling = defaultScalingVector;
}
PARSE_COMPONENT(TransformComponent) {
	TransformComponent* c = PARSE_CAST_COMPONENT(TransformComponent);
	c->position = parseValue<glm::vec2>(values, "position");
	c->rotation = parseValue<float>(values, "rotation");
	c->scaling	= parseValue(values, "scaling", glm::vec2(1,1));
}
DEFINE_PARSE(TransformComponent);
MERGE_COMPONENT(TransformComponent) {
	MERGE_CAST_COMPONENT(TransformComponent);
	merged->position = parseValue(values, "position", merged->position);
	merged->rotation = parseValue(values, "rotation", merged->rotation);
	merged->scaling = parseValue(values, "scaling", merged->scaling);
}
DEFINE_MERGE(TransformComponent);


#include <render/Texture.h>
REGISTER_COMPONENT(SpriteComponent, "Sprite");
PARSE_COMPONENT(SpriteComponent) {
	SpriteComponent* c = PARSE_CAST_COMPONENT(SpriteComponent);
	Texture tex = parseValue<Texture>(values, "texture");
	c->texture = tex.textureId;
	c->dimension = glm::vec2(tex.width, tex.height);
}
DEFINE_PARSE(SpriteComponent);
MERGE_COMPONENT(SpriteComponent) {
	MERGE_CAST_COMPONENT(SpriteComponent);
	Texture mergedTex = Texture();
	mergedTex.height = merged->dimension.y; mergedTex.width = merged->dimension.x;
	mergedTex.textureId = merged->texture;
	Texture tex = parseValue<Texture>(values, "texture", mergedTex);

	merged->texture = tex.textureId;
	merged->dimension = glm::vec2(tex.width, tex.height);
}
DEFINE_MERGE(SpriteComponent);


REGISTER_COMPONENT(StaminaComponent, "Stamina");
PARSE_COMPONENT(StaminaComponent) {
	StaminaComponent* c = PARSE_CAST_COMPONENT(StaminaComponent);
	c->_maxValue = parseValue<int>(values, "max");
	c->_value = c->_maxValue;
}
DEFINE_PARSE(StaminaComponent);


REGISTER_COMPONENT(HealthComponent, "Health");
PARSE_COMPONENT(HealthComponent) {
	HealthComponent* c = PARSE_CAST_COMPONENT(HealthComponent);
	c->_maxValue = parseValue<int>(values, "max");
	c->_value = c->_maxValue;
}
DEFINE_PARSE(HealthComponent);


REGISTER_COMPONENT(HudComponent, "Hud");
PARSE_COMPONENT(HudComponent) {
	HudComponent* c = PARSE_CAST_COMPONENT(HudComponent);
	c->target = parseValue(values, "target", "null");
	c->watch = parseValue(values, "watch", "null");
	Texture dbgTex = loadTextureFromPNG("assets/base_pose.png");
	c->texture = parseValue<Texture>(values, "texture", dbgTex);
	c->relative = parseValue<bool>(values, "relative");

}
DEFINE_PARSE(HudComponent);
MERGE_COMPONENT(HudComponent) {
	MERGE_CAST_COMPONENT(HudComponent);
	merged->relative = parseValue(values, "relative", merged->relative);
	merged->target = parseValue(values, "target", merged->target);
	merged->watch = parseValue(values, "watch", merged->watch);
	merged->texture = parseValue<Texture>(values, "texture", merged->texture);
}
DEFINE_MERGE(HudComponent);


REGISTER_COMPONENT(ReferenceComponent, "Reference");
PARSE_COMPONENT(ReferenceComponent) {
	ReferenceComponent* c = PARSE_CAST_COMPONENT(ReferenceComponent);
	c->referenceAlias = parseValue(values, "alias", "");
}
DEFINE_PARSE(ReferenceComponent);


REGISTER_COMPONENT(PhysicsComponent, "Physics");
PARSE_COMPONENT(PhysicsComponent) {
	PhysicsComponent* c = PARSE_CAST_COMPONENT(PhysicsComponent);
	c->bodyDef = parseValue<b2BodyDef>(values, "b2Body");

	c->fixDef = parseValue<b2FixtureDef>(values, "b2Fixture");

}
DEFINE_PARSE(PhysicsComponent);



REGISTER_COMPONENT(PlayerComponent, "Player");
PARSE_COMPONENT(PlayerComponent) {
/// empty PlayerComponent
}
DEFINE_PARSE(PlayerComponent);


REGISTER_COMPONENT(ItemComponent, "Item");
PARSE_COMPONENT(ItemComponent) {
	ItemComponent* ic =PARSE_CAST_COMPONENT(ItemComponent);
	ic->consumeable = parseValue(values, "consumeable", false);
	ic->count = parseValue(values, "count", 1);
	ic->pickupRadius = parseValue(values, "pickupRadius", 1.0f);
}
DEFINE_PARSE(ItemComponent);


REGISTER_COMPONENT(WeaponComponent, "Weapon");
PARSE_COMPONENT(WeaponComponent) {
	WeaponComponent* wc = PARSE_CAST_COMPONENT(WeaponComponent);

	wc->attachmentOffset = parseValue<glm::vec2>(values, "offset");
}
DEFINE_PARSE(WeaponComponent);


REGISTER_COMPONENT(EquipmentComponent, "Equipment");
PARSE_COMPONENT(EquipmentComponent) {
	EquipmentComponent* ec = PARSE_CAST_COMPONENT(EquipmentComponent);
	ec->equipmentSlots = parseValue(values, "slots", -1);
	ec->_freeSlots = ec->equipmentSlots;

}
DEFINE_PARSE(EquipmentComponent);
