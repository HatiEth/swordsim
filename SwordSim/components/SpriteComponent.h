/*
 * SpriteComponent.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Hati
 */

#ifndef SPRITECOMPONENT_H_
#define SPRITECOMPONENT_H_
#include <gl/glew.h>
#include <glm/glm.hpp>
#include "Component.h"

///@todo other struct required - no pure textureHandle here
struct SpriteComponent : public Component {
	GLuint texture;
	glm::vec2 dimension;
};


#endif /* SPRITECOMPONENT_H_ */
