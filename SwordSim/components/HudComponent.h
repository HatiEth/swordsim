/*
 * HudComponent.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef HUDCOMPONENT_H_
#define HUDCOMPONENT_H_

#include "Component.h"
#include <render/Texture.h>
/**
 * Displays <watch> (ComponentType) for target (entity-flag)
 */
struct HudComponent : public Component {
	const char* watch;
	const char* target;
	Texture texture;
	bool relative;
};


#endif /* HUDCOMPONENT_H_ */
