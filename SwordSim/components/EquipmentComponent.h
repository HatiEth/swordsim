/*
 * EquipmentComponent.h
 *
 *  Created on: Feb 17, 2014
 *      Author: Hati
 */

#ifndef EQUIPMENTCOMPONENT_H_
#define EQUIPMENTCOMPONENT_H_

#include "Component.h"
#include <vector>
#include <entity/Entity.h>

struct EquipmentComponent : public Component {
	int equipmentSlots;
	int _freeSlots;

	// free data;
	std::vector<EntityInfo*> _items;
};


#endif /* EQUIPMENTCOMPONENT_H_ */
