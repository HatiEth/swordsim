/*
 * PhysicsComponent.h
 *
 *  Created on: 16.12.2013
 *      Author: Hati
 */

#ifndef PHYSICSCOMPONENT_H_
#define PHYSICSCOMPONENT_H_

#include "Component.h"
#include <Box2D/Box2D.h>

struct PhysicsComponent : public Component {
	b2BodyDef bodyDef;
	b2FixtureDef fixDef;
	b2Body* _body{nullptr};
};


#endif /* PHYSICSCOMPONENT_H_ */
