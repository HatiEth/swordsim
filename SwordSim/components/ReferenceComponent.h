/*
 * ReferenceComponent.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef REFERENCECOMPONENT_H_
#define REFERENCECOMPONENT_H_

#include "Component.h"
struct ReferenceComponent : public Component {
	const char* referenceAlias;
};



#endif /* REFERENCECOMPONENT_H_ */
