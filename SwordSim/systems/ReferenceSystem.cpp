/*
 * ReferenceSystem.cpp
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#include "ReferenceSystem.h"

inline void ReferenceSystem::createNode(EntityInfo& entity) {
	static ReferenceNode n; // nodes are not required in system directly, but registration requires them
	::fillNode(n, entity);
	ReferenceMap::iterator prevIt;
	if ((prevIt = this->_references.find(n.reference->referenceAlias))
			!= this->_references.end()) {

		KE_CustomWarning(
				"Overwriting entity reference %s (%u) with new Entity %u",
				n.reference->referenceAlias, prevIt->second->_id, entity._id);
		this->_references[n.reference->referenceAlias] = &entity;
	}
	else {
		KE_Notify("Creating reference %s for entity %u", n.reference->referenceAlias, entity._id);
		this->_references[n.reference->referenceAlias] = &entity;
	}
}

/**
 * System will update out-dated or incoming references here.
 * @param system
 * @param dt
 */
ethDefineSystemExecute(ReferenceSystem, system, dt) {
//	using NodeIterator = ReferenceSystem::ReferenceMap::iterator;
}

void ReferenceSystem::removeNode(EntityInfo& entity) {
}
