/*
 * DebugSystem.h
 *
 *  Created on: Feb 20, 2014
 *      Author: Hati
 */

#ifndef DEBUGSYSTEM_H_
#define DEBUGSYSTEM_H_

#include <nodes/DebugNode.h>
#include "System.h"

#include <tr1/unordered_set>

struct eqEntityInfo {
	inline size_t operator()(EntityInfo* val) const {
		return hash(val);
	}
	bool operator()(const EntityInfo* e1, const EntityInfo* e2) const {
	    return e1->_id == e2->_id;
	}
	inline size_t hash(const EntityInfo* val) const {
		return val->_id;
	}
};

typedef std::tr1::unordered_set<EntityInfo*, ::eqEntityInfo, ::eqEntityInfo> EntityInfoHashSet;

struct DebugSystem : public System {
	EntityInfoHashSet entities;

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
	virtual void updateNode(EntityInfo& entity) {}
};

#endif /* DEBUGSYSTEM_H_ */
