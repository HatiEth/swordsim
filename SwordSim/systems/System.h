/*
 * System.h
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <Utility.h>
#include <set>
#include <algorithm>

struct EntityInfo;

struct System;
using SystemContainer = eth::StringMap<System*>::Type;

/**
 * Base class for systems working on nodes
 * @todo maybe without virtual possible?
 */




struct System {
	virtual ~System() {}
	virtual void createNode(EntityInfo& entity)=0;
	virtual void removeNode(EntityInfo& entity)=0;
	virtual void updateNode(EntityInfo& entity) {}


	SystemContainer* _systemContainer{nullptr};
};


template<typename Sys>
void ethSystemExecute(Sys& system, float dt);


#define ethDefineSystemExecute(SystemType, systemId, deltaTimeId) template<> void ethSystemExecute(SystemType& systemId, float deltaTimeId)


#endif /* SYSTEM_H_ */
