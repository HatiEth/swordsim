/*
 * PlayerSystem.h
 *
 *  Created on: Feb 15, 2014
 *      Author: Hati
 */

#ifndef PLAYERSYSTEM_H_
#define PLAYERSYSTEM_H_

#include "System.h"
#include "nodes/PlayerNode.h"
#include <vector>
#include <Box2D/Box2D.h>
#include <glm/glm.hpp>

struct PlayerSystem: public System {
	std::vector<PlayerNode*> _nodes;

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
	virtual void updateNode(EntityInfo& entity){}
};

#endif /* PLAYERSYSTEM_H_ */
