/*
 * DebugSystem.cpp
 *
 *  Created on: Feb 20, 2014
 *      Author: Hati
 */

#include "DebugSystem.h"
#include <GL/glew.h>
#include <components/Components.h>

void DebugSystem::createNode(EntityInfo& entity) {
	if (entities.find(&entity) != entities.end()) {

	} else {
		entities.insert(&entity);

		KE_Notify("Created debug entry for %u", entity._id);
	}
}

template<typename C>
void drawDebugInfo(C* comp);

template<typename C>
void drawDebug(EntityInfo* e, const glm::vec2& position, float rotation = 0.0f, const glm::vec2& scaling=glm::vec2(1,1)) {
	glLoadIdentity();
	glTranslatef(position.x, position.y, 0.0f);
	glRotatef(rotation, 0, 0, 1);
	glScalef(scaling.x, scaling.y, 0);
	if (e->thas<C>()) {
		drawDebugInfo(e->tget<C>());
	}
	glColor4f(1,1,1,1);
}

ethDefineSystemExecute(DebugSystem, system, dt) {
	auto it = system.entities.begin();
	for (; it != system.entities.end(); ++it) {
		EntityInfo* e = *it;
		if(!e->thas<TransformComponent>()) {
			continue;
		}
		TransformComponent* tc = e->tget<TransformComponent>();

		drawDebug<TransformComponent>(e, tc->position, tc->rotation, tc->scaling);
		drawDebug<ItemComponent>(e, tc->position);
	}
	glLoadIdentity();
}

/*
 template<>
 void drawDebugInfo(ItemComponent* comp) {

 }
 */

template<>
void drawDebugInfo(TransformComponent* comp) {
	glBegin(GL_LINES);
	{
		glColor3f(1, 0, 0);
		glVertex2f(0.0f, 0.0f);
		glVertex2f(1.0f / comp->scaling.x, 0.0f);

		glColor3f(0, 1, 0);
		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 1.0f / comp->scaling.y);
	}
	glEnd();
}

template<>
void drawDebugInfo(ItemComponent* comp) {
	const float32 k_segments = 16.0f;
	const float32 k_increment = 2.0f * b2_pi / k_segments;
	glColor4f(0.33f, 0.33f, 0.33f, 1.0f);
	float32 theta = 0.0f;
	glBegin(GL_LINE_LOOP);
	{
		for (int32 i = 0; i < k_segments; ++i) {
			glm::vec2 v = comp->pickupRadius
					* glm::vec2(glm::cos(theta), glm::sin(theta));
			glVertex2f(v.x, v.y);
			theta += k_increment;
		}
	}
	glEnd();
}

void DebugSystem::removeNode(EntityInfo& entity) {
}
