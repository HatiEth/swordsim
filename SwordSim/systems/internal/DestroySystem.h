/*
 * DestroySystem.h
 *
 *  Created on: Feb 23, 2014
 *      Author: Hati
 */

#ifndef DESTROYSYSTEM_H_
#define DESTROYSYSTEM_H_

#include <systems/System.h>
#include <nodes/internal/DestroyNode.h>

#include <entity/Entity.h>

struct DestroySystem : public System {
	std::vector<DestroyNode> _nodes;

	EntityContainer& _entities;

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
	virtual void updateNode(EntityInfo& entity) {}

	DestroySystem(EntityContainer& ec) : _entities(ec) {}
};


#endif /* DESTROYSYSTEM_H_ */
