/*
 * DestroySystem.cpp
 *
 *  Created on: Feb 23, 2014
 *      Author: Hati
 */



#include "DestroySystem.h"


inline void DestroySystem::createNode(EntityInfo& entity) {
	DestroyNode node;
	ethInitNode(node, entity);

	fillNode(node, entity);


	this->_nodes.push_back(node);
}

inline void DestroySystem::removeNode(EntityInfo& entity) {
	// Do nothing here, is done after each execute
}

void removeAllComponents(EntityInfo* e, SystemContainer& sc) {
	KE_Notify("Stripping all components from %u..", e->_id);
	while(e->_components.size()>0) {
		auto cIt = e->_components.begin();
		e->remove(cIt->first, sc);
	}

}

ethDefineSystemExecute(DestroySystem, system, dt) {
	auto it = system._nodes.begin();

	for(;it!=system._nodes.end();++it) {

		removeAllComponents(it->_internalPtr, *system._systemContainer);

		system._entities.destroyEntity((*it)._internalId);
	}

	system._nodes.clear();
}
