/*
 * HealthSystem.cpp
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */


#include "HealthSystem.h"
#include <glm/glm.hpp>

void HealthSystem::createNode(EntityInfo& entity) {
	HealthNode* n = new HealthNode();
	ethInitNode(n, entity);

	::fillNode(*n, entity);
	_nodes.push_back(n);
	KE_Notify("Created HealthNode for %u", entity._id);
}


ethDefineSystemExecute(HealthSystem, system, dt) {
	using NodeIterator = std::vector<HealthNode*>::iterator;

	NodeIterator it = system._nodes.begin();
	for(;it!=system._nodes.end();++it) {
		const int REGEN = 1;
		(*it)->hc->_value = glm::clamp((*it)->hc->_value + REGEN, 0, (*it)->hc->_maxValue);
		(*it)->hc->hudData = (*it)->hc->_value;
	}
}

void HealthSystem::removeNode(EntityInfo& entity) {
}
