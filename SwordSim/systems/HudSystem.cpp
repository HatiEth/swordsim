/*
 * HudSystem.cpp
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */


#include "HudSystem.h"
#include <GL/glew.h>
#include <random>

inline void HudSystem::createNode(EntityInfo& entity) {
	HudNode* n = new HudNode();
	ethInitNode(n, entity);

	::fillNode(*n, entity);
	this->nodes.push_back(n);
	KE_Notify("Created %s for %u", "HudNode", entity._id);
}

ethDefineSystemExecute(HudSystem, system, dt) {
	using NodeIterator = std::vector<HudRenderNode*>::iterator;

	NodeIterator it = system.renderNodes.begin();
	for(;it!=system.renderNodes.end();++it) {


	}
}

void HudSystem::linkHud() {
	using NodeIterator = std::vector<HudNode*>::iterator;

	///@todo memleak after multiple links
	renderNodes.clear();
	KE_Notify("Linking Hud!");

	NodeIterator it = nodes.begin();
	for(;it!=nodes.end();++it) {
		auto& hudComp = (*it)->hudComponent;
		EntityInfo& watchedEntity = watchableEntities.getByReference(hudComp->target);
		if(watchedEntity.has(hudComp->watch)) {
			Component* watchedComp = watchedEntity.get(hudComp->watch);
			WatchableComponent* wC = (WatchableComponent*)watchedComp;
			if(wC != nullptr) {
				KE_Notify("Creating Hud element for Entity %u (%s) Component %s", watchedEntity._id, hudComp->target, hudComp->watch);
				HudRenderNode* hrn = new HudRenderNode(HudType::Bar, hudComp->texture, wC->hudData, watchedEntity, (*it)->transform, hudComp->relative);
				renderNodes.push_back(hrn);

			}
			else {
				KE_CustomWarning("Trying to display unwatchable component %s on Entity %u", hudComp->watch, watchedEntity._id);
			}
		}
		else {
			KE_Notify("Hud Error: Entity %u does not have component %s", watchedEntity._id, hudComp->watch);
		}
	}
}

void drawHudRenderNode(HudRenderNode& node) {
	glColor4f(1,1,1,1);
	glBindTexture(GL_TEXTURE_2D, node._texture.textureId);
	glLoadIdentity();
	if(node._relativeTransform && node._entity.has(COMPONENT_ALIAS(TransformComponent))) {
		TransformComponent* tc = node._entity.get<TransformComponent>(COMPONENT_ALIAS(TransformComponent));
		glRotatef(tc->rotation, 0, 0, 1);
		glTranslatef(tc->position.x, tc->position.y, 0);
		glScalef(tc->scaling.x, tc->scaling.y, 0);
	}
	glRotatef(node._transform->rotation, 0, 0, 1);
	glTranslatef(node._transform->position.x, node._transform->position.y, 0.0f);
	glScalef(node._transform->scaling.x, node._transform->scaling.y, 0);

	glBegin(GL_TRIANGLE_STRIP);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(-node._texture.width*0.5f, -node._texture.height*0.5f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(-node._texture.width*0.5f, +node._texture.height*0.5f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(+node._texture.width*0.5f, -node._texture.height*0.5f);
		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(+node._texture.width*0.5f, +node._texture.height*0.5f);
	}

	glColor4f(1,0,0,1);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(-node._texture.width*0.5f, -node._texture.height*0.5f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(-node._texture.width*0.5f, +node._texture.height*0.5f);
		glTexCoord2f((float(node._value)/100.0f)*1.0f, 0.0f);
		glVertex2f((float(node._value - 50)/50.0f )*node._texture.width*0.5f, -node._texture.height*0.5f);
		glTexCoord2f((float(node._value)/100.0f)*1.0f, 1.0f);
		glVertex2f((float(node._value - 50)/50.0f )*node._texture.width*0.5f, +node._texture.height*0.5f);
	}
	glEnd();


	glBindTexture(GL_TEXTURE_2D, 0);

}

void HudSystem::drawHud() {
	using NodeIterator = std::vector<HudRenderNode*>::iterator;

	NodeIterator it = this->renderNodes.begin();
	for(;it!=renderNodes.end();++it) {
		drawHudRenderNode(*(*it));
	}
}

void HudSystem::removeNode(EntityInfo& entity) {
}
