/*
 * RenderSystem.cpp
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#include "RenderSystem.h"
#include <render/debugDraw.h>
#include <GL/glew.h>
#include <Utility.h>


RenderSystem::RenderSystem(void) {
	this->_spriteProgram = eth::createProgram();
	eth::ShaderHandle spriteFrag = eth::createShader("shaders/sprite.frag");
	eth::ShaderHandle spriteVert = eth::createShader("shaders/sprite.vert");

	eth::attachShader(_spriteProgram, spriteFrag);
	eth::attachShader(_spriteProgram, spriteVert);

	eth::linkProgram(_spriteProgram);


}

void RenderSystem::setCamera(Camera& cam) {
//	memcpy(&_renderProjection, cam.valuePtr(), sizeof(glm::mat4));
	_renderCamera = cam;
}

void RenderSystem::createNode(EntityInfo& entity) {
	RenderNode* rn = new RenderNode();
	ethInitNode(rn, entity);

	::fillNode(*rn, entity);
	this->_nodes.push_back(rn);
	KE_Notify("Created RenderNode for %u", entity._id);
}
/**
 * Draws the SpriteComponent at the transformed point as half extent
 * @param transform
 * @param sprite
 */
inline void drawSpriteAt(RenderSystem& sys, TransformComponent* transform, SpriteComponent* sprite) {
	glColor4f(1,1,1,1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sprite->texture);

	glUniform1i(eth::programUniformLocation(sys._spriteProgram, "uSpriteSampler"), 0);

//	glLoadIdentity();
//	glTranslatef(transform->position.x, transform->position.y, 0.0f);
//	glRotatef(transform->rotation, 0, 0, 1);
//	glScalef(transform->scaling.x, transform->scaling.y, 0);
	GLint uTransformLoc = eth::programUniformLocation(sys._spriteProgram, "uTransforms");
	glUniform1fv(uTransformLoc, 5, (GLfloat*)transform);
//	KE_Notify("%3.2f ", transform->rotation);

	glBegin(GL_TRIANGLE_STRIP);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(-sprite->dimension.x*0.5f, -sprite->dimension.y*0.5f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(-sprite->dimension.x*0.5f, +sprite->dimension.y*0.5f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(+sprite->dimension.x*0.5f, -sprite->dimension.y*0.5f);
		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(+sprite->dimension.x*0.5f, +sprite->dimension.y*0.5f);
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}

ethDefineSystemExecute(RenderSystem, system, dt) {
	using RenderNodeIterator = std::vector<RenderNode*>::iterator;

	eth::enableProgram(system._spriteProgram);
	{
//	GLint location,  GLsizei count,  GLboolean transpose,  const GLfloat *value
		GLint uCamLoc = eth::programUniformLocation(system._spriteProgram, "uCamera");
		glUniformMatrix4fv(uCamLoc, 1, true, system._renderCamera.valuePtr());
//		glUniform2f(eth::programUniformLocation(system._spriteProgram, "uViewport", true), system._renderCamera.m_viewPort.x, system._renderCamera.m_viewPort.y);
//		glUniform2fv(eth::programUniformLocation(system._spriteProgram, "uViewport"), 1, glm::value_ptr(system._renderCamera.m_viewPort));
		RenderNodeIterator it = system._nodes.begin();
		for(;it!=system._nodes.end();++it) {
			drawSpriteAt(system, (*it)->position, (*it)->sprite);
		}
	}
	glUseProgram(0);
}

void RenderSystem::removeNode(EntityInfo& entity) {
	KE_Notify("[RenderSystem] Removing nodes .. ");
	_nodes.erase(std::remove_if(_nodes.begin(), _nodes.end(), [&entity](RenderNode* node) {
		bool b = entity._id == node->_internalId;
		if(b) {
			KE_Notify(">Removed node for %u", entity._id);
			delete node;
		}
		return b;
	}), _nodes.end());
}
