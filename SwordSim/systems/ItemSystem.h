/*
 * ItemSystem.h
 *
 *  Created on: Feb 16, 2014
 *      Author: Hati
 */

#ifndef ITEMSYSTEM_H_
#define ITEMSYSTEM_H_

#include "System.h"
#include <vector>
#include <nodes/ItemNode.h>

struct ItemSystem : public System {
	std::vector<ItemNode*> _nodes;

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
	virtual void updateNode(EntityInfo& entity) {}
};



#endif /* ITEMSYSTEM_H_ */
