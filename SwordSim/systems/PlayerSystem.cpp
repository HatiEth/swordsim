/*
 * PlayerSystem.cpp
 *
 *  Created on: Feb 15, 2014
 *      Author: Hati
 */

#include "PlayerSystem.h"
#include <window/InputService.h>

void PlayerSystem::createNode(EntityInfo& entity) {
	PlayerNode* n = new PlayerNode();
	ethInitNode(n, entity);

	::fillNode(*n, entity);

	_nodes.push_back(n);
	KE_Notify("Created %s for %u", "PlayerNode", entity._id);
}

ethDefineSystemExecute(PlayerSystem, system, dt) {
	using NodeIterator = std::vector<PlayerNode*>::iterator;
	InputService* input = service<InputService>::get();
	NodeIterator it = system._nodes.begin();
	for (; it != system._nodes.end(); ++it) {
		if (input->isKeyPushed(GLFW_KEY_D)) {
			b2Vec2 curVelo = (*it)->physics->_body->GetLinearVelocity();
			curVelo.x = glm::clamp(curVelo.x + 5, -5.f, 10.f);
			(*it)->physics->_body->SetLinearVelocity(curVelo);
			float xFlip = (*it)->transform->scaling.x;
			if (xFlip < 0) {
				(*it)->transform->scaling.x = -xFlip;
			}
		}
		if (input->isKeyPushed(GLFW_KEY_A)) {
			b2Vec2 curVelo = (*it)->physics->_body->GetLinearVelocity();
			curVelo.x = glm::clamp(curVelo.x - 5, -10.f, 5.f);
			(*it)->physics->_body->SetLinearVelocity(curVelo);
			float xFlip = (*it)->transform->scaling.x;
			if (xFlip > 0) {
				(*it)->transform->scaling.x = -xFlip;
			}
		}
		if (input->isKeyPushed(GLFW_KEY_W)) {
			b2Vec2 curVelo = (*it)->physics->_body->GetLinearVelocity();
			curVelo.y = 10;
			(*it)->physics->_body->SetLinearVelocity(curVelo);
		}
	}
}

void PlayerSystem::removeNode(EntityInfo& entity) {
	KE_Notify("[PlayerSystem] Removing nodes .. ");
	_nodes.erase(std::remove_if(_nodes.begin(), _nodes.end(), [&entity](PlayerNode* node) {
		bool b = entity._id == node->_internalId;
		if(b) {
			KE_Notify(">Removed node for %u", entity._id);
			delete node;
		}
		return b;
	}), _nodes.end());
}
