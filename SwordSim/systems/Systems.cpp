/*
 * Systems.cpp
 *
 *  Created on: 12.12.2013
 *      Author: Hati
 */

#include "Systems.h"

LINK_SYSTEM(RenderSystem, RenderNode);

LINK_SYSTEM(HealthSystem, HealthNode);

LINK_SYSTEM(HudSystem, HudNode);

LINK_SYSTEM(ReferenceSystem, ReferenceNode);

LINK_SYSTEM(PhysicsSystem, PhysicsNode);

LINK_SYSTEM(PlayerSystem, PlayerNode);

LINK_SYSTEM(ItemSystem, ItemNode);

LINK_SYSTEM(EquipmentSystem, EquipmentNode);

LINK_SYSTEM(DebugSystem, DebugNode);


