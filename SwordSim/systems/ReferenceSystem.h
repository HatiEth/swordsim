/*
 * ReferenceSystem.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef REFERENCESYSTEM_H_
#define REFERENCESYSTEM_H_

#include "System.h"
#include <entity/Entity.h>
#include <nodes/ReferenceNode.h>


struct ReferenceSystem: public System {
	typedef eth::StringMap<EntityInfo*>::Type ReferenceMap;
	ReferenceMap _references;

	EntityInfo& getByReference(const char* ref) {
		KE_Assert(_references.find(ref) != _references.end(),
				"Unable to find entity reference %s! Invalid behaviour", ref);
		return *_references[ref];
	}

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
};

#endif /* REFERENCESYSTEM_H_ */
