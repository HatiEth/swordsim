/*
 * HudSystem.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef HUDSYSTEM_H_
#define HUDSYSTEM_H_

#include "System.h"
#include <nodes/HudNode.h>
#include "ReferenceSystem.h"
#include <hud/HudRenderNode.h>
#include <vector>


/**
 * Used to display Hud (currently just works with texture bars)
 *
 */
struct HudSystem : public System {
	std::vector<HudNode*> nodes;
	ReferenceSystem& watchableEntities;
	std::vector<HudRenderNode*> renderNodes;


	HudSystem(ReferenceSystem& references) : watchableEntities(references) {}
	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);

	void linkHud();

	void drawHud();
};

#endif /* HUDSYSTEM_H_ */
