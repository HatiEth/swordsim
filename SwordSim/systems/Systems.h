/*
 * Systems.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef SYSTEMS_H_
#define SYSTEMS_H_



#include <systems/DebugSystem.h>

#include <systems/RenderSystem.h>
#include <systems/HudSystem.h>
#include <systems/ReferenceSystem.h>

#include <systems/HealthSystem.h>
#include <systems/PhysicsSystem.h>

#include <systems/PlayerSystem.h>
#include <systems/ItemSystem.h>
#include <systems/EquipmentSystem.h>
#include <systems/AutoEquipSystem.h>


#include <systems/internal/InternalSystems.h>

#endif /* SYSTEMS_H_ */
