/*
 * SystemRequirements.h
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#ifndef SYSTEMREQUIREMENTS_H_
#define SYSTEMREQUIREMENTS_H_

#include <Utility.h>
#include <vector>
#include <nodes/NodeInfo.h>

struct SystemRequirements {
	typedef eth::StringMap<eth::StringHashSet>::Type SystemRequirementMap;

	static SystemRequirementMap& requirements() {
		static SystemRequirementMap sRequirements;
		return sRequirements;
	}
	///@bug not working as expected
	static bool requires(const char* sysAlias, const char* componentAlias) {
		KE_Assert(requirements().find(sysAlias)!=requirements().end(), "Unregistered systemAlias %s!", sysAlias);
		const auto& sysInfoIt = requirements().find(sysAlias);
		if(sysInfoIt->second.find(NODE_REQUIREMENT_ANY)!=sysInfoIt->second.end()) {
			return true;
		}
		return sysInfoIt->second.find(componentAlias)!=sysInfoIt->second.end();
	}

	static bool checkNodeGeneration(const char* systemAlias, const EntityInfo& entityInfo) {
		/// @todo may there is a faster approach to do this (STL functions or something)
		bool summedBool = entityInfo._components.size() > 0u;
		eth::StringHashSet& systemRequirements = requirements()[systemAlias];
		for(auto reqIt : systemRequirements) { // const char* iterator
			if(::eqstr()(reqIt, (const char*)NODE_REQUIREMENT_ANY)) { ///@todo find a better way to actually allow ANY without duplicates(not handled by systems)
				return true;
			}
			else {
				summedBool &= entityInfo._components.find(reqIt)!=entityInfo._components.end();
			}

		}
		return summedBool;
	}

	template<typename SystemType>
	struct SystemInfo {


		static bool requires(const char* componentAlias) {
			const auto& sysInfoIt = requirements().find(alias);
			return sysInfoIt->second.find(componentAlias)!=sysInfoIt->second.end();
		}

		/**
		 * Puts requirements of a specific node as system requirements
		 * @param systemAlias
		 * @return
		 */
		template<typename NodeType>
		static const char* linkSystem(const char* systemAlias) {
			if(requirements().find(systemAlias) != requirements().end()) {
				KE_CustomWarning("Overwriting system-alias %s with %s",
										requirements().find(systemAlias)->first, systemAlias);
			}
			else {
				KE_Notify("Registering system-alias %s", systemAlias);
			}

			requirements()[systemAlias] = NodeInfo<NodeType>::requirements();
			return systemAlias;
		}


		static const char* alias;
	};
};

//using SystemRequirements = SystemInfo;

#define TOSTRING(x) (#x)
#define LINK_SYSTEM(SystemType, NodeType) template<> const char* SystemRequirements::SystemInfo<SystemType>::alias = SystemRequirements::SystemInfo<SystemType>::linkSystem<NodeType>(TOSTRING(SystemType));
#define SYSTEM_NAME(SystemType) SystemRequirements::SystemInfo<SystemType>::alias


template<typename Sys>
KECode registerSystem(SystemContainer& sc, Sys* _system) {
	KE_Assert(_system->_systemContainer == nullptr, "Already system container assigned with _system!");
	sc[SYSTEM_NAME(Sys)] = _system;
	_system->_systemContainer = &sc;

	KE_Notify("[system container:%u] Registered %s", &sc, SYSTEM_NAME(Sys));

	return KE_SUCCESS;
}


/**
 * Zugriffsprototype:
 * 	ComponentDescription := name of component in file ("Sprite", "Transform", "Item", etc.)
 * 	func(Component* nc, ComponentDescription& cdesc) { //called post add
 * 	* 	std::map<SystemName, System> systems
 * 	* 	foreach(SystemName sys : systems) {
 * 	* 		if(sysRequirements(sys).requires(cdesc) && sysRequirements(sys).checkEntity(entity)) {
 * 	*			createNode(sys, entityData);
 * 	*		}
 * 	* 	}
 * 	}
 */



#endif /* SYSTEMREQUIREMENTS_H_ */
