/*
 * ItemSystem.cpp
 *
 *  Created on: Feb 16, 2014
 *      Author: Hati
 */


#include "ItemSystem.h"


void ItemSystem::createNode(EntityInfo& entity) {
	ItemNode* n = new ItemNode();
	ethInitNode(n, entity);


	::fillNode(*n, entity);

	_nodes.push_back(n);
}

ethDefineSystemExecute(ItemSystem, system, dt) {
/**
 * @todo broadphase/quadtree for faster finds
 */
}

void ItemSystem::removeNode(EntityInfo& entity) {
	KE_Notify("[ItemSystem] Removing nodes .. ");
	_nodes.erase(std::remove_if(_nodes.begin(), _nodes.end(), [&entity](ItemNode* node) {
		bool b = entity._id == node->_internalId;
		if(b) {
			KE_Notify(">Removed node for %u", entity._id);
			delete node;
		}
		return b;
	}), _nodes.end());
}
