/*
 * PhysicsSystem.h
 *
 *  Created on: 16.12.2013
 *      Author: Hati
 */

#ifndef PHYSICSSYSTEM_H_
#define PHYSICSSYSTEM_H_

#include "System.h"
#include <nodes/PhysicsNode.h>
#include <render/Box2DDebugDraw.h>
#include <vector>
#include <Box2D/Box2D.h>
#include <glm/glm.hpp>


struct PhysicsSystem : public System {
	b2World* const box2dWorld;
	std::vector<PhysicsNode*> nodes;
	Box2DDebugDraw* debugRenderer{nullptr};

	PhysicsSystem(b2World* _box2dWorld);

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
	virtual void updateNode(EntityInfo& entity);

	void debugDraw(void);
};


#endif /* PHYSICSSYSTEM_H_ */
