/*
 * EquipmentSystem.h
 *
 *  Created on: Feb 17, 2014
 *      Author: Hati
 */

#ifndef EQUIPMENTSYSTEM_H_
#define EQUIPMENTSYSTEM_H_

#include "System.h"
#include <nodes/EquipmentNode.h>
#include "ItemSystem.h"

struct EquipmentSystem: public System {
	std::vector<EquipmentNode*> _nodes;
	ItemSystem& _items;

	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
	virtual void updateNode(EntityInfo& entity) {
	}

	static const float pickupEpsilon() {
		return glm::epsilon<float>();
	}

	EquipmentSystem(ItemSystem& itemSystem) :
			_items(itemSystem) {
	}
};

#endif /* EQUIPMENTSYSTEM_H_ */
