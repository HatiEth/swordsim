/*
 * PhysicsSystem.cpp
 *
 *  Created on: 16.12.2013
 *      Author: Hati
 */

#include "PhysicsSystem.h"
#include <window/InputService.h>

void PhysicsSystem::createNode(EntityInfo& entity) {
	PhysicsNode* n = new PhysicsNode();
	ethInitNode(n, entity);

	::fillNode(*n, entity);

	n->physics->_body = box2dWorld->CreateBody(&n->physics->bodyDef);
	n->physics->_body->CreateFixture(&n->physics->fixDef);
	n->physics->_body->SetUserData(&entity);

	this->nodes.push_back(n);
	KE_Notify("Created %s for %u", "PhysicsNode", entity._id);
}

void PhysicsSystem::updateNode(EntityInfo& entity) {
	auto it = this->nodes.begin();
	for(;it!=this->nodes.end();++it) {
		if( (*it)->_internalId == entity._id ) {
			PhysicsNode* n = *it;
			::fillNode(*n, entity);

			b2Vec2 p(n->transform->position.x, n->transform->position.y);
			n->physics->_body->SetTransform(p, n->transform->rotation);
			KE_Notify("[PhysicsSystem] Updated Entity %d", entity._id);
			return;
		}
	}
}

void buildDemoLevelPhysicSystem(b2World* world) {
	b2BodyDef levelDef;
	levelDef.position.Set(0, -5);
	levelDef.type = b2_staticBody;

	b2Body* body = world->CreateBody(&levelDef);

	b2ChainShape* levelShape = new b2ChainShape();

	b2Vec2 levelVerts[10];
	for (int i = 0; i < 10; ++i) {
		levelVerts[i].Set(-50 + i * 10, 0);
	}

	levelShape->CreateChain(levelVerts, 10);

	b2FixtureDef fixDef;
	fixDef.density = 1;
	fixDef.shape = levelShape;

	body->CreateFixture(&fixDef);
}

ethDefineSystemExecute(PhysicsSystem, system, dt) {
	using NodeIterator = std::vector<PhysicsNode*>::iterator;


	// recommended values by manual
	system.box2dWorld->Step(dt, 10, 8);
	system.box2dWorld->ClearForces();

	NodeIterator it = system.nodes.begin();
	for (; it != system.nodes.end(); ++it) {
		b2Vec2 pos = (*it)->physics->_body->GetPosition();
		
		(*it)->transform->position = glm::vec2(pos.x, pos.y);
		(*it)->transform->rotation = glm::degrees((*it)->physics->_body->GetAngle());
	}
}

PhysicsSystem::PhysicsSystem(b2World* _box2dWorld) :
		box2dWorld(_box2dWorld), debugRenderer(nullptr) {

	buildDemoLevelPhysicSystem(this->box2dWorld);

	box2dWorld->SetAllowSleeping(false);
	box2dWorld->SetSubStepping(true);

}

void PhysicsSystem::debugDraw(void) {
	if (debugRenderer == nullptr) {
		debugRenderer = new Box2DDebugDraw();

		debugRenderer->AppendFlags(b2Draw::e_shapeBit);

		this->box2dWorld->SetDebugDraw(debugRenderer);
		KE_Notify("PhysicsSystem: Created debug renderer");

	}

	this->box2dWorld->DrawDebugData();
}

void PhysicsSystem::removeNode(EntityInfo& entity) {
	KE_Notify("[PhysicsSystem] Removing nodes .. ");
	nodes.erase(std::remove_if(nodes.begin(), nodes.end(), [&entity, this](PhysicsNode* node) {
		bool b = entity._id == node->_internalId;
		if(b) {
			KE_Notify(">Removed node for %u", entity._id);
			this->box2dWorld->DestroyBody(node->physics->_body);
			node->physics->_body = nullptr;
			delete node;
		}
		return b;
	}), nodes.end());
}
