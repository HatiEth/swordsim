/*
 * EquipmentSystem.cpp
 *
 *  Created on: Feb 17, 2014
 *      Author: Hati
 */

#include "EquipmentSystem.h"
#include <components/internal/InternalComponents.h>


void EquipmentSystem::createNode(EntityInfo& entity) {
	EquipmentNode* n = new EquipmentNode();
	ethInitNode(n, entity);

	::fillNode(*n, entity);

	_nodes.push_back(n);
}


ethDefineSystemExecute(EquipmentSystem, system, dt) {
	auto itemIt = system._items._nodes.begin();
	for(;itemIt!=system._items._nodes.end(); ++itemIt) {
		ItemNode* item = *itemIt;
		auto it = system._nodes.begin();
		for(;it!=system._nodes.end();++it) {
			EquipmentNode* n = *it;
			if(n->ec->equipmentSlots==-1 || n->ec->_freeSlots>0) { // has slots free
			// search for items
				float delta = glm::length(item->tc->position - n->tc->position);
				if(delta <= item->ic->pickupRadius) {
//					n->ec->_freeSlots--;
//					n->ec->_items.push_back(item->)
					///@todo actually add picked up item to inventory
					item->_internalPtr->add(COMPONENT_ALIAS(DestroyComponent), *(system._systemContainer));
				}
			}
		}
	}
}

void EquipmentSystem::removeNode(EntityInfo& entity) {
}
