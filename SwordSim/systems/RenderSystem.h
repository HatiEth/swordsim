/*
 * RenderSystem.h
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#ifndef RENDERSYSTEM_H_
#define RENDERSYSTEM_H_

#include <nodes/RenderNode.h>
#include "System.h"
#include "SystemRequirements.h"
#include <vector>
#include <render/Camera.h>
#include <render/Shader.h>

struct RenderSystem : public System {
	std::vector<RenderNode*> _nodes;
	eth::ProgramHandle _spriteProgram;
	Camera _renderCamera;


	RenderSystem(void);

	void setCamera(Camera& cam);
	virtual void createNode(EntityInfo& entity);
	virtual void removeNode(EntityInfo& entity);
};


#endif /* RENDERSYSTEM_H_ */
