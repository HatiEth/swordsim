/*
 * EntityInfoParser.cpp
 *
 *  Created on: Dec 14, 2013
 *      Author: Hati
 */

#include "EntityInfoParser.h"
#include "ParseInfo.h"

/*
 * Reading templates
 */

KECode EntityInfoParser::parseTemplate(const char* templateFilePath, EntityID* createdEntity/*=nullptr*/) {
	rapidjson::Document doc;
	KECode err = JSON::createRapidJsonDoc(doc, templateFilePath);
	KE_Notify(">---Parsing template %s..", templateFilePath);

	if (err != KE_SUCCESS) {
		return KE_FAILURE;
	}

	if (!doc.IsObject()) {
		return KE_CustomError("%s does not contain a proper template object!", templateFilePath);
	}

	EntityID newId = this->_entityContainer.createEntity();
	EntityInfo& entity = this->_entityContainer.entity(newId);
	if (doc.HasMember("components") && doc["components"].IsObject()) { 
		rapidjson::Value& components = doc["components"];
		parseComponents(entity, components);
	}
	if(createdEntity!=nullptr) {
		*createdEntity = newId;
	}
	

	KE_Notify("<---Parsed template %s", templateFilePath);

	return KE_SUCCESS;
}

KECode EntityInfoParser::parseComponent(EntityInfo& entity,
		const rapidjson::Value::Member& val) {

	KE_Notify("Parsing component %s for entity %u", val.name.GetString(), entity._id);

	Component* parsedComponent = ParseInfo::parse(val.name.GetString(), entity, val.value);
	entity.inject(val.name.GetString(), this->_systems, parsedComponent);

	return KE_SUCCESS;
}

/*
 * Reading data files
 */

KECode EntityInfoParser::parseInclude(const char* includedFilePath) {

	return KE_SUCCESS;
}

KECode EntityInfoParser::parseComponents(EntityInfo& entity,
		const rapidjson::Value& comps) {
		rapidjson::Document::ConstMemberIterator it = comps.MemberBegin();
		for (; it != comps.MemberEnd(); ++it) {
			parseComponent(entity, *it);
		}
		
		return KE_SUCCESS;
}

KECode EntityInfoParser::parse(const char* rootFile) {
	rapidjson::Document doc;
	KECode err = JSON::createRapidJsonDoc(doc, rootFile);

	return err;
}

