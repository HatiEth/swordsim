/*
 * LevelInfoParser.h
 *
 *  Created on: 07.01.2014
 *      Author: Hati
 */

#ifndef LEVELINFOPARSER_H_
#define LEVELINFOPARSER_H_

#include "JSON.h"
#include <entity/Entity.h>

struct EntityInfoParser;

struct LevelInfoParser {
	EntityContainer& _entities;
	SystemContainer& _systems;
	
	LevelInfoParser(EntityContainer& entities, SystemContainer& systems) :
		_entities(entities), _systems(systems)  {}
	
	
	KECode parseLevel(const char* levelPath);
	
	KECode parseEntities(rapidjson::Value& entityArray);
	KECode parseEntity(EntityInfoParser& entityParser, rapidjson::Value& entityVals);
};



#endif /* LEVELINFOPARSER_H_ */
