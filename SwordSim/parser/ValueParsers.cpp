/*
 * ValueParsers.cpp
 *
 *  Created on: Dec 14, 2013
 *      Author: Hati
 */

#include "ParseInfo.h"
#include <vector>

#define DEFINE_DEFAULT_VALUE(ValueType) template<> ValueType DefaultValue<ValueType>::value

/*
 * Standard defaults
 */
DEFINE_DEFAULT_VALUE(bool) = bool(false);
DEFINE_DEFAULT_VALUE(int) = int(0);
DEFINE_DEFAULT_VALUE(float) = float(0);
DEFINE_DEFAULT_VALUE(const char*) = "";

/*
 * GLM defaults
 */
#include <glm/glm.hpp>
DEFINE_DEFAULT_VALUE(glm::vec2)= glm::vec2(0);
DEFINE_DEFAULT_VALUE(glm::vec3)= glm::vec3(0);
DEFINE_DEFAULT_VALUE(glm::vec4)= glm::vec4(0);

/*
 * Custom Data values
 */
#include <render/Texture.h>
DEFINE_DEFAULT_VALUE(Texture) = Texture();

#include <Box2D/Box2D.h>
DEFINE_DEFAULT_VALUE(b2BodyDef) = b2BodyDef();
DEFINE_DEFAULT_VALUE(b2FixtureDef) = b2FixtureDef();
DEFINE_DEFAULT_VALUE(b2BodyType) = b2_staticBody;
DEFINE_DEFAULT_VALUE(const b2Shape*) = nullptr;
#include "b2VertexValues.h"
DEFINE_DEFAULT_VALUE(b2VectorArray) = { nullptr, 0 };
DEFINE_DEFAULT_VALUE(b2PolygonValues) = b2PolygonValues { nullptr, 0 };
DEFINE_DEFAULT_VALUE(b2ChainValues) = b2ChainValues { nullptr, 0 };
DEFINE_DEFAULT_VALUE(FixedFloatArray<1>)= FixedFloatArray<1> {0};
DEFINE_DEFAULT_VALUE(FixedFloatArray<2>)= FixedFloatArray<2> {0};
DEFINE_DEFAULT_VALUE(FixedFloatArray<3>)= FixedFloatArray<3> {0};
DEFINE_DEFAULT_VALUE(FixedFloatArray<4>)= FixedFloatArray<4> {0};

/*
 * Standard Parsers
 */
template<>
bool parseValue(const rapidjson::Value& val, const char* alias,
		const bool defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	return object.GetBool();
}

template<>
int parseValue(const rapidjson::Value& val, const char* alias,
		const int defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	return object.GetInt();
}

template<>
float parseValue(const rapidjson::Value& val, const char* alias,
		const float defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	return object.GetDouble();
}

template<>
const char* parseValue(const rapidjson::Value& val, const char* alias,
		const char* defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	return object.GetString();
}

template<size_t N>
FixedFloatArray<N> parseFixedFloatArray(const rapidjson::Value& val,
		const char* alias,
		const FixedFloatArray<N> defaultValue =
				DefaultValue<FixedFloatArray<N>>::value) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	FixedFloatArray<N> floatArray;
	memset(floatArray.vertices, 0, sizeof(floatArray.vertices));
	if (object.IsArray()) {
		rapidjson::Value::ConstValueIterator arrayIt = object.Begin();
		int arrayIdx = 0;
		for (; arrayIt != object.End(); ++arrayIt) {
			if (arrayIdx >= N) {
				KE_CustomWarning("Canceled parsing of fixed float array in %s",
						alias);
				break;
			}
			float x = (float) arrayIt->GetDouble();
			floatArray.vertices[arrayIdx] = x;
			++arrayIdx;

		}
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json array");
	}
	return floatArray;
}

/*
 * GLM Parsers
 */

template<>
glm::vec2 parseValue(const rapidjson::Value& val, const char* alias,
		const glm::vec2 defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	glm::vec2 output;
	if (object.IsObject()) {
		output.x = parseValue<float>(object, "x");
		output.y = parseValue<float>(object, "y");
	} else if (object.IsArray()) {
		FixedFloatArray<2> dftArray { 0 };
		dftArray.vertices[0] = defaultValue[0];
		dftArray.vertices[1] = defaultValue[1];

		FixedFloatArray<2> arrayValues = parseFixedFloatArray<2>(val, alias,
				dftArray);
		output.x = arrayValues.vertices[0];
		output.y = arrayValues.vertices[1];
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json object");
	}

	return output;
}

template<>
glm::vec3 parseValue(const rapidjson::Value& val, const char* alias,
		const glm::vec3 defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	glm::vec3 output;
	if (object.IsObject()) {
		output.x = parseValue<float>(object, "x");
		output.y = parseValue<float>(object, "y");
		output.z = parseValue<float>(object, "z");
	} else if (object.IsArray()) {
		FixedFloatArray<3> dftArray { 0 };
		dftArray.vertices[0] = defaultValue[0];
		dftArray.vertices[1] = defaultValue[1];
		dftArray.vertices[2] = defaultValue[2];

		FixedFloatArray<3> arrayValues = parseFixedFloatArray<3>(val, alias,
				dftArray);
		output.x = arrayValues.vertices[0];
		output.y = arrayValues.vertices[1];
		output.z = arrayValues.vertices[2];
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json object");
	}

	return output;
}

template<>
glm::vec4 parseValue(const rapidjson::Value& val, const char* alias,
		const glm::vec4 defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	glm::vec4 output;
	if (object.IsObject()) {
		output.x = parseValue<float>(object, "x");
		output.y = parseValue<float>(object, "y");
		output.z = parseValue<float>(object, "z");
		output.w = parseValue<float>(object, "w");
	} else if (object.IsArray()) {
		FixedFloatArray<4> dftArray { 0 };
		dftArray.vertices[0] = defaultValue[0];
		dftArray.vertices[1] = defaultValue[1];
		dftArray.vertices[2] = defaultValue[2];
		dftArray.vertices[3] = defaultValue[3];

		FixedFloatArray<4> arrayValues = parseFixedFloatArray<4>(val, alias,
				dftArray);
		output.x = arrayValues.vertices[0];
		output.y = arrayValues.vertices[1];
		output.z = arrayValues.vertices[2];
		output.w = arrayValues.vertices[3];
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json object");
	}

	return output;
}

/*
 * Custom Data Parsers
 */
template<>
Texture parseValue(const rapidjson::Value& val, const char* alias,
		const Texture defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	Texture tex;
	if (object.IsString()) {
		tex = loadTextureFromPNG(object.GetString());
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"string");
	}
	return tex;
}

/*
 * Box2D Data Parsers
 */
template<>
b2VectorArray parseValue(const rapidjson::Value& val, const char* alias,
		const b2VectorArray defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	b2VectorArray vectorArray = { nullptr, 0 };
	if (object.IsArray()) {
		rapidjson::Value::ConstValueIterator arrayIt = object.Begin();
		std::vector<b2Vec2> vectors;
		for (; arrayIt != object.End(); ++arrayIt) {
			float x = (float) arrayIt->GetDouble();
			++arrayIt;
			KE_Assert(arrayIt != object.End(),
					"Invalid behaviour! Polygons expect to have equal amount of (x,y) pairs!");
			float y = (float) arrayIt->GetDouble();
			b2Vec2 newVec = b2Vec2(x, y);
			vectors.push_back(newVec);
		}
		vectorArray.vertices = new b2Vec2[vectors.size()];
		vectorArray.count = vectors.size();
		memcpy(vectorArray.vertices, &vectors[0],
				vectors.size() * sizeof(b2Vec2));
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json array");
	}
	return vectorArray;
}

template<>
b2BodyType parseValue(const rapidjson::Value& val, const char* alias,
		const b2BodyType defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	b2BodyType type;
	if (object.IsString()) {
		const char* string = object.GetString();
		if (::eqstr()(string, "static")) {
			type = b2_staticBody;
		} else if (::eqstr()(string, "dynamic")) {
			type = b2_dynamicBody;
		} else if (::eqstr()(string, "kinematic")) {
			type = b2_kinematicBody;
		}
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"string");
	}
	return type;
}

template<>
b2BodyDef parseValue(const rapidjson::Value& val, const char* alias,
		const b2BodyDef defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}

	const rapidjson::Value& object = val[alias];
	b2BodyDef def;
	if (object.IsObject()) {
		def.type = parseValue<b2BodyType>(object, "type");
		glm::vec2 offset = parseValue<glm::vec2>(object, "offset");
		def.position.Set(offset.x, offset.y);

		def.fixedRotation = parseValue<bool>(object, "fixedRotation", false);
		def.bullet = parseValue<bool>(object, "bullet", false);
		def.gravityScale = parseValue(object, "gravityScale", 1.0f);
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json object");
	}
	return def;
}

template<>
b2PolygonValues parseValue(const rapidjson::Value& val, const char* alias,
		const b2PolygonValues defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}

	b2PolygonValues polygonVals { nullptr, 0 };
	b2VectorArray vectors = parseValue<b2VectorArray>(val, alias);
	KE_Assert(vectors.count >= 3,
			"Polygons require at least 3 vertices! %u available.",
			vectors.count);
	polygonVals.count = vectors.count;
	polygonVals.vertices = vectors.vertices;

	return polygonVals;
}

template<>
b2ChainValues parseValue(const rapidjson::Value& val, const char* alias,
		const b2ChainValues defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}

	b2ChainValues chainVals { nullptr, 0 };
	b2VectorArray vectors = parseValue<b2VectorArray>(val, alias);
	KE_Assert(vectors.count >= 2,
			"Chains require at least 2 vertices! %u available.", vectors.count);
	chainVals.count = vectors.count;
	chainVals.vertices = vectors.vertices;
	return chainVals;
}

template<>
const b2Shape* parseValue(const rapidjson::Value& val, const char* alias,
		const b2Shape* defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	b2Shape* shape { nullptr };
	if (object.IsObject() && object.HasMember("type")) {
		const char* type = parseValue<const char*>(object, "type");
		if (::eqstr()(type, "polygon")) {
			b2PolygonShape* polygon = new b2PolygonShape();
			b2PolygonValues polyInfo = parseValue<b2PolygonValues>(object,
					"vertices");
			polygon->Set(polyInfo.vertices, polyInfo.count);
			shape = polygon;
		} else if (::eqstr()(type, "circle")) {
			b2CircleShape* circle = new b2CircleShape();
			circle->m_radius = parseValue<float>(object, "radius");
			glm::vec2 offset = parseValue<glm::vec2>(object, "offset");
			circle->m_p.Set(offset.x, offset.y);

			shape = circle;
		} else if (::eqstr()(type, "chain")) {
			b2ChainShape* chain = new b2ChainShape();
			bool isLoop = parseValue<bool>(object, "loop", false);
			b2ChainValues chainInfo = parseValue<b2ChainValues>(object,
					"vertices");
			if (!isLoop) {
				chain->CreateChain(chainInfo.vertices, chainInfo.count);
			} else {
				chain->CreateLoop(chainInfo.vertices, chainInfo.count);
			}

			shape = chain;
		}
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json object");
	}
	return shape;
}

template<>
b2FixtureDef parseValue(const rapidjson::Value& val, const char* alias,
		const b2FixtureDef defaultValue /*= DefaultValue<ParsedType>::value*/) {
	if (!val.HasMember(alias)) {
		return defaultValue;
	}
	const rapidjson::Value& object = val[alias];
	b2FixtureDef def;
	if (object.IsObject()) {
		def.density = parseValue<float>(object, "density", 1.0f);
		def.isSensor = parseValue<bool>(object, "isSensor", false);
//		def.restitution = parseValue<float>(object, "restitution", 0.0f);

		def.shape = parseValue<const b2Shape*>(object, "shape");
		KE_Assert(def.shape != nullptr,
				"Invalid behaviour! Shape is undefined!");
	} else {
		KE_CustomError("Parse error: Invalid value format! Expected %s.",
				"json object");
	}
	return def;
}
