/*
 * EntityInfoParser.h
 *
 *  Created on: Dec 14, 2013
 *      Author: Hati
 */

#ifndef ENTITYINFOPARSER_H_
#define ENTITYINFOPARSER_H_

#include "JSON.h"
#include <entity/Entity.h>

struct EntityInfoParser {

	EntityContainer& _entityContainer;
	SystemContainer& _systems;



	EntityInfoParser(EntityContainer& entityContainer, SystemContainer& systemContainer) :
		_entityContainer(entityContainer), _systems(systemContainer) {}

	/**
	 * Parses a single template file, containing a <b>single</b> template
	 * @param templateFilePath path to template file
	 * @return KE_SUCCESS on success, failure code otherwise
	 */
	KECode parseTemplate(const char* templateFilePath, EntityID* createdEntity=nullptr);
	
	KECode parseComponents(EntityInfo& entity, const rapidjson::Value& comps);
	/**
	 * Parses a single component for the given entity out of the value
	 * @param entity
	 * @param val component json object
	 * @return
	 */
	KECode parseComponent(EntityInfo& entity, const rapidjson::Value::Member& val);

	KECode parseInclude(const char* includedFilePath);
	KECode parse(const char* rootFile);


};


#endif /* ENTITYINFOPARSER_H_ */
