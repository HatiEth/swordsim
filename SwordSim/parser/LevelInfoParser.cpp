/*
 * LevelInfoParser.cpp
 *
 *  Created on: 07.01.2014
 *      Author: Hati
 */

#include "LevelInfoParser.h"
#include "EntityInfoParser.h"

KECode LevelInfoParser::parseLevel(const char* levelPath) {
	rapidjson::Document doc;
	KECode err = JSON::createRapidJsonDoc(doc, levelPath);
	if(!doc.IsObject()) {
		return KE_CustomError("Invalid level format: Expected Json Object! %s", levelPath);
	}
	KE_Notify("Parsing level %s", levelPath);
	if(doc.HasMember("entities") && doc["entities"].IsArray()) {
		parseEntities(doc["entities"]);
	}
	
	return err;
}

KECode LevelInfoParser::parseEntities(rapidjson::Value& entityArray) {
	EntityInfoParser entityParser(this->_entities, this->_systems);
	rapidjson::Document::ValueIterator it = entityArray.Begin();
	for(;it!=entityArray.End();++it) {
		/* Parse single entity */
		KECode err = parseEntity(entityParser, *it);
		if(err != KE_SUCCESS)  {
			KE_CustomWarning("LevelParse Warning: %s");
		}
	}
	
	
	return KE_SUCCESS;
}

KECode LevelInfoParser::parseEntity(EntityInfoParser& entityParser,
		rapidjson::Value& entityVals) {
	if(!entityVals.HasMember("template")) {
		return KE_FAILURE;
	}
	
	rapidjson::Value& templatePath = entityVals["template"];
	EntityID createdEntity;
	entityParser.parseTemplate(templatePath.GetString(), &createdEntity);
	if(entityVals.HasMember("components") && entityVals["components"].IsObject()) {
		rapidjson::Value& components = entityVals["components"];
		EntityInfo& entity = this->_entities.entity(createdEntity);
		entityParser.parseComponents(entity, components);
	}
	
	
	
	return KE_SUCCESS;
}
