/*
 * JSON.h
 *
 *  Created on: 07.01.2014
 *      Author: Hati
 */

#ifndef JSON_H_
#define JSON_H_

#include <rapidjson/document.h>
#include <Utility.h>

struct JSON {
	static KECode createRapidJsonDoc(rapidjson::Document& doc, const char* path);

}; /* JSON */
#endif /* JSON_H_ */
