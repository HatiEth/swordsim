/*
 * ParseInfo.h
 *
 *  Created on: Dec 11, 2013
 *      Author: Hati
 */

#ifndef PARSEINFO_H_
#define PARSEINFO_H_

#include <components/Component.h>
#include <rapidjson/document.h>
#include <entity/Entity.h>
#include <Utility.h>
#include <typeinfo.h>

/**
 * Contains information on how-to parse a single component by name
 */
struct ParseInfo {
	// CompAlias : ParseFunction
	typedef void (*ParseFunction)(Component*, const rapidjson::Value&);
	typedef eth::StringMap<ParseFunction>::Type ParseMap;
	typedef eth::StringMap<ParseFunction>::Type MergeMap;

	static MergeMap& mergers() {
		static MergeMap  sMergers;
		return sMergers;
	}
	static ParseMap& parsers() {
		static ParseMap sParsers;
		return sParsers;
	}

					/* val.name.GetString(), entity, rapidjson::Value& values); */
	static Component* parse(const char* compAlias, EntityInfo& entity, const rapidjson::Value& values) {
		if(entity.has(compAlias)) {
			KE_CustomWarning("Parsing warning: Merging component %s at %u", compAlias, entity._id);
			KE_Assert(mergers().find(compAlias)!=mergers().end(), "Merge definition for component %s not found!", compAlias);
			mergers()[compAlias](entity.get(compAlias), values);

			return entity.get(compAlias);
		}
		else {
			Component* comp = ComponentFactory::create(compAlias);
			if(parsers().find(compAlias) != parsers().end()) {
				KE_Notify("Parsing json data into component %s", compAlias);
				parsers()[compAlias](comp, values);

			}
			else {
				KE_CustomError("Parsing error: No parsing defined for component %s!", compAlias);
			}
			return comp;
		}
	}

	template<typename C>
	struct ParseRegistration {
		static void parse(Component* comp, const rapidjson::Value& values);

		static ParseFunction registerParse(const char* componentAlias) {
			if (parsers().find(componentAlias) != parsers().end()) {
				KE_Notify("Overwriting creationAlias %s with %s",
						parsers().find(componentAlias)->first, componentAlias);
			} else {
				KE_Notify("Registering %s parsing info", componentAlias);
			}

			return parsers()[componentAlias] = parse;
		}

		static ParseFunction _parseFunction;
	};

	template<typename C>
	struct MergeRegistration {
		static void merge(Component* merged, const rapidjson::Value& values);

		static ParseFunction registerMerge(const char* compAlias) {
			if (mergers().find(compAlias) != mergers().end()) {
				KE_Notify("Overwriting creationAlias %s with %s",
						mergers().find(compAlias)->first, compAlias);
			} else {
				KE_Notify("Registering %s parsing info", compAlias);
			}

			return mergers()[compAlias] = merge;
		}

		static ParseFunction _mergeFunction;
	};
};

///@todo make more handy to use
/// PARSE
#define PARSE_COMPONENT(ComponentType) template<> void ParseInfo::ParseRegistration<ComponentType>::parse(Component* comp, const rapidjson::Value& values)
#define PARSE_CAST_COMPONENT(ComponentType) (ComponentType*)comp
#define DEFINE_PARSE(ComponentType) template<> ParseInfo::ParseFunction ParseInfo::ParseRegistration<ComponentType>::_parseFunction = ParseInfo::ParseRegistration<ComponentType>::registerParse(COMPONENT_ALIAS(ComponentType))
/// MERGE
#define MERGE_COMPONENT(ComponentType) template<> void ParseInfo::MergeRegistration<ComponentType>::merge(Component* _merged, const rapidjson::Value& values)
#define MERGE_CAST_COMPONENT(ComponentType) ComponentType* merged = (ComponentType*)_merged;
#define DEFINE_MERGE(ComponentType) template<> ParseInfo::ParseFunction ParseInfo::MergeRegistration<ComponentType>::_mergeFunction = ParseInfo::MergeRegistration<ComponentType>::registerMerge(COMPONENT_ALIAS(ComponentType))

template <typename ValType>
struct DefaultValue {
	static ValType value;
};

template<typename ParsedType>
ParsedType parseValue(const rapidjson::Value& val, const char* alias, const ParsedType defaultValue = DefaultValue<ParsedType>::value);

#endif /* PARSEINFO_H_ */
