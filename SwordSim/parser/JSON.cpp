/*
 * JSON.cpp
 *
 *  Created on: 07.01.2014
 *      Author: Hati
 */

#include "JSON.h"

KECode JSON::createRapidJsonDoc(rapidjson::Document& doc, const char* path) {
	eth::File file;
	KECode err = eth::readFile(path, file);
	if (err != KE_SUCCESS) {
		KE_CustomWarning("Unable to parse entity file %s! File unreadable!",
				path);
		return err;
	}

	doc.ParseInsitu<0>(file.content);
	if (doc.HasParseError()) {
		return KE_CustomError("Json parsing error: %s", doc.GetParseError());
	}
	return KE_SUCCESS;
}
