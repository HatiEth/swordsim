/*
 * b2VertexValues.h
 *
 *  Created on: 17.12.2013
 *      Author: Hati
 */

#ifndef B2VERTEXVALUES_H_
#define B2VERTEXVALUES_H_


#include <Box2D/Box2D.h>
#include <memory.h>

template<size_t RequiredVerts>
struct b2VertexValues {
	b2Vec2* vertices;
	int count;
};

using b2VectorArray = b2VertexValues<0>;
using b2ChainValues = b2VertexValues<2>;
using b2PolygonValues = b2VertexValues<3>;

template<size_t N>
struct FixedFloatArray {
	float vertices[N];
	FixedFloatArray(float dft = 0.0f) {
		int *c = (reinterpret_cast<int*>(&dft));
		memset(vertices, *c, sizeof(vertices));
	}
};


#endif /* B2VERTEXVALUES_H_ */
