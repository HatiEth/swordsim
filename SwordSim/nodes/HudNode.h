/*
 * HudNode.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef HUDNODE_H_
#define HUDNODE_H_

#include "Node.h"
#include <components/HudComponent.h>
#include <components/TransformComponent.h>


struct HudNode : public Node {
	HudComponent* hudComponent;
	TransformComponent* transform;
};


#endif /* HUDNODE_H_ */
