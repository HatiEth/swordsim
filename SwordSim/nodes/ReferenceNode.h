/*
 * ReferenceNode.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#ifndef REFERENCENODE_H_
#define REFERENCENODE_H_


#include "Node.h"
#include <components/ReferenceComponent.h>

struct ReferenceNode : public Node {
	ReferenceComponent* reference;
};


#endif /* REFERENCENODE_H_ */
