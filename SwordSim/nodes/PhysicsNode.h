/*
 * PhysicsNode.h
 *
 *  Created on: 16.12.2013
 *      Author: Hati
 */

#ifndef PHYSICSNODE_H_
#define PHYSICSNODE_H_

#include "Node.h"
#include <components/TransformComponent.h>
#include <components/PhysicsComponent.h>

struct PhysicsNode : public Node {
	TransformComponent* transform;
	PhysicsComponent* physics;
};



#endif /* PHYSICSNODE_H_ */
