/*
 * NodeInfo.h
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#ifndef NODEINFO_H_
#define NODEINFO_H_

#include <components/Component.h>
#include <Utility.h>
struct Node;
/**
 * Defines which components are required by a certain node
 */
template<typename nodeType>
struct NodeInfo {
	static_assert(std::tr1::is_base_of<Node, nodeType>::value, "Trying to register nodeinfo for non-node type!");

	typedef eth::StringHashSet RequiredComponentSet;



	static RequiredComponentSet& requirements() {
		static RequiredComponentSet _requirements;
		return _requirements;
	}

	static NodeInfo<nodeType>& get() {
		return _nodeInfo;
	}

	static NodeInfo<nodeType>& add(const char* componentAlias) {
		requirements().insert(componentAlias);
		return _nodeInfo;
	}

	template<typename C>
	static NodeInfo<nodeType>& add() {
		requirements().insert(COMPONENT_ALIAS(C));
		return _nodeInfo;
	}

	static bool requires(const char* componentAlias) {
		return requirements().find(componentAlias)!=requirements().end();

	}


	static NodeInfo<nodeType> _nodeInfo;
};
/// Used to register required components for nodes
#define NODE_REQUIRE(NodeType) template<> NodeInfo<NodeType> NodeInfo<NodeType>::_nodeInfo = NodeInfo<NodeType>::get()

#define NODE_REQUIREMENT_ANY "#ANY#"

#endif /* NODEINFO_H_ */
