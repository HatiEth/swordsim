/*
 * EquipmentNode.h
 *
 *  Created on: Feb 17, 2014
 *      Author: Hati
 */

#ifndef EQUIPMENTNODE_H_
#define EQUIPMENTNODE_H_

#include "Node.h"
#include <components/TransformComponent.h>
#include <components/EquipmentComponent.h>


/**
 * Equipment nodes represent an entity which can have items equipped (used by
 */
struct EquipmentNode : public Node {
	TransformComponent* tc;
	EquipmentComponent* ec;
};



#endif /* EQUIPMENTNODE_H_ */
