/*
 * PlayerNode.h
 *
 *  Created on: Feb 15, 2014
 *      Author: Hati
 */

#ifndef PLAYERNODE_H_
#define PLAYERNODE_H_

#include "Node.h"
#include <components/PlayerComponent.h>
#include <components/PhysicsComponent.h>
#include <components/TransformComponent.h>

struct PlayerNode : public Node {
	PlayerComponent* player;
	PhysicsComponent* physics;
	TransformComponent* transform;
};


#endif /* PLAYERNODE_H_ */
