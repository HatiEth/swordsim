/*
 * ItemNode.h
 *
 *  Created on: Feb 16, 2014
 *      Author: Hati
 */

#ifndef ITEMNODE_H_
#define ITEMNODE_H_

#include "Node.h"
#include <components/ItemComponent.h>
#include <components/TransformComponent.h>

struct ItemNode : public Node {
	ItemComponent* ic;
	TransformComponent* tc;
};

#endif /* ITEMNODE_H_ */
