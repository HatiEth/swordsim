/*
 * Nodes.cpp
 *
 *  Created on: Dec 15, 2013
 *      Author: Hati
 */

#include "Node.h"
#include "NodeInfo.h"

#include "RenderNode.h"
NODE_REQUIRE(RenderNode).add(COMPONENT_ALIAS(TransformComponent)).add(COMPONENT_ALIAS(SpriteComponent));
template<>
void fillNode<RenderNode>(RenderNode& node, EntityInfo& info) {
	node.position = info.get<TransformComponent>(
			COMPONENT_ALIAS(TransformComponent));
	node.sprite = info.get<SpriteComponent>(COMPONENT_ALIAS(SpriteComponent));
}

#include "HealthNode.h"
NODE_REQUIRE(HealthNode).add(COMPONENT_ALIAS(HealthComponent));
template<>
void fillNode<HealthNode>(HealthNode& node, EntityInfo& info) {
	node.hc = info.get<HealthComponent>(COMPONENT_ALIAS(HealthComponent));
}

#include "HudNode.h"
NODE_REQUIRE(HudNode).add(COMPONENT_ALIAS(HudComponent)).add(COMPONENT_ALIAS(TransformComponent));
template<>
void fillNode<HudNode>(HudNode& node, EntityInfo& info) {
	node.hudComponent = info.get<HudComponent>(COMPONENT_ALIAS(HudComponent));
	node.transform = info.get<TransformComponent>(
			COMPONENT_ALIAS(TransformComponent));
}

#include "ReferenceNode.h"
NODE_REQUIRE(ReferenceNode).add(COMPONENT_ALIAS(ReferenceComponent));
template<>
void fillNode<ReferenceNode>(ReferenceNode& node, EntityInfo& info) {
	node.reference = info.get<ReferenceComponent>(
			COMPONENT_ALIAS(ReferenceComponent));
}

#include "PhysicsNode.h"
NODE_REQUIRE(PhysicsNode).add(COMPONENT_ALIAS(PhysicsComponent)).add(COMPONENT_ALIAS(TransformComponent));
template<>
void fillNode<PhysicsNode>(PhysicsNode& node, EntityInfo& info) {
	node.physics = info.get<PhysicsComponent>(
			COMPONENT_ALIAS(PhysicsComponent));
	node.transform = info.get<TransformComponent>(
			COMPONENT_ALIAS(TransformComponent));
}

#include "PlayerNode.h"
NODE_REQUIRE(PlayerNode).add(COMPONENT_ALIAS(PlayerComponent)).add(COMPONENT_ALIAS(PhysicsComponent)).add(COMPONENT_ALIAS(TransformComponent)); // requires transform + physics to actual have a body!
template<>
void fillNode<PlayerNode>(PlayerNode& node, EntityInfo& info) {
	node.physics = info.get<PhysicsComponent>(
			COMPONENT_ALIAS(PhysicsComponent));
	node.player = info.get<PlayerComponent>(
			COMPONENT_ALIAS(PlayerComponent));
	node.transform = info.tget<TransformComponent>();

}

#include "ItemNode.h"
NODE_REQUIRE(ItemNode).add(COMPONENT_ALIAS(ItemComponent)).add(COMPONENT_ALIAS(TransformComponent));
template<>
void fillNode(ItemNode& node, EntityInfo& info) {
	node.ic = info.tget<ItemComponent>();
	node.tc = info.tget<TransformComponent>();
}

#include "EquipmentNode.h"
NODE_REQUIRE(EquipmentNode).add<TransformComponent>().add<EquipmentComponent>();
ethDefineFillNode(EquipmentNode) {
	node.ec = entity.tget<EquipmentComponent>();
	node.tc = entity.tget<TransformComponent>();
}

#include "DebugNode.h"
NODE_REQUIRE(DebugNode).add(NODE_REQUIREMENT_ANY);
ethDefineFillNode(DebugNode) {}
