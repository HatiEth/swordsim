/*
 * Node.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Hati
 */

#ifndef NODE_H_
#define NODE_H_

#include <vector>
#include <deque>

#include <entity/Entity.h>
#include <Utility.h>

/**
 * @todo maybe generate something to create nodes by macro ?
 */

struct Node {
	EntityID _internalId;
	EntityInfo* _internalPtr;
};


template<typename NodeType>
inline void ethInitNode(NodeType& node, EntityInfo& entity) {
	static_assert(std::tr1::is_base_of<Node, NodeType>::value, "Trying to init non-node type with node-type function! See compiler output!");
	node._internalId = entity._id;
	node._internalPtr = &entity;
}

template<typename NodeType>
inline void ethInitNode(NodeType* node, EntityInfo& entity) {
	static_assert(std::tr1::is_base_of<Node, NodeType>::value, "Trying to init non-node type with node-type function! See compiler output!");
	ethInitNode(*node, entity);
}

template<typename NodeType>
void fillNode(NodeType& node, EntityInfo& entity);

template<typename NodeType>
void fillNode(NodeType* node, EntityInfo& entity) {
	static_assert(std::tr1::is_base_of<Node, NodeType>::value, "Trying to fill non-node type with node-type function! See compiler output!");
	::fillNode(*node, entity);
}

#define ethDefineFillNode(NodeType) template<> void fillNode(NodeType& node, EntityInfo& entity)


#endif /* NODE_H_ */
