/*
 * Entity.cpp
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#include "Entity.h"
#include <systems/System.h>
#include <components/Component.h>
#include <systems/SystemRequirements.h>

void registerNodesForEntity(const char* compName, EntityInfo& entity,
		SystemContainer& systems, bool overwrite = false) {
	auto sysIt = systems.begin();
	for (; sysIt != systems.end(); ++sysIt) { // for-each system
		if (overwrite && SystemRequirements::requires(sysIt->first, compName)) { // overwrite existing node
			KE_CustomWarning(
					"External component (%s, %u) manipulation of existing node %s.",
					compName, entity._id, sysIt->first);
			sysIt->second->updateNode(entity);
		} else if (SystemRequirements::requires(sysIt->first, compName) // system ben�tigt component
		&& SystemRequirements::checkNodeGeneration(sysIt->first, // und alle anderen sind bereits vorhanden
				entity)) { // first creation
			sysIt->second->createNode(entity);
		} else { // if requirements are not met

		}
	}
}

void EntityInfo::add(const char* compName, SystemContainer& systems) {
	bool overwritten = false;
	if (_components.find(compName) != _components.end()) {
		KE_CustomWarning("Overwriting %s at entity %u", compName, _id);
		overwritten = true;
	} else {
		KE_Notify("Adding %s at entity %u", compName, _id);
		Component* comp = ComponentFactory::create(compName);
		_components[compName] = comp;
	}

	registerNodesForEntity(compName, *this, systems, overwritten);
}

void EntityInfo::inject(const char* compName, SystemContainer& systems,
		Component* comp) {
	bool overwritten = false;
	if (_components.find(compName) != _components.end()) {
		/// @bug memory leak, due to new assignment each overwrite
		KE_Notify("Merging %s in entity %u", compName, _id);
		overwritten = true;

	} else {
		KE_Notify("Adding %s at entity %u", compName, _id);
		_components[compName] = comp;
	}

	registerNodesForEntity(compName, *this, systems, overwritten);
}

void EntityInfo::remove(const char* compName, SystemContainer& systems) {

	KE_Assert(_components.find(compName) != _components.end(),
			"Trying to remove unavailable component %s in %u", compName, _id);

	Component* c = _components[compName];
	_components.erase(compName);
	auto sysIt = systems.begin();
	for (; sysIt != systems.end(); ++sysIt) { // for-each system
		if (SystemRequirements::requires(sysIt->first, compName)// system ben�tigt component
		&& !SystemRequirements::checkNodeGeneration(sysIt->first, *this)) { //entity hat aber nicht mehr alle anderen
			///@todo remove node if components were removed afterwards
			sysIt->second->removeNode(*this);
		}
	}
	delete c;

	KE_Notify("Removed component %s from %u", compName, _id);
}

