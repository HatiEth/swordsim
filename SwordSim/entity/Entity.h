/*
 * Entity.h
 *
 *  Created on: Nov 1, 2013
 *      Author: Hati
 */

#ifndef ENTITY_H_
#define ENTITY_H_
#include <Utility.h>
#include <systems/System.h>
#include <components/Component.h>
struct Component;

typedef unsigned int EntityID;

// name - component map


struct EntityInfo {
	typedef eth::StringMap<Component*>::Type Components;
	EntityID _id;
	Components _components;

	template<typename C = Component>
	C* get(const char* compName) {
		KE_Assert(_components.find(compName)!=_components.end(), "Trying to get an invalid component %s in %u!", compName, _id);
		return (C*)_components[compName];
	}

	template<typename C = Component>
	const C* get(const char* compName) const {
		KE_Assert(_components.find(compName)!=_components.end(), "Trying to get an invalid component %s in %u!", compName, _id);
		auto fIt = _components.find(compName);
		return (const C*)*fIt;
	}

	void add(const char* compName, SystemContainer& systems);
	void inject(const char* compName, SystemContainer& systems, Component* comp);

	void remove(const char* compName, SystemContainer& systems);

	bool has(const char* compName) const {
		return _components.find(compName)!=_components.end();
	}


	/**
	 * Special get function for components using internal COMPONENT_ALIAS ..
	 * @return getted component if available
	 */
	template<typename C>
	C* tget() {
		static const char* CNAME = COMPONENT_ALIAS(C);
		KE_Assert(_components.find(CNAME)!=_components.end(), "Trying to get an invalid component %s in %u!", CNAME, _id);
		return (C*)_components[CNAME];
	}

	template<typename C>
	const C* tget() const {
		static const char* CNAME = COMPONENT_ALIAS(C);
		KE_Assert(_components.find(CNAME)!=_components.end(), "Trying to get an invalid component %s in %u!", CNAME, _id);
		auto fIt = _components.find(CNAME);
		return (const C*)*fIt;
	}

	template<typename C>
	bool thas() const {
		static const char* CNAME = COMPONENT_ALIAS(C);
		return _components.find(CNAME)!=_components.end();
	}
};

struct EntityContainer {
	typedef std::tr1::unordered_map<EntityID, EntityInfo*> Entities;

	Entities _entities;


	EntityInfo& entity(EntityID e) {
		return *_entities[e];
	}

	EntityID _nextId{0};

	EntityID createEntity() {
		EntityInfo* ei = new EntityInfo();
		ei->_id = _nextId;
		_entities[_nextId++] = ei;
		return ei->_id;
	}

	void destroyEntity(EntityID eId) {
		KE_Notify("Destroy entity %u", eId);
		EntityInfo* e = _entities[eId];

		_entities.erase(eId);

		delete e;

	}
};


#endif /* ENTITY_H_ */
