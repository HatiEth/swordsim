/*
 * AABB.h
 *
 *  Created on: Jul 9, 2013
 *      Author: Hati
 */

#ifndef AABB_H_
#define AABB_H_
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl/gl.h>

struct AABB {
	glm::vec2 position;
	glm::vec2 halfExtent;

	AABB(const glm::vec2& pos, const glm::vec2& halfExtent) :
			position(pos), halfExtent(halfExtent) {
	}

	AABB(const AABB& rh) :
			position(rh.position), halfExtent(rh.halfExtent) {

	}

	~AABB() {
	}

	bool contains(const glm::vec2& point) {
		if (point.x > position.x + halfExtent.x)
			return false;
		if (point.x < position.x - halfExtent.x)
			return false;
		if (point.y > position.y + halfExtent.y)
			return false;
		if (point.y < position.y - halfExtent.y)
			return false;

		return true;
	}

	void move(const glm::vec2& newPos) {
		position = newPos;
	}

	bool intersects(const AABB& aabb) {
		glm::vec2 test = glm::abs(this->position - aabb.position);
//		
		if (test.x <= aabb.halfExtent.x + halfExtent.x
				&& test.y <= aabb.halfExtent.y + halfExtent.y)
			return true;
		return false;
	}

	inline glm::vec2 left(void) const {
		return this->position - glm::vec2(halfExtent.x, 0);
	}

	inline glm::vec2 right(void) const {
		return this->position + glm::vec2(halfExtent.x, 0);
	}

	inline glm::vec2 top(void) const {
		return this->position + glm::vec2(0, halfExtent.y);
	}

	inline glm::vec2 bottom(void) const {
		return this->position - glm::vec2(0, halfExtent.y);
	}

	void debugDraw(glm::vec4 borderColor = glm::vec4(1, 0, 0, 1), bool filled =
			false, glm::vec4 fillColor = glm::vec4(0.1, 0.1, 0.1, 0.3)) {
		glColor4f(borderColor.r, borderColor.g, borderColor.b, borderColor.a);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2f(position.x - halfExtent.x, position.y - halfExtent.y);
			glVertex2f(position.x + halfExtent.x, position.y - halfExtent.y);
			glVertex2f(position.x + halfExtent.x, position.y + halfExtent.y);
			glVertex2f(position.x - halfExtent.x, position.y + halfExtent.y);
		}
		glEnd();

		if (filled) {
			glColor4f(fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			glBegin(GL_QUADS);
			{
				glVertex2f(position.x - halfExtent.x,
						position.y - halfExtent.y);
				glVertex2f(position.x + halfExtent.x,
						position.y - halfExtent.y);
				glVertex2f(position.x + halfExtent.x,
						position.y + halfExtent.y);
				glVertex2f(position.x - halfExtent.x,
						position.y + halfExtent.y);
			}
			glEnd();
		}

	}
};

#endif /* AABB_H_ */
