/*
 * Rotation.h
 *
 *  Created on: Jul 9, 2013
 *      Author: Hati
 */

#ifndef ROTATION_H_
#define ROTATION_H_

#include <glm/glm.hpp>

namespace eth {
    struct Rotation {
        Rotation(void) :
                s(0.f), c(1.f) {
        }

        Rotation(float degrees) {
            const float rad = glm::radians(degrees);
            s = glm::sin(rad);
            c = glm::cos(rad);
        }

        ~Rotation(void) {
        }

        /// sine representation of complete rotation
        float s;
        /// cosine representation of complete rotation
        float c;
    };
}

glm::vec2 operator*(const glm::vec2& v, const float& inDegree) {
    const float rad = glm::radians(inDegree);
    glm::vec2 rotated(glm::cos(rad), glm::sin(rad));

    return glm::vec2(v.x * rotated.x - v.y * rotated.y, v.y * rotated.x + v.x * rotated.y);
}

glm::vec2 operator*(const glm::vec2& v, const eth::Rotation& keRot) {
    return glm::vec2(v.x * keRot.c - v.y * keRot.s, v.y * keRot.c + v.x * keRot.s);
}

glm::vec2& operator*=(glm::vec2& v, const eth::Rotation& keRot) {
    v.x = v.x * keRot.c - v.y * keRot.s;
    v.y = v.y * keRot.c + v.x * keRot.s;
    return v;
}

#endif /* ROTATION_H_ */
