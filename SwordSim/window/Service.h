/*
 * Service.h
 *
 *  Created on: Nov 18, 2013
 *      Author: Hati
 */

#ifndef SERVICE_H_
#define SERVICE_H_

#include <Utility.h>

template<typename T>
struct service {
	static inline T*& get() {
		static T* gService = nullptr; ///@todo proper nullservice here
		return gService;
	}

	static inline void provide(T* serv) {
		get() = serv;
		KE_Assert(get()!=nullptr, "Trying to set service to nullptr! Invalid behaviour!");
		KE_Notify("Registered service.. mem_ptr: %u", serv);
	}
};


#endif /* SERVICE_H_ */
