/*
 * InputManager.h
 *
 *  Created on: Nov 17, 2013
 *      Author: Hati
 */

#ifndef INPUTMANAGER_H_
#define INPUTMANAGER_H_

#include <window/Service.h>
#include <GLFW/glfw3.h>
#include <Utility.h>


struct ButtonStatus { // 1 byte data structure for keys
	uint8_t cached : 1;
	uint8_t wasReleased : 1; // flag if previously was released
	uint8_t wasPressed : 1; // flag if previously was pressed
	uint8_t isPushed : 1; // flag if pressed in current frame
	uint8_t isReleased : 1; // flag if released in current frame
};



/**
 * inputservice for glfw
 */
class InputService {
	GLFWwindow& mWindowRef;


	typedef std::tr1::unordered_map<int, ButtonStatus> Buttons;
	Buttons keyboardMap;

	void cacheKey(ButtonStatus& currentStatus, int keyCode) {
		int keyStatus = glfwGetKey(&mWindowRef, keyCode);
		if ((keyStatus == GLFW_PRESS)) {
			currentStatus.isPushed = !currentStatus.wasPressed;
			currentStatus.wasPressed = 1;
			currentStatus.wasReleased = 0;
		}
		else {
			currentStatus.isReleased = !currentStatus.wasReleased;
			currentStatus.wasReleased = 1;
			currentStatus.wasPressed = 0;
		}
		currentStatus.cached = 1;
	}
public:
	InputService(GLFWwindow& window) :
			mWindowRef(window) {
	}
	~InputService() {
	}

	bool isKeyPushed(int keyCode) {
		auto it = keyboardMap.find(keyCode);
		if(it==keyboardMap.end()) {
			ButtonStatus& newStatus = keyboardMap[keyCode];
			newStatus = {0,0,0,0,0};
		}
		ButtonStatus& currentStatus = keyboardMap[keyCode];
		if(currentStatus.cached) {
			return currentStatus.isPushed;
		}
		else {
			cacheKey(currentStatus, keyCode);
		}
		return currentStatus.isPushed;
	}


	void uncache() {
		auto it = keyboardMap.begin();
		for(;it!=keyboardMap.end();++it) {
			it->second.cached = 0;
		}
	}
};

#endif /* INPUTMANAGER_H_ */
