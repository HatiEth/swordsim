/*
 * Window.cpp
 *
 *  Created on: Aug 7, 2013
 *      Author: Hati
 */

#include "Window.h"


void error_callback(int errCode, const char* errMsg) {
    KE_Error(KE_FAILURE_STR_NUM, errMsg, errCode);
}

bool toggleCam = false;
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
//    glfwInterface* interface = (glfwInterface*)glfwGetWindowUserPointer(window);
//    if(action == GLFW_RELEASE) {
//    float fac = 0.001f;
    if(action==GLFW_PRESS) {
        switch(key) {
        case GLFW_KEY_KP_DIVIDE:
            toggleCam=!toggleCam;
            break;
        case GLFW_KEY_KP_ADD:
            return;
        case GLFW_KEY_KP_SUBTRACT:
            return;
        }
    }
//    }
    
    ///@todo use to get modifiers here
}
void mouse_callback(GLFWwindow* window, int key, int action, int mods) {
//    glfwInterface* interface = (glfwInterface*)glfwGetWindowUserPointer(window);
    double posX, posY;
    glfwGetCursorPos(window, &posX, &posY);
}

void framebufferSize_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
//    glfwInterface* userptr = (glfwInterface*)glfwGetWindowUserPointer(window);
    
}
