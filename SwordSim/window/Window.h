/*
 * Window.h
 *
 *  Created on: Jul 8, 2013
 *      Author: Hati
 */

#ifndef WINDOW_H_
#define WINDOW_H_
#include <gl/glew.h>
#include <window/glfwInterface.h>
#include "../Utility.h"
#include <states/StateMachine.h>
#include <states/DemoState.h>
#include <states/GameState.h>
#include <states/SwordCombatTestState.h>
#include <states/TextureAdvectionState.h>
#include <states/CutEffectState.h>

#include "InputService.h"

void error_callback(int errCode, const char* errMsg);
void key_callback(GLFWwindow* window, int key, int scancode, int action,
		int mods);
void mouse_callback(GLFWwindow* window, int key, int action, int mods);
void framebufferSize_callback(GLFWwindow* window, int width, int height);

#define WINDOW_TITLE "Medienprojekt"

class Window {
	glfwInterface dataInterface;
public:
	Window() :
			dataInterface() {

	}

	~Window() {
	}

	KECode create(int argc, char** argv) {
		if (!glfwInit())
			return KE_Error(KE_FAILURE_STR, "Unable to initialize glfw!");

		KE_Notify("Initialized glfw%s!", glfwGetVersionString());

		glfwSetErrorCallback(error_callback);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		dataInterface.glfwWindow = glfwCreateWindow(800, 600, WINDOW_TITLE,
		NULL, NULL);

		KE_Notify("Using OpenGL%d.%d",
				glfwGetWindowAttrib(dataInterface.glfwWindow, GLFW_CONTEXT_VERSION_MAJOR),
				glfwGetWindowAttrib(dataInterface.glfwWindow,GLFW_CONTEXT_VERSION_MINOR));

		if (!dataInterface.glfwWindow) {
			glfwTerminate();
			return 1;
		}

		glfwMakeContextCurrent(dataInterface.glfwWindow);

		glewExperimental = GL_TRUE;
		glewInit();
		KE_Notify("Using glew%s", glewGetString(GLEW_VERSION));

		glfwSetKeyCallback(dataInterface.glfwWindow, key_callback);
		glfwSetMouseButtonCallback(dataInterface.glfwWindow, mouse_callback);
		glfwSetFramebufferSizeCallback(dataInterface.glfwWindow,
				framebufferSize_callback);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glfwSetWindowUserPointer(dataInterface.glfwWindow, &dataInterface);

		this->dataInterface._states = new StateMachine();
		this->dataInterface._states->registerState<DemoState>("demostate",
				dataInterface.glfwWindow);
		this->dataInterface._states->registerState<SwordCombatTestState>(
				"swordtest", dataInterface.glfwWindow);
		this->dataInterface._states->registerState<TextureAdvectionState>(
				"textureAdvection", dataInterface.glfwWindow);
		this->dataInterface._states->registerState<CutEffectState>("cutEffect",
				dataInterface.glfwWindow);
		this->dataInterface._states->registerState<GameState>("gamestate",
				dataInterface.glfwWindow);

		if (argc < 2
				|| KE_SUCCESS != dataInterface._states->changeState(argv[1])) {
			dataInterface._states->changeState("demostate");
		}

		InputService* inputService = new InputService(
				*this->dataInterface.glfwWindow);
		service<InputService>::provide(inputService);
		KE_Assert(service<InputService>::get() != nullptr,
				"Input service failed to initialize!");
		InputService* input = service<InputService>::get();
		KE_Assert(input != nullptr, "Input service failed to initialize!");

		return KE_SUCCESS;
	}

	KECode start() {

		FloatTimer timer = FloatTimer(1 / 60.f);

		glfwSetTime(0.0);

		while (!glfwWindowShouldClose(dataInterface.glfwWindow)) {
			glfwPollEvents();

			timer = (float) glfwGetTime();

			timer += timer.elapsedTime;
			while (timer.timeAccumulator > timer.updateFrequency) {
				timer -= timer.updateFrequency;
				dataInterface._states->update(timer.updateFrequency);
				service<InputService>::get()->uncache();
			}

			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

			dataInterface._states->render();

			glfwSwapBuffers(dataInterface.glfwWindow);

		}

		return KE_SUCCESS;
	}
};

#endif /* WINDOW_H_ */
