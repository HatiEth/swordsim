/*
 * glfwInterface.h
 *
 *  Created on: Sep 5, 2013
 *      Author: Hati
 */

#ifndef GLFWINTERFACE_H_
#define GLFWINTERFACE_H_
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <states/StateMachine.h>


struct glfwInterface {
    GLFWwindow* glfwWindow;
    StateMachine* _states;
};


#endif /* GLFWINTERFACE_H_ */
