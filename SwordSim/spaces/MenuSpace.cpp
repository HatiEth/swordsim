/*
 * MenuSpace.cpp
 *
 *  Created on: Nov 3, 2013
 *      Author: Hati
 */

#include "MenuSpace.h"
#include <gl/glew.h>

template<>
KECode spaces<MenuSpace>::initialize(MenuSpace& space) {
	space._camera.resize(800, 600);
	space._camera.setZoom(0.8f);


	KE_Notify("Initialized menu space!");
	return KE_SUCCESS;
}

template<>
void spaces<MenuSpace>::update(MenuSpace& space, float dt) {
	space._camera.update(dt);
}


template<>
void spaces<MenuSpace>::render(MenuSpace& space) {
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMultMatrixf(space._camera.valuePtr());
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();




	glColor3f(1,0,0);
	glBegin(GL_TRIANGLE_STRIP); {
		glVertex2f(-0.5f, -0.5f);
		glVertex2f(-0.5f, 0.5f);
		glVertex2f(0.5f, -0.5f);
		glVertex2f(0.5f, 0.5f);
	} glEnd();

	glPopMatrix();
}
