/*
 * HudSpace.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Hati
 */

#include "HudSpace.h"
#include <render/Texture.h>
#include <render/debugDraw.h>
#include <parser/EntityInfoParser.h>
#include <parser/LevelInfoParser.h>

template<>
KECode spaces<HudSpace>::initialize(HudSpace& space) {
	space.renderSystem = new RenderSystem();
	space.systems[SYSTEM_NAME(RenderSystem)] = space.renderSystem;


	space.mCamera.setZoom(0.0f);
	space.mCamera.resize(800, 600);
	space.mCamera.update(0.0f);

	space.hudSystem = new HudSystem(space._referenceSystem);
	space.systems[SYSTEM_NAME(HudSystem)] = space.hudSystem;

	space.renderSystem = new RenderSystem();
	space.systems[SYSTEM_NAME(RenderSystem)] = space.renderSystem;

	LevelInfoParser levelParser(space.entities, space.systems);
	levelParser.parseLevel("levels/hudLevel.json");


	space.hudSystem->linkHud();

	return KE_SUCCESS;
}

template<>
void spaces<HudSpace>::update(HudSpace& space, float dt) {
	space.mCamera.update(dt);

	ethSystemExecute(*space.hudSystem, dt);
}

template<>
void spaces<HudSpace>::render(HudSpace& space) {
	useCamera(space.mCamera);

	ethSystemExecute(*space.renderSystem, 0.0f);


	glPopMatrix();
}

template<>
KECode spaces<HudSpace>::dispose(HudSpace& space) {
	return KE_SUCCESS;
}

