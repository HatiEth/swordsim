/*
 * FBODemoSpace.h
 *
 *  Created on: Nov 6, 2013
 *      Author: Hati
 */

#ifndef FBODEMOSPACE_H_
#define FBODEMOSPACE_H_
#include "Space.h"
#include <GL/glew.h>

struct FBODemoSpace : public Space {
	GLuint fb;
	GLuint depth_rb;
	GLuint color_tex;
};

#endif /* FBODEMOSPACE_H_ */
