/*
 * HudSpace.h
 *
 *  Created on: Nov 17, 2013
 *      Author: Hati
 */

#ifndef HUDSPACE_H_
#define HUDSPACE_H_

#include "Space.h"
#include <render/Camera.h>
#include <systems/Systems.h>
/**
 * HUD space for game behaviour
 */
struct HudSpace: public Space {
	Camera mCamera;

	ReferenceSystem& _referenceSystem;



	SystemContainer systems;
	EntityContainer entities;

	HudSystem* hudSystem;
	RenderSystem* renderSystem;

	HudSpace(ReferenceSystem& refSystem)
		: _referenceSystem(refSystem), hudSystem(nullptr), renderSystem(nullptr)
	{ }
};

#endif /* HUDSPACE_H_ */
