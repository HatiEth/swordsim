/*
 * GameSpace.h
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#ifndef GAMESPACE_H_
#define GAMESPACE_H_

#include "Space.h"
#include "HudSpace.h"
#include <systems/Systems.h>


// subspaces
#include "gamespaces/ItemSpace.h"
#include "gamespaces/DebugSpace.h"

/**
 * Real space for game logic and game behaviour
 */
struct GameSpace : public Space {
	HudSpace* _hud; /// pause space?
	Camera mCamera;

	SystemContainer systems;
	EntityContainer entities;

	RenderSystem* renderSystem;
	ReferenceSystem* referenceSystem;

	HealthSystem* healthSystem;
	PhysicsSystem* physicsSystem;

	PlayerSystem* playerSystem;

	DestroySystem* destroySystem;

	/*
	 * Subspaces
	 */
	DebugSpace _dbgSpace;
	ItemSpace _itemSpace;
};

#endif /* GAMESPACE_H_ */
