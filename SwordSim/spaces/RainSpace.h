/*
 * RainSpace.h
 *
 *  Created on: Jan 30, 2014
 *      Author: Hati
 */

#ifndef RAINSPACE_H_
#define RAINSPACE_H_

#include "Space.h"
#include <render/DoubleBufferedFBO.h>
#include <render/Shader.h>
#include <render/NoiseTextures.h>
#include <glm/glm.hpp>
#include <render/Camera.h>
#include <render/debugDraw.h>


struct RainSpace: public Space {
	Camera mCamera;

	eth::ProgramHandle _rainProgram;
	eth::ProgramHandle _displayTextureProgram;

	glm::vec2 _rainGravity;
	DoubleBufferedFBO<1> _rainAdvect;

	// Advect Input
	Texture _noise;

	enum RunningState {
		Stepping,
		Play
	} _runningState;

	void step(float dt);
};

#endif /* RAINSPACE_H_ */
