/*
 * Space.h
 *
 *  Created on: Nov 1, 2013
 *      Author: Hati
 */

#ifndef SPACE_H_
#define SPACE_H_

#include "../Utility.h"
#include <vector>
#include "entity/Entity.h"
#include <type_traits>

/**
 * @details Basic Struct for spaces, further spaces inherit from this one.
 * Also the spaces:: static methods have to be specialized,
 * because standard implementations are either, pure asserts or not working correctly
 */
struct Space {
};

template<typename S>
struct spaces {
	// Assert if S is true Space or any other value
	static_assert(std::tr1::is_base_of<Space, S>::value, "space based functions expect Space derived type");
	/**
	 * Initializes a space of type @tparam S
	 * @param space
	 * @return
	 */
	static KECode initialize(S& space);

	/**
	 * Function to update a single space object
	 * @param space reference to the space object
	 * @param dt estimated delta time since last update
	 */
	static void update(S& space, float dt);

	/**
	 * Contains information upon rendering a specific space type
	 * @param space used for rendering
	 */
	static void render(S& space);

	/**
	 * disposes all resources acquired by a certain space
	 * @param space
	 * @return
	 */
	static KECode dispose(S& space);
};

template<>
inline KECode spaces<Space>::initialize(Space& space) {
	KE_Assert(false,
			"Cannot initialize default space! Please use template specification to create proper initialize code!");
	return KE_FAILURE;
}

template<>
inline void spaces<Space>::update(Space& space, float dt) {
	KE_Assert(false,
			"No update defined for default space! Please use template specification to create proper update code!");
}

template<>
inline void spaces<Space>::render(Space& space) {
	KE_Assert(false,
			"No render defined for default space! Please use template specification to create proper render code!");
}

template<>
inline KECode spaces<Space>::dispose(Space& space) {
	KE_Assert(false,
			"No update defined for default space! Please use template specification to create proper dispose code!");
}

/*
 * Defines for quick implementation changes and reminders for function definitions
 */
#define ethDefineSpaceInit(SpaceName, identifier) template<> KECode spaces<SpaceName>::initialize(SpaceName& identifier)
#define ethDefineSpaceStep(SpaceName, identifier, stepIdentifier) template<> void spaces<SpaceName>::update(SpaceName& identifier, float stepIdentifier)
#define ethDefineSpaceDraw(SpaceName, identifier) template<> void spaces<SpaceName>::render(SpaceName& identifier)
#define ethDefineSpaceDispose(SpaceName, identifier) template<> KECode spaces<SpaceName>::dispose(SpaceName& identifier)


/*
 * Easy function calls for pseudo virtuals
 */
template<typename T>
inline KECode ethSpaceInit(T& space) {
	return spaces<T>::initialize(space);
}

template<typename T>
inline void ethSpaceStep(T& space, float dt) {
	spaces<T>::update(space, dt);
}
template<typename T>
inline void ethSpaceDraw(T& space) {
	spaces<T>::render(space);
}
template<typename T>
inline KECode ethSpaceDispose(T& space) {
	return spaces<T>::dispose(space);
}

#endif /* SPACE_H_ */
