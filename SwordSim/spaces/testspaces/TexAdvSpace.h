/*
 * TexAdvSpace.h
 *
 *  Created on: 18.11.2013
 *      Author: Hati
 */

#ifndef TEXADVSPACE_H_
#define TEXADVSPACE_H_

#include <spaces/Space.h>
#include <render/Camera.h>
#include <render/DoubleBufferedFBO.h>
#include <render/debugDraw.h>
#include <render/Shader.h>
#include <GLFW/glfw3.h>


struct TexAdvSpace: public Space {
	Camera mCamera;
	GLFWwindow* window;
	enum States {
		VectorField,
		Particles,
		Cutting,
		Simulation,
		OneStep
	} displayState;

	int step;
	const int stepsPerUpdate = 1;

	eth::ProgramHandle advectShader;
	eth::ProgramHandle finalShader;


	DoubleBufferedFBO<1> particles;
	SingleTextureFBO cutFBO;

	struct MouseTrace {
		glm::vec2 previousPosition;
		TexAdvSpace::States prevTraceState = VectorField;

		glm::vec2 mousePosition;
	} mouseTracer;
	bool cutRequiresRedraw = false;
	std::vector<glm::vec2> points;

	GLuint vectorFieldId;

	eth::ProgramHandle finalProgram;
};

#endif /* TEXADVSPACE_H_ */
