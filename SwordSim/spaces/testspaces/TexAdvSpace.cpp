/*
 * TexAdvSpace.cpp
 *
 *  Created on: 18.11.2013
 *      Author: Hati
 */

#include <spaces/testspaces/TexAdvSpace.h>

#include <window/InputService.h>
#include <render/NoiseTextures.h>


template<>
KECode spaces<TexAdvSpace>::initialize(TexAdvSpace& space) {
	KE_Assert(space.window != nullptr, "glfwWindow not set!");

	space.mCamera.resize(800, 600);
	space.mCamera.update(0.0f);
	space.mCamera.setZoom(0.0f);

	glEnable(GL_TEXTURE_2D);

	///@todo change to double buffer
	createDoubleBuffer(space.particles);
	space.cutFBO = createFBO();

	TextureInfo vectorFieldTexInfo;
	vectorFieldTexInfo.internalFormat = GL_RGBA32F;
	vectorFieldTexInfo.internalType = GL_FLOAT;
	Texture vectorFieldTexture = createTexture(vectorFieldTexInfo);
//	KE_Assert(vectorFieldTexture.textureId != -1, "Vector field texture failed to create!");
	space.vectorFieldId = vectorFieldTexture.textureId;
	KE_Notify("Created texture %u ", space.vectorFieldId);
	srand(0); // MAX_RAND = 32767

	Texture vectorFieldTex; vectorFieldTex.textureId = space.vectorFieldId;
	vectorFieldTex.width = 800; vectorFieldTex.height = 600;

	generateColorNoise(vectorFieldTex);

	space.advectShader = eth::createProgram();
	space.finalShader = eth::createProgram();

	eth::ShaderHandle f1 = eth::createShader("advect.frag");
	eth::ShaderHandle f2 = eth::createShader("final_advection.frag");

	eth::attachShader(space.advectShader, f1);
	eth::attachShader(space.finalShader, f2);

	eth::linkProgram(space.advectShader);
	eth::linkProgram(space.finalShader);

	space.displayState = space.VectorField;

	swapDoubleBuffer(space.particles);

	return KE_SUCCESS;
}

template<>
void spaces<TexAdvSpace>::update(TexAdvSpace& space, float dt) {
	InputService* input = service<InputService>::get();
	if (input->isKeyPushed(GLFW_KEY_V)) {
		space.displayState = space.VectorField;
	}
	if (input->isKeyPushed(GLFW_KEY_P)) {
		space.displayState = space.Particles;
	}
	if (input->isKeyPushed(GLFW_KEY_R)) {
		space.displayState = space.Simulation;
	}
	if (input->isKeyPushed(GLFW_KEY_S)) {
		space.displayState = space.OneStep;
	}
	if (input->isKeyPushed(GLFW_KEY_C)) {
		space.displayState = space.Cutting;
	}
	if(input->isKeyPushed(GLFW_KEY_F5)) {
		clearDoubleBuffer(space.particles, glm::vec4(0,0,0,0));
	}
	if(input->isKeyPushed(GLFW_KEY_F6)) {

	}

	double currentx, currenty;

	glfwGetCursorPos(space.window, &currentx, &currenty);
	space.mouseTracer.mousePosition.x = float(currentx); space.mouseTracer.mousePosition.y = float(currenty);
	if (space.displayState == space.Cutting) {

		glm::vec2 current = glm::vec2(currentx, currenty);
		const float min_distance = 1.0f;

		if (glfwGetMouseButton(space.window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {

			if (glm::distance(space.mouseTracer.previousPosition,
					glm::vec2(currentx, currenty)) > min_distance) {
				space.points.push_back(
						(current - glm::vec2(400, 300)) * glm::vec2(1, -1));
			}
			space.cutRequiresRedraw = true;
		}

		if (glfwGetMouseButton(space.window,
				GLFW_MOUSE_BUTTON_1) == GLFW_RELEASE) {
			if(space.cutRequiresRedraw) {
				space.cutRequiresRedraw = false;
			}

			space.points.clear();
		}
	}
	space.step++;

	if ((space.step > space.stepsPerUpdate
			&& space.displayState == space.Simulation)
			|| space.displayState == space.OneStep) {
		space.step = 0;

//		float randomizeVF = rand() / float(RAND_MAX);
//		if(randomizeVF > 0.8f) {
//			generateVectorField(space.vectorFieldId);
//		}

		const GLenum drawBuffers[] = {
		GL_COLOR_ATTACHMENT0 };
		glBindFramebuffer(GL_FRAMEBUFFER,
				space.particles.buffers[space.particles.activeBuffer]);
		int otherBufferId = (space.particles.activeBuffer + 1) % 2;

		//		KE_Notify("use %d as prev and %d as current", otherBufferId, space.bufferId);
		eth::enableProgram(space.advectShader);
		glDrawBuffers(1, &drawBuffers[0]);
		{
			glActiveTexture(GL_TEXTURE0);
			glClear(GL_DEPTH_BUFFER_BIT);
			glBindTexture(GL_TEXTURE_2D, space.vectorFieldId); // use cut texture to generate simply blood flow
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D,
					space.particles.textures[otherBufferId]); // use previous cut texture to generate simply blood flow
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, space.cutFBO.textureId);

			GLint vectorFieldLoc = eth::programUniformLocation(
					space.advectShader, "vectorField");
			GLint particlesLoc = eth::programUniformLocation(space.advectShader,
					"particles");
			GLint cutLoc = eth::programUniformLocation(space.advectShader,
					"cutSampler");

			GLint mpLoc = eth::programUniformLocation(space.advectShader,
								"mousePosition");
//			KE_Notify("%3.2f, %3.2f", space.mouseTracer.mousePosition.x, space.mouseTracer.mousePosition.y);
			glUniform1i(vectorFieldLoc, 0);

			glUniform1i(particlesLoc, 1);
			glUniform1i(cutLoc, 2);
			glUniform2f(mpLoc, space.mouseTracer.mousePosition.x, space.mouseTracer.mousePosition.y);

			glm::vec2 halfExtent(400, 300);
			glColor3f(1, 1, 1);
			glBegin(GL_TRIANGLE_STRIP);
			{
				glTexCoord2f(0.0f, 0.0f);
				glVertex2f(-halfExtent.x, -halfExtent.y);
				glTexCoord2f(0.0f, 1.0f);
				glVertex2f(-halfExtent.x, halfExtent.y);
				glTexCoord2f(1.0f, 0.0f);
				glVertex2f(halfExtent.x, -halfExtent.y);
				glTexCoord2f(1.0f, 1.0f);
				glVertex2f(halfExtent.x, halfExtent.y);
			}
			glEnd();
		}

		swapDoubleBuffer(space.particles);
		glUseProgram(0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glBindTexture(GL_TEXTURE_2D, 0); // disable tex3
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, 0); // disable tex2
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0); // disable tex1

		if (space.displayState == space.OneStep)
			space.displayState = space.Particles;
	}
}



void renderCutToTexture(TexAdvSpace& space) {
	if(!space.cutRequiresRedraw)
		return;


	const GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };

	glBindFramebuffer(GL_FRAMEBUFFER, space.cutFBO.fboId);
	glClear(GL_DEPTH_BUFFER_BIT);

	glDrawBuffers(1, drawBuffers);

	glLineWidth(2.0f);

	auto it = space.points.begin();
	if(it==space.points.end()) {
		return;
	}
	glm::vec2 prevPoint = *it; // 0 rotation

	glBegin(GL_LINE_STRIP);
	{
		if (space.points.size() > 2) {
			for (; it != space.points.end(); ++it) {
				glm::vec2& curPoint = *it;

				glm::vec2 dp = curPoint - prevPoint;
				if (glm::length2(dp) > glm::epsilon<float>()) {
					glm::vec2 dpN = glm::abs(glm::normalize(dp));
					glColor3f(dpN.y, dpN.x, 0);
				} else {
					glColor3f(1, 0, 0);
				}
				/*
				 px = x * cs - y * sn;
				 py = x * sn + y * cs;
				 */
				glVertex2f(it->x, it->y);
				prevPoint = *it;
			}
		}
	}
	glEnd();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


template<>
void spaces<TexAdvSpace>::render(TexAdvSpace& space) {
	useCamera(space.mCamera);
	int otherId = (space.particles.activeBuffer + 1) % 2;
	GLuint pLoc = 0;

	switch (space.displayState) {
	case space.VectorField:
		displayTexture(space.vectorFieldId);
		break;
	case space.Particles:
		displayTexture(space.particles.textures[otherId]);
		break;
	case space.Simulation:
		eth::enableProgram(space.finalShader);
		pLoc = eth::programUniformLocation(space.finalShader, "uTexture");
		glUniform1i(pLoc, 0);
		displayTexture(space.particles.textures[otherId]);

		glUseProgram(0);
		break;
	case space.Cutting:
		renderCutToTexture(space);
		displayTexture(space.cutFBO.textureId);
		break;
	default:
		break;
	}

	glPopMatrix();
}

template<>
KECode spaces<TexAdvSpace>::dispose(TexAdvSpace& space) {
	return KE_SUCCESS;
}
