/*
 * TestCutSpace.h
 *
 *  Created on: Nov 3, 2013
 *      Author: Hati
 */

#ifndef TESTCUTSPACE_H_
#define TESTCUTSPACE_H_

#include <spaces/Space.h>
#include <render/Camera.h>
#include <render/Shader.h>
#include <render/FBO.h>
#include <GLFW/glfw3.h>
#include <vector>

struct MouseTrace {
	glm::vec2 previousPosition;

};

struct TestCutSpace: public Space {
	GLFWwindow* window;

	Camera _camera;

	GLuint cutFBO;
	GLuint cutTexture;

	DoubleBufferedFBO<1> iterationFBO;

	eth::ProgramHandle smearShader;
	eth::ProgramHandle finalRender;

	MouseTrace mouseTracer;

	std::vector<glm::vec2> points;
	glm::vec2 lastNormal;

	bool _simulationEnabled;
};

#endif /* TESTCUTSPACE_H_ */
