/*
 * CutEffectSpace.cpp
 *
 *  Created on: 23.12.2013
 *      Author: Hati
 */

#include <spaces/testspaces/CutEffectSpace.h>
#include <window/InputService.h>
#include <render/NoiseTextures.h>

template<>
KECode spaces<CutEffectSpace>::initialize(CutEffectSpace& space) {
	space.mCamera.resize(800, 600);
	space.mCamera.setZoom(0.0f);
	space.mCamera.update(0.0f);
	space._loaded = false;
	srand(0);
	space._runningState = CutEffectSpace::Play;

	glEnable(GL_TEXTURE_2D);
	space.splashNoise = createTexture();
	generateColorNoise(space.splashNoise);

	createDoubleBuffer(space.cutEffectFBO);
	space.cutsFBO = createFBO();

	space.reload();

	return KE_SUCCESS;
}

template<>
void spaces<CutEffectSpace>::update(CutEffectSpace& space, float dt) {
	InputService* input = service<InputService>::get();
	if (input->isKeyPushed(GLFW_KEY_F5)) {
		space.reload();
		return;
	}
	if (input->isKeyPushed(GLFW_KEY_S)) {
		clearFBO(space.cutsFBO, glm::vec4(0));
		space.texPos= (glm::vec2((float(rand()) / RAND_MAX), (float(rand()) / RAND_MAX))
				 - glm::vec2(0.5f)) * glm::vec2(800, 600);
	}
	if (input->isKeyPushed(GLFW_KEY_P)) {
		if (space._runningState != CutEffectSpace::Play) {
			space._runningState = CutEffectSpace::Play;
			KE_Notify("CutEffectSpace::_runningState = Play");
		} else {
			space._runningState = CutEffectSpace::Stepping;
			KE_Notify("CutEffectSpace::_runningState = Step");
		}

	}
	if (input->isKeyPushed(GLFW_KEY_R)) { //break on not R
		if (space._runningState != CutEffectSpace::Stepping) {
			space._runningState = CutEffectSpace::Stepping;
			KE_Notify("CutEffectSpace::_runningState = Step");
		}
		space.step(dt);
	}

	if (space._runningState == CutEffectSpace::Play) {
		space.step(dt);
	}

}

template<>
void spaces<CutEffectSpace>::render(CutEffectSpace& space) {
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	useCamera(space.mCamera);
	int otherId = (space.cutEffectFBO.activeBuffer + 1) % 2;
	eth::enableProgram(space.displayShader);
	GLuint pLoc = eth::programUniformLocation(space.displayShader, "uTexture");
	glUniform1i(pLoc, 0);
	displayTexture(space.cutEffectFBO.textures[otherId]);
//	displayTexture(space.cutsFBO.textureId);

	glUseProgram(0);
	glPopMatrix();
}

template<>
KECode spaces<CutEffectSpace>::dispose(CutEffectSpace& space) {
	return KE_SUCCESS;
}

void CutEffectSpace::step(float dt) {
	// code is just executed on R
	const GLenum drawBuffers[] = {
	GL_COLOR_ATTACHMENT0 };

	glBindFramebuffer(GL_FRAMEBUFFER, cutsFBO.fboId); // insert cut to cut texture
	eth::enableProgram(applyCutShader);
	glDrawBuffers(1, &drawBuffers[0]);
	{
		glClear(GL_DEPTH_BUFFER_BIT);

		// use cut texture to generate simply blood flow
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, customCut.textureId);

		GLint cutTexLoc = eth::programUniformLocation(cutEffectShader,
				"uCutTexture");

		glUniform1i(cutTexLoc, 0);

		glm::vec2 halfExtent(this->customCut.width / 2,
				this->customCut.height / 2);
		glColor3f(1, 1, 1);

		glTranslatef(texPos.x, texPos.y,0);
		glBegin(GL_TRIANGLE_STRIP);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex2f(-halfExtent.x, -halfExtent.y);
			glTexCoord2f(0.0f, 1.0f);
			glVertex2f(-halfExtent.x, halfExtent.y);
			glTexCoord2f(1.0f, 0.0f);
			glVertex2f(halfExtent.x, -halfExtent.y);
			glTexCoord2f(1.0f, 1.0f);
			glVertex2f(halfExtent.x, halfExtent.y);
		}
		glEnd();
		glTranslatef(-texPos.x, -texPos.y,0);
	}

	glBindFramebuffer(GL_FRAMEBUFFER,
			cutEffectFBO.buffers[cutEffectFBO.activeBuffer]); // use backbuffer to draw to
	int otherBufferId = (cutEffectFBO.activeBuffer + 1) % 2;
	eth::enableProgram(cutEffectShader);
	glDrawBuffers(1, &drawBuffers[0]); //select bufferId:0
	{
		glClear(GL_DEPTH_BUFFER_BIT);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, cutsFBO.textureId);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, cutEffectFBO.textures[otherBufferId]);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, splashNoise.textureId);

		GLint cutTexLoc = eth::programUniformLocation(cutEffectShader,
				"uCutTexture");
		GLint prevTexLoc = eth::programUniformLocation(cutEffectShader,
				"uPrevStep");
		GLint noiseTexLoc = eth::programUniformLocation(cutEffectShader,
						"uNoise");

		glUniform1i(cutTexLoc, 0);
		glUniform1i(prevTexLoc, 1);
		glUniform1i(noiseTexLoc, 2);

		glm::vec2 halfExtent(400, 300);
		glColor3f(1, 1, 1);
		glBegin(GL_TRIANGLE_STRIP);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex2f(-halfExtent.x, -halfExtent.y);
			glTexCoord2f(0.0f, 1.0f);
			glVertex2f(-halfExtent.x, halfExtent.y);
			glTexCoord2f(1.0f, 0.0f);
			glVertex2f(halfExtent.x, -halfExtent.y);
			glTexCoord2f(1.0f, 1.0f);
			glVertex2f(halfExtent.x, halfExtent.y);
		}
		glEnd();
	}

	swapDoubleBuffer(cutEffectFBO);

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0); // disable tex1
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0); // disable tex0

}

void CutEffectSpace::reload() {
	const char* cutPath = "assets/customCut.png";
	if (_loaded) {
		eth::freeProgram(displayShader);
		eth::freeProgram(cutEffectShader);
		eth::freeProgram(applyCutShader);

		clearDoubleBuffer(cutEffectFBO, glm::vec4(0, 0, 0, 0));

		fillTextureFromPNG(customCut, cutPath);



	} else {
		customCut = loadTextureFromPNG(cutPath);
	}



	this->_runningState = Stepping;

	displayShader = eth::createProgram();
	cutEffectShader = eth::createProgram();
	applyCutShader = eth::createProgram();

	eth::ShaderHandle displayFrag = eth::createShader("displayTexture.frag");
	eth::ShaderHandle effectFrag = eth::createShader("cuteffect.frag");
	eth::ShaderHandle applyFrag = eth::createShader("applyCut.frag");

	eth::attachShader(displayShader, displayFrag);
	eth::attachShader(cutEffectShader, effectFrag);
	eth::attachShader(applyCutShader, applyFrag);

	eth::linkProgram(displayShader);
	eth::linkProgram(cutEffectShader);
	eth::linkProgram(applyCutShader);

	_loaded = true;
}
