/*
 * CutEffectSpace.h
 *
 *  Created on: 23.12.2013
 *      Author: Hati
 */

#ifndef CUTEFFECTSPACE_H_
#define CUTEFFECTSPACE_H_

#include <spaces/Space.h>
#include <render/Texture.h>
#include <render/DoubleBufferedFBO.h>
#include <render/Camera.h>
#include <render/Shader.h>
#include <render/debugDraw.h>

struct CutEffectSpace: public Space {
	Texture customCut;

	glm::vec2 mGravity;
	Camera mCamera;

	eth::ProgramHandle applyCutShader;
	eth::ProgramHandle cutEffectShader;
	eth::ProgramHandle displayShader;

	Texture splashNoise;
	SingleTextureFBO cutsFBO;
	DoubleBufferedFBO<1> cutEffectFBO;

	glm::vec2 texPos{1,0};

	enum RunningState {
		Stepping,
		Play
	} _runningState;

	void reload();
	void step(float dt);
	bool _loaded;
};

#endif /* CUTEFFECTSPACE_H_ */
