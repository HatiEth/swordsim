/*
 * SwordCombatSpace.cpp
 *
 *  Created on: Nov 10, 2013
 *      Author: Hati
 */

#include "SwordCombatSpace.h"
#include <render/Box2DDebugDraw.h>
#include <gl/glew.h>
#include <render/debugDraw.h>
//#include <window/InputService.h>
#include <render/Texture.h>
#include <render/DoubleBufferedFBO.h>

void buildDemoLevel(b2World* world) {
	b2BodyDef levelDef;
	levelDef.position.Set(0, -5);
	levelDef.type = b2_staticBody;

	b2Body* body = world->CreateBody(&levelDef);

	b2ChainShape* levelShape = new b2ChainShape();

	b2Vec2 levelVerts[10];
	for (int i = 0; i < 10; ++i) {
		levelVerts[i].Set(-50 + i * 10, 0);
	}

	levelShape->CreateChain(levelVerts, 10);

	b2FixtureDef fixDef;
	fixDef.density = 1;
	fixDef.shape = levelShape;

	body->CreateFixture(&fixDef);
}

void buildPlayer(SwordCombatSpace& space) {
	Box2DDebugDraw* debugDraw = new Box2DDebugDraw();
	debugDraw->AppendFlags(b2Draw::e_shapeBit);
//	debugDraw->AppendFlags(b2Draw::e_aabbBit);
//	debugDraw->AppendFlags(b2Draw::e_jointBit);

	space.mWorld->SetDebugDraw(debugDraw);

	b2BodyDef testBodyDef;
	testBodyDef.position.Set(0.0f, 0.0f);
	testBodyDef.type = b2_dynamicBody;
	space.testBody = space.mWorld->CreateBody(&testBodyDef);

	b2FixtureDef testBodyFixtureDef;
	b2PolygonShape* shape = new b2PolygonShape();
	shape->SetAsBox(0.5f, 1.0f);
	testBodyFixtureDef.shape = shape;
	testBodyFixtureDef.density = 1.0f;

	space.testBody->CreateFixture(&testBodyFixtureDef);
}

template<>
KECode spaces<SwordCombatSpace>::initialize(SwordCombatSpace& space) {
	glEnable(GL_TEXTURE_2D);

	space.mCamera.resize(800, 600);
	space.mCamera.setZoom(0.98f);
	space.mCamera.update(0.0f);

//	KECode hudInitCode = spaces<HudSpace>::initialize(space.hud);
//	if (hudInitCode != KE_SUCCESS) {
//		KE_CustomWarning("Hud initialization failed! %s", KE_GetError());
//	}

	space.mWorld = new b2World(b2Vec2(0, -9.81));

	buildDemoLevel(space.mWorld);
	buildPlayer(space);

	loadTextureFromPNG("sword.png");

	KE_Notify("Initialized sword combat space!");
	return KE_SUCCESS;
}

template<>
void spaces<SwordCombatSpace>::update(SwordCombatSpace& space, float dt) {
	space.mCamera.update(dt);
	space.mWorld->Step(dt, 8, 3);

//	InputService* input = service<InputService>::get();
//	if (input->isKeyPushed(GLFW_KEY_KP_ADD)) {
//		space.mCamera.zoom(0.001f);
////		KE_Notify("GLFW_KEY_F5 pushed");
//	}
//	if (input->isKeyPushed(GLFW_KEY_KP_SUBTRACT)) {
//		space.mCamera.zoom(-0.001f);
//	}
//
//	if (input->isKeyPushed(GLFW_KEY_D)) {
//		b2Vec2 curVelo = space.testBody->GetLinearVelocity();
//		curVelo.x = glm::clamp(curVelo.x + 5, -5.f, 10.f);
//		space.testBody->SetLinearVelocity(curVelo);
//	}
//	if (input->isKeyPushed(GLFW_KEY_A)) {
//		b2Vec2 curVelo = space.testBody->GetLinearVelocity();
//		curVelo.x = glm::clamp(curVelo.x - 5, -10.f, 5.f);
//		space.testBody->SetLinearVelocity(curVelo);
//	}
//	if (input->isKeyPushed(GLFW_KEY_W)) {
//		b2Vec2 curVelo = space.testBody->GetLinearVelocity();
//		curVelo.y = 5;
//		space.testBody->SetLinearVelocity(curVelo);
//	}
}

template<>
void spaces<SwordCombatSpace>::render(SwordCombatSpace& space) {
//	spaces<HudSpace>::render(space.hud);

	useCamera(space.mCamera);

	space.mWorld->DrawDebugData();

//	_displayTexture(space.tex);

	glPopMatrix();
}
