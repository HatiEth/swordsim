/*
 * TestCutSpace.cpp
 *
 *  Created on: Nov 3, 2013
 *      Author: Hati
 */

#include "TestCutSpace.h"
#include <render/debugDraw.h>

void initShaders(TestCutSpace& space) {
	space.smearShader = eth::createProgram();
	eth::ShaderHandle f1 = eth::createShader("mousetrace.frag");
//	eth::ShaderHandle v1 = eth::createShader("mousetrace.vert");

	eth::attachShader(space.smearShader, f1);
//	eth::attachShader(space.smearShader, v1);

	eth::linkProgram(space.smearShader);

	space.finalRender = eth::createProgram();
	eth::ShaderHandle f2 = eth::createShader("full_smear.frag");
	eth::ShaderHandle v2 = eth::createShader("full_smear.vert");

	eth::attachShader(space.finalRender, v2);
	eth::attachShader(space.finalRender, f2);

	eth::linkProgram(space.finalRender);
}

template<>
KECode spaces<TestCutSpace>::initialize(TestCutSpace& space) {
	KE_Notify("Initialized game space!");
	space._camera.resize(800, 600);
	space._camera.update(0.0f);
	space._camera.setZoom(0.0f);

	glEnable(GL_TEXTURE_2D);

	SingleTextureFBO fbo = createFBO();
	space.cutFBO = fbo.fboId;
	space.cutTexture = fbo.textureId;

	createDoubleBuffer(space.iterationFBO);

	initShaders(space);

	space.points.reserve(20);

	space._simulationEnabled = false;

	space.lastNormal = glm::vec2(0,1);

	return KE_SUCCESS;
}

template<>
void spaces<TestCutSpace>::update(TestCutSpace& space, float dt) {
	space._camera.update(dt);

	double currentx, currenty;

	glfwGetCursorPos(space.window, &currentx, &currenty);
	glm::vec2 current = glm::vec2(currentx, currenty);
	const float min_distance = 1.0f;

	if (glfwGetMouseButton(space.window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {

		if (glm::distance(space.mouseTracer.previousPosition,
				glm::vec2(currentx, currenty)) > min_distance) {
			space.points.push_back(
					(current - glm::vec2(400, 300)) * glm::vec2(1, -1));

		}
		space._simulationEnabled = false;
	}
	if (glfwGetMouseButton(space.window, GLFW_MOUSE_BUTTON_1) == GLFW_RELEASE) {
		space.points.clear();
		space._simulationEnabled = true;
	}

	space.mouseTracer.previousPosition = glm::vec2(currentx, currenty);

	// f5 pressed (and released) -> clear
	if (glfwGetKey(space.window, GLFW_KEY_F5) == GLFW_PRESS) {
		glBindFramebuffer(GL_FRAMEBUFFER, space.cutFBO);
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		space.points.clear();
		space.lastNormal = glm::vec2(0,1);

		eth::deleteProgram(space.finalRender);
		eth::deleteProgram(space.smearShader);

		initShaders(space);

		clearDoubleBuffer(space.iterationFBO, glm::vec4(0, 0, 0, 0));
	}

	if (space._simulationEnabled) {
		const GLenum drawBuffers[] = {
		GL_COLOR_ATTACHMENT0 };
		glBindFramebuffer(GL_FRAMEBUFFER,
				space.iterationFBO.buffers[space.iterationFBO.activeBuffer]);
		int otherBufferId = (space.iterationFBO.activeBuffer + 1) % 2;

		//		KE_Notify("use %d as prev and %d as current", otherBufferId, space.bufferId);
		eth::enableProgram(space.smearShader);
		glDrawBuffers(1, &drawBuffers[0]);
		{
			glClear(GL_DEPTH_BUFFER_BIT);
			glBindTexture(GL_TEXTURE_2D, space.cutTexture); // use cut texture to generate simply blood flow
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D,
					space.iterationFBO.textures[otherBufferId]); // use previous cut texture to generate simply blood flow
			GLint cutLoc = eth::programUniformLocation(space.smearShader,
					"cutTexture");
			GLint prevLoc = eth::programUniformLocation(space.smearShader,
					"prevTexture");
			//			KE_Notify("cuttexture: %d | prevTexture: %d", cutLoc, prevLoc);
			glUniform1i(cutLoc, 0);

			glUniform1i(prevLoc, 1);

			glm::vec2 halfExtent(400, 300);
			glColor3f(1, 1, 1);
			glBegin(GL_TRIANGLE_STRIP);
			{
				glTexCoord2f(0.0f, 0.0f);
				glVertex2f(-halfExtent.x, -halfExtent.y);
				glTexCoord2f(0.0f, 1.0f);
				glVertex2f(-halfExtent.x, halfExtent.y);
				glTexCoord2f(1.0f, 0.0f);
				glVertex2f(halfExtent.x, -halfExtent.y);
				glTexCoord2f(1.0f, 1.0f);
				glVertex2f(halfExtent.x, halfExtent.y);
			}
			glEnd();

		}
		swapDoubleBuffer(space.iterationFBO);
		glUseProgram(0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

template<>
void spaces<TestCutSpace>::render(TestCutSpace& space) {
	useCamera(space._camera);
//	glColor3f(0, 1, 0);
//	drawQuadAt(5.f,
//			glm::vec2(1, -1)
//					* (space.mouseTracer.previousPosition - glm::vec2(400, 300)));

	const GLenum drawBuffers[] = {
	GL_COLOR_ATTACHMENT0 };

	glBindFramebuffer(GL_FRAMEBUFFER, space.cutFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	glDrawBuffers(1, drawBuffers);



	glLineWidth(2.0f);

	auto it = space.points.begin();
	glm::vec2 colorCode(space.lastNormal);
	glm::vec2 prevPoint = *it; // 0 rotation

	glBegin(GL_LINE_STRIP);
	{
		if(space.points.size()>2) {
		for (; it != space.points.end(); ++it) {
			glm::vec2& curPoint = *it;

			glm::vec2 dp = curPoint - prevPoint;
			if(glm::length2(dp) > glm::epsilon<float>()) {
				glm::vec2 dpN = glm::abs(glm::normalize(dp));
				glColor3f(dpN.y, dpN.x, 0);
			}
			else {
				glColor3f(1,0,0);
			}
			/*
				px = x * cs - y * sn;
				py = x * sn + y * cs;
			 */
			glVertex2f(it->x, it->y);
			prevPoint = *it;
		}
		}
	}
	glEnd();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (space.points.size() > 10u) {
		glm::vec2 lastpoint = space.points.back();
		space.points.clear();
		space.points.push_back(lastpoint);
		space.lastNormal = colorCode;
	}

	eth::enableProgram(space.finalRender);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, space.cutTexture);
	glUniform1i(eth::programUniformLocation(space.finalRender, "cutSampler"),
			0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,
			space.iterationFBO.textures[space.iterationFBO.activeBuffer]);
	glEnable(GL_TEXTURE_2D);
	glUniform1i(eth::programUniformLocation(space.finalRender, "smearSampler"),
			1);

	glm::vec2 halfExtent(400, 300);
	glBegin(GL_TRIANGLE_STRIP);
	{
		glMultiTexCoord2f( GL_TEXTURE0, 0.0f, 0.0f);
		glMultiTexCoord2f( GL_TEXTURE1, 0.0f, 0.0f);
		glVertex2f(-halfExtent.x, -halfExtent.y);
		glMultiTexCoord2f( GL_TEXTURE0, 0.0f, 1.0f);
		glMultiTexCoord2f( GL_TEXTURE1, 0.0f, 1.0f);
		glVertex2f(-halfExtent.x, halfExtent.y);
		glMultiTexCoord2f( GL_TEXTURE0, 1.0f, 0.0f);
		glMultiTexCoord2f( GL_TEXTURE1, 1.0f, 0.0f);
		glVertex2f(halfExtent.x, -halfExtent.y);
		glMultiTexCoord2f( GL_TEXTURE0, 1.0f, 1.0f);
		glMultiTexCoord2f( GL_TEXTURE1, 1.0f, 1.0f);
		glVertex2f(halfExtent.x, halfExtent.y);
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);

}
