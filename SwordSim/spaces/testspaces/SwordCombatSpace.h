/*
 * SwordCombatSpace.h
 *
 *  Created on: Nov 10, 2013
 *      Author: Hati
 */

#ifndef SWORDCOMBATSPACE_H_
#define SWORDCOMBATSPACE_H_

#include <GL/glew.h>
#include "../Space.h"
#include "../HudSpace.h"
#include <render/Camera.h>
#include <Box2D/Box2D.h>

struct SwordCombatSpace: public Space {
	HudSpace* hud;

	Camera mCamera;
	b2World* mWorld;

	b2Body* testBody;

	GLuint tex;

};

#endif /* SWORDCOMBATSPACE_H_ */
