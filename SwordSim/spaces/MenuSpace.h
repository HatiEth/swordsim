/*
 * MenuSpace.h
 *
 *  Created on: Nov 3, 2013
 *      Author: Hati
 */

#ifndef MENUSPACE_H_
#define MENUSPACE_H_

#include "Space.h"
#include <render/Camera.h>

struct MenuSpace: public Space {
	Camera _camera;
};


#endif /* MENUSPACE_H_ */
