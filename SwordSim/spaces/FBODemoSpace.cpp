/*
 * FBODemoSpace.cpp
 *
 *  Created on: Nov 6, 2013
 *      Author: Hati
 */

#include "FBODemoSpace.h"
#include <render/debugDraw.h>
template<>
KECode spaces<FBODemoSpace>::initialize(FBODemoSpace& space) {


	   //RGBA8 2D texture, 24 bit depth texture, 256x256
	   glGenTextures(1, &space.color_tex);
	   glBindTexture(GL_TEXTURE_2D, space.color_tex);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	   //NULL means reserve texture memory, but texels are undefined
	   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 256, 256, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
	   //-------------------------
	   glGenFramebuffersEXT(1, &space.fb);
	   glBindFramebufferEXT(GL_FRAMEBUFFER, space.fb);
	   //Attach 2D texture to this FBO
	   glFramebufferTexture2DEXT(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, space.color_tex, 0);
	   //-------------------------
	   glGenRenderbuffers(1, &space.depth_rb);
	   glBindRenderbuffer(GL_RENDERBUFFER, space.depth_rb);
	   glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 256, 256);
	   //-------------------------
	   //Attach depth buffer to FBO
	   glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, space.depth_rb);
	   //-------------------------
	   //Does the GPU support current FBO configuration?
	   GLenum status;
	   status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	   KE_Assert(status == GL_FRAMEBUFFER_COMPLETE, "%s",
	   			glewGetErrorString(status));
	   //-------------------------
	   return KE_SUCCESS;
}


template<>
void spaces<FBODemoSpace>::render(FBODemoSpace& space) {
	//and now you can render to GL_TEXTURE_2D
	glBindFramebufferEXT(GL_FRAMEBUFFER, space.fb);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//-------------------------
	glViewport(0, 0, 256, 256);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 256.0, 0.0, 256.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//-------------------------
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	//-------------------------
	//**************************
	//RenderATriangle, {0.0, 0.0}, {256.0, 0.0}, {256.0, 256.0}
	glColor3f(1,0,0);
	drawRect(glm::vec2(128,128));
	//Read http://www.opengl.org/wiki/VBO_-_just_examples
	//-------------------------
	//----------------
	//Bind 0, which means render to back buffer
	glBindFramebufferEXT(GL_FRAMEBUFFER, 0);

	glBindTexture(GL_TEXTURE_2D, space.color_tex);
	glm::vec2 halfExtent(800,600);
	glColor3f(1,0,0);
	glBegin(GL_TRIANGLE_STRIP); {
		glTexCoord2f(0.0f, 0.0f); glVertex2f(-halfExtent.x, -halfExtent.y);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(-halfExtent.x, halfExtent.y);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(halfExtent.x, -halfExtent.y);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(halfExtent.x, halfExtent.y);
	} glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

}
