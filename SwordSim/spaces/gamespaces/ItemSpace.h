/*
 * ItemSpace.h
 *
 *  Created on: Feb 20, 2014
 *      Author: Hati
 */

#ifndef ITEMSPACE_H_
#define ITEMSPACE_H_

#include <spaces/Space.h>

#include <systems/ItemSystem.h>
#include <systems/EquipmentSystem.h>

struct ItemSpace : public Space {
	ItemSystem* itemSystem;
	EquipmentSystem* equipmentSystem;
};

#endif /* ITEMSPACE_H_ */
