/*
 * DebugSpace.h
 *
 *  Created on: Feb 20, 2014
 *      Author: Hati
 */

#ifndef DEBUGSPACE_H_
#define DEBUGSPACE_H_

#include <spaces/Space.h>
#include <systems/DebugSystem.h>
#include <systems/PhysicsSystem.h>

struct DebugSpace : public Space {

	DebugSystem* dbgSystem;
	PhysicsSystem* externalPhysicsSystem{nullptr};

};


#endif /* DEBUGSPACE_H_ */
