/*
 * DebugSpace.cpp
 *
 *  Created on: Feb 20, 2014
 *      Author: Hati
 */

#include "DebugSpace.h"


ethDefineSpaceInit(DebugSpace, space) {
	space.dbgSystem = new DebugSystem();


	return KE_SUCCESS;
}

ethDefineSpaceDispose(DebugSpace, space) {
	delete space.dbgSystem;


	return KE_SUCCESS;
}

ethDefineSpaceDraw(DebugSpace, space) {
	ethSystemExecute(*space.dbgSystem, 0.0f);

	if(space.externalPhysicsSystem!=nullptr) {
		space.externalPhysicsSystem->debugDraw();
	}
}


