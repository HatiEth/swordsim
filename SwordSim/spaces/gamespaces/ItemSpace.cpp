/*
 * ItemSpace.cpp
 *
 *  Created on: Feb 20, 2014
 *      Author: Hati
 */

#include "ItemSpace.h"

ethDefineSpaceInit(ItemSpace, space) {
	space.itemSystem = new ItemSystem();
	space.equipmentSystem = new EquipmentSystem(*space.itemSystem);



	return KE_SUCCESS;
}

ethDefineSpaceStep(ItemSpace, space, dt) {

	ethSystemExecute(*space.equipmentSystem, dt);

}


ethDefineSpaceDispose(ItemSpace, space) {
	delete space.equipmentSystem;
	delete space.itemSystem;

	return KE_SUCCESS;
}
