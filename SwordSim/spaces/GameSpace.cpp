/*
 * GameSpace.cpp
 *
 *  Created on: Dec 9, 2013
 *      Author: Hati
 */

#include "GameSpace.h"
#include <render/Texture.h>
#include <render/debugDraw.h>
#include <parser/EntityInfoParser.h>
#include <parser/LevelInfoParser.h>

template<>
KECode spaces<GameSpace>::initialize(GameSpace& space) {
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);

	space.mCamera.setZoom(0.920f);
	space.mCamera.resize(800, 600);
	space.mCamera.update(0.0f);

	ethSpaceInit(space._dbgSpace);
	space.systems[SYSTEM_NAME(DebugSystem)] = space._dbgSpace.dbgSystem;

	space.renderSystem = new RenderSystem();
	space.systems[SYSTEM_NAME(RenderSystem)] = space.renderSystem;

	space.referenceSystem = new ReferenceSystem();
	space.systems[SYSTEM_NAME(ReferenceSystem)] = space.referenceSystem;

	space.healthSystem = new HealthSystem();
	space.systems[SYSTEM_NAME(HealthSystem)] = space.healthSystem;

	b2World* box2dWorld = new b2World(b2Vec2(0, -9.81));
	box2dWorld->SetGravity(b2Vec2(0.0f, -9.81f));

	space.physicsSystem = new PhysicsSystem(box2dWorld);
	space.systems[SYSTEM_NAME(PhysicsSystem)] = space.physicsSystem;

	space.playerSystem = new PlayerSystem();
	space.systems[SYSTEM_NAME(PlayerSystem)] = space.playerSystem;

	ethSpaceInit(space._itemSpace);
	space.systems[SYSTEM_NAME(ItemSystem)] = space._itemSpace.itemSystem;
//	space.systems[SYSTEM_NAME(EquipmentSystem)] = space._itemSpace.equipmentSystem;
	registerSystem(space.systems, space._itemSpace.equipmentSystem);

	space.destroySystem = new DestroySystem(space.entities);
	registerSystem(space.systems, space.destroySystem);


	LevelInfoParser levelParser(space.entities, space.systems);
	levelParser.parseLevel("levels/testlevel.json");

	space._hud = new HudSpace(*space.referenceSystem);
	KECode _hudErr = spaces<HudSpace>::initialize(*space._hud);
	if (_hudErr != KE_SUCCESS) {
		KE_CustomWarning("GameSpace::Hudspace failed to initialize! %s",
				KE_GetError());
	}

	space._dbgSpace.externalPhysicsSystem = space.physicsSystem;

	return KE_SUCCESS;
}

template<>
void spaces<GameSpace>::update(GameSpace& space, float dt) {
	spaces<HudSpace>::update(*space._hud, dt);

	space.mCamera.update(dt);
	ethSystemExecute(*space.playerSystem, dt);
	ethSystemExecute(*space.physicsSystem, dt);
	ethSystemExecute(*space.healthSystem, dt);

	ethSpaceStep(space._itemSpace, dt);


	ethSystemExecute(*space.destroySystem, dt);
}

template<>
void spaces<GameSpace>::render(GameSpace& space) {

	spaces<HudSpace>::render(*space._hud);
	useCamera(space.mCamera);

	space.renderSystem->setCamera(space.mCamera);
	ethSystemExecute(*space.renderSystem, 0.0f);

	space._hud->hudSystem->drawHud();

	ethSpaceDraw(space._dbgSpace);

	glPopMatrix();
}

template<>
KECode spaces<GameSpace>::dispose(GameSpace& space) {
	return KE_SUCCESS;
}
