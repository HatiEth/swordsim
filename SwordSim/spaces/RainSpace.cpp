/*
 * RainSpace.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: Hati
 */

#include "RainSpace.h"
#include <window/InputService.h>

void initShaders(RainSpace& space) {

	//reinit code here
	space._rainProgram = eth::createProgram();
	eth::ShaderHandle rainFrag;
	rainFrag = eth::createShader("shaders/rain.frag");

	eth::attachShader(space._rainProgram, rainFrag);

	eth::linkProgram(space._rainProgram);

	// load texture display shader
	space._displayTextureProgram = eth::createProgram();
	eth::ShaderHandle displayFrag = eth::createShader("displayTexture.frag");
	eth::attachShader(space._displayTextureProgram, displayFrag);
	eth::linkProgram(space._displayTextureProgram);

}

void initAdvectInput(RainSpace& space) {
	space._noise = createTexture();
	generateColorNoise(space._noise);
}

template<>
KECode spaces<RainSpace>::initialize(RainSpace& space) {
	space.mCamera.resize(800, 600);
	space.mCamera.setZoom(0.0f);
	space.mCamera.update(0.0f);

	srand(0);
	space._runningState = RainSpace::Stepping;
	glEnable(GL_TEXTURE_2D);

	initAdvectInput(space);
	initShaders(space);

	createDoubleBuffer(space._rainAdvect);

	return KE_SUCCESS;
}

template<>
void spaces<RainSpace>::update(RainSpace& space, float dt) {
	InputService* input = service<InputService>::get();
	if (input->isKeyPushed(GLFW_KEY_P)) {
		if (space._runningState != RainSpace::Play) {
			space._runningState = RainSpace::Play;
			KE_Notify("RainSpace::_runningState = Play");
		} else {
			space._runningState = RainSpace::Stepping;
			KE_Notify("RainSpace::_runningState = Step");
		}

	}
	if (input->isKeyPushed(GLFW_KEY_R)) { //break on not R
		if (space._runningState != RainSpace::Stepping) {
			space._runningState = RainSpace::Stepping;
			KE_Notify("RainSpace::_runningState = Step");
		}
		space.step(dt);
	}

	if (space._runningState == RainSpace::Play) {
		space.step(dt);
	}
}

template<>
void spaces<RainSpace>::render(RainSpace& space) {
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	useCamera(space.mCamera);
	int otherId = (space._rainAdvect.activeBuffer + 1) % 2;
	eth::enableProgram(space._displayTextureProgram);
	GLuint pLoc = eth::programUniformLocation(space._displayTextureProgram,
			"uTexture");
	glUniform1i(pLoc, 0);

	displayTexture(space._rainAdvect.textures[otherId]);

	glUseProgram(0);
	glPopMatrix();
}

template<>
KECode spaces<RainSpace>::dispose(RainSpace& space) {
	return KE_SUCCESS;
}

void RainSpace::step(float dt) { // Executed code on run
	const GLenum drawBuffers[] = {
	GL_COLOR_ATTACHMENT0 };
	glBindFramebuffer(GL_FRAMEBUFFER,
			_rainAdvect.buffers[_rainAdvect.activeBuffer]);
	GLuint otherBufferId = (_rainAdvect.activeBuffer + 1) % 2;

	eth::enableProgram(_rainProgram);
	glDrawBuffers(1, &drawBuffers[0]);
	{
		glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
		eth::bindTexture2d(0, _rainAdvect.textures[otherBufferId]);
		eth::bindTexture2d(1, _noise.textureId);

		GLint uRainAdvectLoc = eth::programUniformLocation(_rainProgram, "uRainAdvect");
		GLint uEmitterLoc = eth::programUniformLocation(_rainProgram, "uEmitter");

		glUniform1i(uRainAdvectLoc, 0);
		glUniform1i(uEmitterLoc, 1);

		glm::vec2 halfExtent(400, 300);
		glColor4f(1, 1, 1, 1);
		glBegin(GL_TRIANGLE_STRIP);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex2f(-halfExtent.x, -halfExtent.y);
			glTexCoord2f(0.0f, 1.0f);
			glVertex2f(-halfExtent.x, halfExtent.y);
			glTexCoord2f(1.0f, 0.0f);
			glVertex2f(halfExtent.x, -halfExtent.y);
			glTexCoord2f(1.0f, 1.0f);
			glVertex2f(halfExtent.x, halfExtent.y);
		}
		glEnd();


	}



	swapDoubleBuffer(_rainAdvect);
	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	eth::unbindTexture2d(1);
	eth::unbindTexture2d(0);
}
