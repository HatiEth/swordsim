#ifndef BUCKET_H_
#define BUCKET_H_
#include <Utility.h>
#include "MemDefines.h"

/** 
 * @brief Bucket Memory Allocator 
 * @details Creates separate fixed-size buckets for memory allocations from given memory.
 * @todo test if works properly
 * @todo offset viability (for <4byte)
 */
class Bucket {
protected:
    /// Pointer to next bucket
    Bucket* next;
public:


    /// input will be char[]
    Bucket(void* start, void* end, size_t bucketSize) {

            union {
                void* as_void;
                char* as_char;
                Bucket* as_bucket;
            };
            as_void = start;

            next = as_bucket;
            size_t numBuckets = ((char*)end - (char*)start) / bucketSize;
            //KE_ConditionalWarning((((char*)end - (char*)start) % bucketSize) == 0, KE_WARNING_STR,
            //        "Wasting memory, bucketSize does not match given memory block.");
            Bucket* writer = next;
            for (size_t i = 0; i < numBuckets; ++i) {
                writer = as_bucket;
                as_char += bucketSize;
                writer->next = as_bucket;
            }
            writer->next = nullptr;
        }


    Bucket(void* start, size_t totalSize, size_t bucketSize) {
//    	memset(start, totalSize/2, 0xbeef);
        union {
            void* as_void;
            char* as_char;
            Bucket* as_bucket;
        };
        as_void = start;

        next = as_bucket;
        size_t numBuckets = (totalSize) / bucketSize;

        KE_ConditionalWarning((totalSize % bucketSize) == 0, KE_WARNING_STR,
                "Wasting memory, bucketSize does not match given memory block.");
        Bucket* writer = next;
        for (size_t i = 0; i < numBuckets; ++i) {
            writer = as_bucket;
            as_char += bucketSize;
            writer->next = as_bucket;
        }
        writer->next = nullptr;
    }

	Bucket(size_t totalSize, size_t bucketSize) : next(nullptr) {
		void* start = malloc(totalSize);

		union {
			void* as_void;
			char* as_char;
			Bucket* as_bucket;
		};
		as_void = start;

		next = as_bucket;
		size_t numBuckets = (totalSize) / bucketSize;
		KE_ConditionalWarning((totalSize % bucketSize) == 0, KE_WARNING_STR,
		        "Wasting memory, bucketSize does not match given memory block.");
		Bucket* writer = next;
		for (size_t i = 0; i < numBuckets; ++i) {
			writer = as_bucket;
			as_char += bucketSize;
			writer->next = as_bucket;
		}
		writer->next = nullptr;
	}

    /// @brief Returns a bucket from the pool
    void* obtain() {
		KE_Assert(next != nullptr, "Trying to obtain memory from no available");
        if (next == nullptr) {
            return nullptr;
        }

        Bucket* head = next;
        next = head->next;
        return head;
    }

    /// returns a bucket into the freelist
    void free(void* ptr) {
		KE_Assert(ptr != nullptr, "Trying to free a nullptr!");
        // interpret as bucket
        Bucket* b = static_cast<Bucket*>(ptr);
        // make initial 
		b->next = this->next;
		
        this->next = b;
    }

};

typedef Bucket BucketAllocator;

#endif /* BUCKET_H_ */
