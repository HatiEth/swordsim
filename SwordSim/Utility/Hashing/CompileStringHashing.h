/*
 * CompileStringHashing.h
 *
 *  Created on: Oct 9, 2013
 *      Author: Hati
 */

#ifndef COMPILESTRINGHASHING_H_
#define COMPILESTRINGHASHING_H_
#include <cstring>

namespace eth {
    static inline unsigned int calculateFNV1a(const char* str) {
        const size_t len = strlen(str)+1;
        unsigned int val = 2166136261u;
        for (size_t i = 0; i < len; ++i) {
            val ^= *str++;
            val *= 16777619u;
        }
        return val;
    }
    struct StringHash {
        const unsigned int _hashValue;
        struct ConstCharPointer{
            inline ConstCharPointer(const char* str) : _value(str) {}
            const char* _value;
            
        };
        StringHash(ConstCharPointer nonconststr) :
                _hashValue(calculateFNV1a(nonconststr._value)) {
        }
        
        StringHash(const char (&constStr)[1]) : _hashValue((2166136261u^ constStr[0]) * 16777619u) {
            
        }
        
    };
}


#endif /* COMPILESTRINGHASHING_H_ */
