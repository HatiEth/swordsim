#ifndef KEHASH_H_
#define KEHASH_H_
#include <tr1/unordered_set>
#include <tr1/unordered_map>
#include <string.h>

namespace std {
#if __GNUC__
    namespace tr1 {

    }
#else 
template<> struct hash<const char*> {
    inline size_t operator()(const char* val) {
        unsigned long h = 0;
        for (; *val; ++val)
        h = 5 * h + *val;
        return size_t(h);
    }
};
#endif
}

struct eqstr {
inline size_t operator()(const char* val) const {
    size_t h = 1;
    for (; *val; ++val)
        h = 5 * h + *val;
    return size_t(h);
}
bool operator()(const char* s1, const char* s2) const {
    return strcmp(s1, s2) == 0;
}
bool operator()(const char*& s1, const char*& s2) const {
    return strcmp(s1, s2) == 0;
}

inline size_t hash(const char* val) const {
    size_t h = 1;
    for (; *val; ++val)
        h = 5 * h + *val;
    return size_t(h);
}
};

/** @file  
 *** @addtogroup KEHash Hashing
 *** @{
 **/

namespace eth {

/**	@brief Enables correct hashing for std::unordered_map
 *** @details std::unordered_map hashes pointers by numeric value instead of correct hashing
 **/
template<typename T>
struct StringMap {
#ifdef COMPILETIME_HASHING //NYI
    typedef std::tr1::unordered_map<const char*, T, ::eqstr, ::eqstr> Type;
#else
#ifdef __GNUC__
    typedef std::tr1::unordered_map<const char*, T, ::eqstr, ::eqstr> Type;
#else
    typedef std::unordered_map<const char*, T, ::std::hash<const char*>, ::eqstr> Type;
#endif
#endif
};

#ifdef __GNUC__
	typedef std::tr1::unordered_set<const char*, ::eqstr, eqstr> StringHashSet;
#else
#endif
}
/// @}

#endif /* KEHASH_H_ */
