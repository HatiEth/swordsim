#ifndef KELOGGER_H_
#define KELOGGER_H_


#include <iostream>
#include <string>
#include <sstream>

class KELogger {
protected:
    virtual void write(const char*) {
    }
public:

    KELogger(void) {
    }

    virtual ~KELogger(void) {
    }
    template<typename T>
    friend KELogger& operator<<(KELogger&, T val);

    friend KELogger& operator<<(KELogger&, const char* ptr_val);
    KELogger& print(const char* ptr_val) {
        write(ptr_val);
        return *this;
    }
};
template<typename T>
KELogger& operator<<(KELogger& log, T val) {
    std::stringstream ss;
    ss << val;
    log.write(ss.str().c_str());
    return log;
}

class BasicLogger: public KELogger {
protected:
    virtual void write(const char* msg) {
        std::cout << msg << std::endl;
    }

};

#endif /* KELOGGER_H_ */
