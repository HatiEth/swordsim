/*
 * Timer.h
 *
 *  Created on: Jul 27, 2013
 *      Author: Hati
 */

#ifndef TIMER_H_
#define TIMER_H_

namespace eth {
    template<typename T>
    struct GenericTimer {
        T timeAccumulator;
        const T updateFrequency;
        T currentTimestamp;
        T lastTimestamp;

        T elapsedTime;

        GenericTimer(const T updateFrequency) :
                timeAccumulator(T(0)), updateFrequency(updateFrequency), currentTimestamp(T(0)), lastTimestamp(T(0)), elapsedTime(T(0)) {
        }

        ///@brief increments accumulator
        GenericTimer<T> operator+=(const T& value) {
            this->timeAccumulator += value;
            return *this;
        }

        GenericTimer<T> operator-=(const T& value) {
            this->timeAccumulator -= value;
            return *this;
        }

        /// @brief will update current time
        GenericTimer<T> operator=(const T& value) {
            currentTimestamp = value;
            elapsedTime = currentTimestamp - lastTimestamp;
            lastTimestamp = currentTimestamp;
#ifndef NDEBUG
            if (updateFrequency < elapsedTime) {
                elapsedTime = updateFrequency;
            }
#endif
            return *this;
        }
    };
}
typedef eth::GenericTimer<float> FloatTimer;
typedef eth::GenericTimer<int> IntTimer;

#endif /* TIMER_H_ */
