/*
 * SwordCombatTestState.cpp
 *
 *  Created on: Nov 10, 2013
 *      Author: Hati
 */

#include "SwordCombatTestState.h"

SwordCombatTestState::SwordCombatTestState(const StateMachine& handle) : State(handle) {
}

SwordCombatTestState::~SwordCombatTestState() {
}

void SwordCombatTestState::update(float dt) {
	spaces<SwordCombatSpace>::update(this->swordCombatSpace, dt);
}

void SwordCombatTestState::render() {
	spaces<SwordCombatSpace>::render(this->swordCombatSpace);
}

KECode SwordCombatTestState::dispose(State* nextState) {
	return KE_SUCCESS;
}

KECode SwordCombatTestState::initialize() {
	spaces<SwordCombatSpace>::initialize(this->swordCombatSpace);
	return KE_SUCCESS;
}
