/*
 * GameState.h
 *
 *  Created on: 10.12.2013
 *      Author: Hati
 */

#ifndef GAMESTATE_H_
#define GAMESTATE_H_

#include <states/State.h>
#include <spaces/GameSpace.h>
#include <spaces/HudSpace.h>

class GameState: public State {
	GameSpace _game;
public:
	GameState(StateMachine& sm) : State(sm) {}

	void update(float deltaTime);

	void render();

	KECode dispose(State* nextState);

	KECode initialize();
};

#endif /* GAMESTATE_H_ */
