
/*
 * StateMachine.cpp
 *
 *  Created on: Aug 7, 2013
 *      Author: Hati
 */
#include "StateMachine.h"

void StateMachine::update(float delta) {
	KE_Assert(currentState!=nullptr, "No state set! Invalid behaviour!");
    currentState->update(delta);
}

KECode StateMachine::changeState(const char* stateId) {
    if(states.find(stateId) == states.end() ) {
        return KE_CustomError("Cannot change state to %s! No such state found!", stateId);
    }

    State* state = states[stateId];
    if(state == currentState) {
        return KE_CustomWarning("Cannot change from current to current state! (%s)", stateId);
    }
    KE_Notify("Entering state: %s", stateId);

    State* disposeState = nullptr;
    
    if(currentState!=nullptr) {
        disposeState = currentState;
    }
    ///@todo initialize only if not currently initialize
    state->initialize();
    state->enter(currentState);
    
    currentState = state;
    
    if(disposeState)
        disposeState->dispose(state);
    
    return KE_SUCCESS;
}

void StateMachine::render() {
	KE_Assert(currentState!=nullptr, "No state set! Invalid behaviour!");
    currentState->render();
}
