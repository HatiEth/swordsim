/*
 * CutEffectState.h
 *
 *  Created on: 23.12.2013
 *      Author: Hati
 */

#ifndef CUTEFFECTSTATE_H_
#define CUTEFFECTSTATE_H_

#include <spaces/testspaces/CutEffectSpace.h>
#include <states/State.h>

class CutEffectState : public State {
	CutEffectSpace stateSpace;
public:
	CutEffectState(const StateMachine& handle);
	virtual ~CutEffectState() {}

	void update(float deltaTime);

	void render();

	KECode dispose(State* nextState);

	KECode initialize();
};

#endif /* CUTEFFECTSTATE_H_ */
