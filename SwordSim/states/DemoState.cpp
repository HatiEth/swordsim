/*
 * DemoState.cpp
 *
 *  Created on: Nov 6, 2013
 *      Author: Hati
 */

#include "DemoState.h"

DemoState::DemoState(const StateMachine& handle) : State(handle) {
}


DemoState::~DemoState() {
}

void DemoState::update(float deltaTime) {
//	spaces<TestCutSpace>::update(_stateSpace, deltaTime);
	spaces<RainSpace>::update(_rainSpace, deltaTime);
}

void DemoState::render() {
	spaces<RainSpace>::render(_rainSpace);
//	spaces<TestCutSpace>::render(_stateSpace);
//	spaces<FBODemoSpace>::render(_demoSpace);
}

KECode DemoState::dispose(State* nextState) {

	return KE_SUCCESS;
}


KECode DemoState::initialize() {
	KE_Assert(stateInfo != nullptr, "No state info for spaces!");
	_stateSpace.window = (GLFWwindow*)stateInfo;
//	spaces<TestCutSpace>::initialize(_stateSpace);
//	spaces<FBODemoSpace>::initialize(_demoSpace);
	spaces<RainSpace>::initialize(_rainSpace);
	return KE_SUCCESS;
}
