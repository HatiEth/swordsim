/*
 * SwordCombatTestState.h
 *
 *  Created on: Nov 10, 2013
 *      Author: Hati
 */

#ifndef SWORDCOMBATTESTSTATE_H_
#define SWORDCOMBATTESTSTATE_H_

#include <spaces/testspaces/SwordCombatSpace.h>
#include "State.h"

class SwordCombatTestState: public State {
	SwordCombatSpace swordCombatSpace;
public:
	SwordCombatTestState(const StateMachine& handle);
	virtual ~SwordCombatTestState();

	void update(float deltaTime);

	void render();

	KECode dispose(State* nextState);

	KECode initialize();
};

#endif /* SWORDCOMBATTESTSTATE_H_ */
