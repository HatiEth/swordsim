/*
 * StateMachine.h
 *
 *  Created on: Aug 7, 2013
 *      Author: Hati
 */

#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_
#include <Utility.h>
#include "State.h"

class StateMachine {
    typedef eth::StringMap<State*>::Type StateMap;
    StateMap states;
    
    State* currentState;
public:
    StateMachine() : currentState(nullptr) {
        
    }
    virtual ~StateMachine() {
        ///@todo dispose all states here
    }

    /**
     * updates the state machine
     * @param delta
     */
    void update(float delta);
    void render();
    
    KECode changeState(const char* stateId);
    
    template <typename state>
    void registerState(const char* stateId, void* stateInfo = nullptr) {
        if(states.find(stateId) != states.end()) {
            KE_CustomWarning("Overriding state %s (%d)", stateId, states[stateId]);
            
            if(currentState == states[stateId]) {
                KE_CustomError("Trying to change current state %s with another state!", stateId);
            }
            
            State* overridenState = states[stateId];
            overridenState->dispose(overridenState);
        }
        ///@todo variadic template for constructor check and stateInfo as parameters
        State* s = new state(*this);
        s->stateInfo = stateInfo;
        states[stateId] = s;
    }
    
    void manageKey(int key, int scancode, int action, int mods) {
        if(currentState==nullptr)
            return;
        currentState->manageKey(key,scancode,action,mods);
    }
    
    void manageMouse(int key, int action, int mods, double& posX, double& posY) {
        if(currentState==nullptr)
                    return;
        currentState->manageMouse(key, action, mods, posX, posY); 
    }
    
    void resize(int w, int h) {
        
    }

};
#endif /* STATEMACHINE_H_ */
