/*
 * TextureAdvectionState.cpp
 *
 *  Created on: 18.11.2013
 *      Author: Hati
 */

#include <states/TextureAdvectionState.h>

TextureAdvectionState::TextureAdvectionState(const StateMachine& handle) : State(handle) {
}


void TextureAdvectionState::update(float deltaTime) {
	spaces<TexAdvSpace>::update(this->texAdv, deltaTime);
}

void TextureAdvectionState::render() {
	spaces<TexAdvSpace>::render(this->texAdv);
}

KECode TextureAdvectionState::dispose(State* nextState) {
	return KE_SUCCESS;
}


KECode TextureAdvectionState::initialize() {
	texAdv.window = (GLFWwindow*)this->stateInfo;
	spaces<TexAdvSpace>::initialize(this->texAdv);
	return KE_SUCCESS;
}
