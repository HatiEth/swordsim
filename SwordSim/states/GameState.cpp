/*
 * GameState.cpp
 *
 *  Created on: 10.12.2013
 *      Author: Hati
 */

#include <states/GameState.h>

void GameState::update(float deltaTime) {
	spaces<GameSpace>::update(_game, deltaTime);
}

void GameState::render() {
	spaces<GameSpace>::render(_game);
}

KECode GameState::dispose(State* nextState) {
	return KE_SUCCESS;
}

KECode GameState::initialize() {
	spaces<GameSpace>::initialize(_game);

	return KE_SUCCESS;
}
