/*
 * DemoState.h
 *
 *  Created on: Nov 6, 2013
 *      Author: Hati
 */

#ifndef DEMOSTATE_H_
#define DEMOSTATE_H_

#include "State.h"
#include <spaces/testspaces/TestCutSpace.h>
#include <spaces/FBODemoSpace.h>
#include <spaces/RainSpace.h>

class DemoState : public State {
	TestCutSpace _stateSpace;
	FBODemoSpace _demoSpace;
	RainSpace _rainSpace;
public:
	DemoState(const StateMachine& handle);
	virtual ~DemoState();

	void update(float deltaTime);

	void render();

	KECode dispose(State* nextState);

	KECode initialize();
};

#endif /* DEMOSTATE_H_ */
