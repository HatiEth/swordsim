/*
 * State.h
 *
 *  Created on: Aug 7, 2013
 *      Author: Hati
 */

#ifndef STATE_H_
#define STATE_H_
#include <Utility/ErrorHandling/ErrorHandling.h>

class StateMachine;

class State {
protected:
    const StateMachine& stateMachine;
    void* stateInfo;
    friend class StateMachine;
public:
    State(const StateMachine& stateHandler);
    virtual ~State();
    
    /**
     * updates the state by deltaTime
     * @param deltaTime elapsed time used for interpolation
     */
    virtual void update(float deltaTime) =0;
    
    /**
     * renders the current state
     */
    virtual void render() =0;
    
    /**
     * disposes resources of this state depending on the next state entering
     * @param currentState
     * @return ke succession flag
     */
    virtual KECode dispose(State* nextState)=0;
    
    /**
     * initializes the state
     * @return ke succession flag
     */
    virtual KECode initialize()=0;
    
    
    /**
     * enters this state 
     * @param lastState
     * @return
     */
    KECode enter(State* lastState) {
//        KECode disposeCode = lastState->dispose(this);
        
        return KE_SUCCESS;
    }
    
    virtual void manageKey(int key, int scancode, int action, int mod) {
        
    }
    
    virtual void manageMouse(int key, int action, int mod, double& posX, double& posY) {
            
    }
};



#endif /* STATE_H_ */
