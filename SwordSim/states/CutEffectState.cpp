/*
 * CutEffectState.cpp
 *
 *  Created on: 23.12.2013
 *      Author: Hati
 */

#include <states/CutEffectState.h>

CutEffectState::CutEffectState(const StateMachine& handle) : State(handle) {
}

void CutEffectState::update(float deltaTime) {
	spaces<CutEffectSpace>::update(stateSpace, deltaTime);
}

void CutEffectState::render() {
	spaces<CutEffectSpace>::render(stateSpace);
}

KECode CutEffectState::dispose(State* nextState) {
	spaces<CutEffectSpace>::dispose(stateSpace);
	return KE_SUCCESS;
}

KECode CutEffectState::initialize() {
	spaces<CutEffectSpace>::initialize(this->stateSpace);

	return KE_SUCCESS;
}
