/*
 * TextureAdvectionState.h
 *
 *  Created on: 18.11.2013
 *      Author: Hati
 */

#ifndef TEXTUREADVECTIONSTATE_H_
#define TEXTUREADVECTIONSTATE_H_

#include <states/State.h>
#include <spaces/testspaces/TexAdvSpace.h>

class TextureAdvectionState: public State {
	TexAdvSpace texAdv;
public:
	TextureAdvectionState(const StateMachine& handle);
	virtual ~TextureAdvectionState() {}

	void update(float deltaTime);

	void render();

	KECode dispose(State* nextState);

	KECode initialize();
};

#endif /* TEXTUREADVECTIONSTATE_H_ */
