/*
 * HealthSystem.h
 *
 *  Created on: Dec 4, 2013
 *      Author: Hati
 */

#ifndef HEALTHSYSTEM_H_
#define HEALTHSYSTEM_H_

#include "System.h"
#include <nodes/HealthNode.h>
#include <vector>

struct HealthSystem : public System {
	std::vector<HealthNode> _nodes;
};

LINK_SYSTEM(HealthSystem, HealthNode);


#endif /* HEALTHSYSTEM_H_ */
