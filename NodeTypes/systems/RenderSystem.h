/*
 * RenderSystem.h
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#ifndef RENDERSYSTEM_H_
#define RENDERSYSTEM_H_

#include <nodes/RenderNode.h>
#include "System.h"
#include <vector>

struct RenderSystem : public System {
	std::vector<RenderNode> _nodes;
	NodeInfo<RenderNode> _info;
};

LINK_SYSTEM(RenderSystem, RenderNode);

#endif /* RENDERSYSTEM_H_ */
