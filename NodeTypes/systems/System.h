/*
 * System.h
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <entity/Entity.h>
#include <nodes/Node.h>
#include <set>

/**
 * Base class for systems working on nodes
 */
struct System {

};
/*
template<typename SystemType, typename NodeType>
struct SystemInfo {
	static_assert(std::tr1::is_base_of<System, SystemType>::value, "Trying to register systeminfo for non-system type!");

	static NodeInfo<NodeType>& _info;
};
*/

struct SystemRequirements {
	typedef eth::StringMap<eth::StringHashSet>::Type SystemRequirementMap;

	static SystemRequirementMap& requirements() {
		static SystemRequirementMap sRequirements;
		return sRequirements;
	}

	static bool requires(const char* sysAlias, const char* componentAlias) {
		KE_Assert(requirements().find(sysAlias)!=requirements().end(), "Unregistered systemAlias %s!", sysAlias);
		const auto& sysInfoIt = requirements().find(sysAlias);
		return sysInfoIt->second.find(componentAlias)!=sysInfoIt->second.end();
	}

	static bool checkNodeGeneration(const char* systemAlias, const EntityInfo& entityInfo) {
		/// @todo may there is a faster approach to do this (STL functions or something)
		bool summedBool = true;
		for(const auto& cIt : entityInfo._components) {
			requires(systemAlias, cIt.first);
		}
		return summedBool;
	}

	template<typename SystemType, typename NodeType>
	struct SystemInfo {

		static bool requires(const char* componentAlias) {
			const auto& sysInfoIt = requirements().find(alias);
			return sysInfoIt->second.find(componentAlias)!=sysInfoIt->second.end();
		}

		static const char* linkSystem(const char* systemAlias) {
			if(requirements().find(systemAlias) != requirements().end()) {
				KE_CustomWarning("Overwriting system-alias %s with %s",
										requirements().find(systemAlias)->first, systemAlias);
			}
			else {
				KE_Notify("Registering system-alias %s", systemAlias);
			}

			requirements()[systemAlias] = NodeInfo<NodeType>::requirements();
			return systemAlias;
		}


		static const char* alias;
	};
};

//using SystemRequirements = SystemInfo;

#define SYSTEM_NAME(x) (#x)
#define LINK_SYSTEM(SystemType, NodeType) template<> const char* SystemRequirements::SystemInfo<SystemType, NodeType>::alias = SystemRequirements::SystemInfo<SystemType, NodeType>::linkSystem(SYSTEM_NAME(SystemType));



/**
 * Zugriffsprototype:
 * 	ComponentDescription := name of component in file ("Sprite", "Transform", "Item", etc.)
 * 	func(Component* nc, ComponentDescription& cdesc) { //called post add
 * 	* 	std::map<SystemName, System> systems
 * 	* 	foreach(SystemName sys : systems) {
 * 	* 		if(sysRequirements(sys).requires(cdesc) && sysRequirements(sys).checkEntity(entity)) {
 * 	*			createNode(sys, entityData);
 * 	*		}
 * 	* 	}
 * 	}
 */




#endif /* SYSTEM_H_ */
