/*
 * main.cpp
 *
 *  Created on: Dec 3, 2013
 *      Author: Hati
 */

#include <nodes/RenderNode.h>
#include <components/HealthComponent.h>
#include <systems/RenderSystem.h>
#include <systems/HealthSystem.h>
#include <entity/Entity.h>


void testEntityAdd(EntityInfo& e) {
	e.add("Health"); /// requires header to be linked somewhere
	KE_Assert(e.has("Health"), "No component found!");

}

void testEntityRemove(EntityInfo& e) {
	e.remove("Health"); /// requires header to be linked somewhere
	KE_Assert(!e.has("Health"), "Component still there found!");

}

int main(int argc, char** argv) {
	EntityID test{0};

	EntityInfo entity{test};

	testEntityAdd(entity);

	KE_Notify("HealthNode requires: ");
	for(auto it : NodeInfo<HealthNode>::requirements()) {
		KE_Notify(" %s", it);
	}

	KE_Assert(SystemRequirements::requires(SYSTEM_NAME(HealthSystem), "Health"), "RenderSystem does requires Sprite component");
	KE_Assert(SystemRequirements::checkNodeGeneration(SYSTEM_NAME(HealthSystem), entity), "Entity should have health component!");

	testEntityRemove(entity);
	RenderNode rn;
	KE_Notify("RenderNode requires: ");
	for(auto it : NodeInfo<RenderNode>::requirements()) {
		KE_Notify(" %s", it);
	}


//	KE_Notify("%u", SystemInfo<RenderSystem, RenderNode>::_info.requires("Sprite"));

	KE_Assert(SystemRequirements::requires("RenderSystem", "Unknown")==false, "RenderSystem does not require Unknown component");
	KE_Assert(SystemRequirements::requires("RenderSystem", "Sprite"), "RenderSystem does requires Sprite component");

	return 0;
}

