/*
 * Node.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Hati
 */

#ifndef NODE_H_
#define NODE_H_

#include <entity/Entity.h>
#include <Utility.h>

/**
 * @todo maybe generate something to create nodes by macro ?
 */

struct Node {
	EntityID _internalId;
};

/**
 * Defines which components are required by a certain node
 */
template<typename nodeType>
struct NodeInfo {
	static_assert(std::tr1::is_base_of<Node, nodeType>::value, "Trying to register nodeinfo for non-node type!");

	typedef eth::StringHashSet RequiredComponentSet;
	static RequiredComponentSet& requirements() {
		static RequiredComponentSet _requirements;
		return _requirements;
	}

	static NodeInfo<nodeType>& get() {
		return _nodeInfo;
	}

	static NodeInfo<nodeType>& add(const char* componentAlias) {
		requirements().insert(componentAlias);
		return _nodeInfo;
	}

	static bool requires(const char* componentAlias) {
		return requirements().find(componentAlias)!=requirements().end();
	}

	static NodeInfo<nodeType> _nodeInfo;
};

#define NODE_REQUIRE(NodeType) template<> NodeInfo<NodeType> NodeInfo<NodeType>::_nodeInfo = NodeInfo<NodeType>::get()

#endif /* NODE_H_ */
