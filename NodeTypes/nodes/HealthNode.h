/*
 * HealthNode.h
 *
 *  Created on: Dec 4, 2013
 *      Author: Hati
 */

#ifndef HEALTHNODE_H_
#define HEALTHNODE_H_

#include "Node.h"
#include <components/HealthComponent.h>

struct HealthNode : public Node {
	HealthComponent* hc;
};

NODE_REQUIRE(HealthNode).add("Health");

#endif /* HEALTHNODE_H_ */
