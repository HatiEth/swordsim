/*
 * RenderNode.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Hati
 */

#ifndef RENDERNODE_H_
#define RENDERNODE_H_

#include <components/TransformComponent.h>
#include <components/SpriteComponent.h>
#include "Node.h"

struct RenderNode : public Node {
	TransformComponent* position;
	SpriteComponent* sprite;
};

NODE_REQUIRE(RenderNode).add("Transform").add("Sprite");

#endif /* RENDERNODE_H_ */
