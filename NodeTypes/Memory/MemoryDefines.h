/*
 * MemoryDefines.h
 *
 *  Created on: Jul 12, 2013
 *      Author: Hati
 */

#ifndef MEMORYDEFINES_H_
#define MEMORYDEFINES_H_

#define BYTE(n) n
#define KILOBYTE(n) (BYTE(n)<<10)
#define MEGABYTE(n) (KILOBYTE(n)<<10)

#endif /* MEMORYDEFINES_H_ */
