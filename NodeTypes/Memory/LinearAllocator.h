/*
 * LinearAllocator.h
 *
 *  Created on: Jul 11, 2013
 *      Author: Hati
 */

#ifndef LINEARALLOCATOR_H_
#define LINEARALLOCATOR_H_
#include "../Utility/ErrorHandling/ErrorHandling.h" 

namespace eth {

    union MemConversion {
        char* chPtr;
        void* ptr;
    };

    class LinearAllocator {
        char* memStart;
        char* memCurrent;
        char* memEnd;
    public:
        LinearAllocator(size_t bufferSize) {
            memStart = (char*) malloc(bufferSize);
            KE_Assert(memStart != nullptr, "Cannot allocate %u bytes", bufferSize);
            memCurrent = memStart;
            memEnd = memStart + bufferSize;
        }

        LinearAllocator(void* start, void* end) :
                memStart((char*) start), memCurrent((char*) start), memEnd((char*) end) {
        }

        ~LinearAllocator() {
        }

        void* allocate(size_t requestedMem) {
            KE_Assert((size_t(memEnd - memCurrent) >= requestedMem), "Cannot allocate: %d Byte. Allocator misses %d Byte.", requestedMem,
                    requestedMem - (memEnd - memCurrent));

            void* userptr = memCurrent;
            memCurrent += requestedMem;

            return userptr;
        }

        void free(void* ptr) {
            // unable to free certain ptr in linear allocation

        }

        void reset(void) {
            // invalidate memory
            memset(memStart, 0xbeef, memEnd - memStart);
            memCurrent = memStart;

        }
    };

} /* namespace ke */
#endif /* LINEARALLOCATOR_H_ */
