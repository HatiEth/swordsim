/*
 * StackAllocator.h
 *
 *  Created on: Jul 11, 2013
 *      Author: Hati
 */

#ifndef STACKALLOCATOR_H_
#define STACKALLOCATOR_H_

#include "../Utility/ErrorHandling/ErrorHandling.h"
#include <stdint.h>

namespace {
    static const size_t STACK_OFFSET = sizeof(uint32_t);
    static_assert(STACK_OFFSET==4, "Stack offset not 4 Bytes");
}

namespace eth {

    class StackAllocator {
        char* memStart;
        char* stackTop;
        char* memEnd;
    public:
        StackAllocator(size_t bufferSize) {
            memStart = (char*) malloc(bufferSize);
            KE_Assert(memStart!=nullptr, "Cannot allocate %u bytes!", bufferSize);
            stackTop = memStart;
            memEnd = memStart + bufferSize;
        }

        StackAllocator(void* memBegin, void* memEnd) : memStart((char*)memBegin), stackTop((char*)memBegin), memEnd((char*)memEnd) {}

        ~StackAllocator() {
        }

        void* allocate(size_t requestedMem) {

            size_t totalSizeOfAllocation = requestedMem + STACK_OFFSET;
            KE_Assert(((size_t)(memEnd - stackTop) < totalSizeOfAllocation), "Cannot allocate: %d Byte. Allocator misses %d Byte.", requestedMem,
                    totalSizeOfAllocation - (memEnd - stackTop));

            void* userptr = stackTop;
            stackTop += requestedMem;
            *stackTop = static_cast<uint32_t>(STACK_OFFSET);
            stackTop += STACK_OFFSET;

            return userptr;
        }

        void free(void* ptr) {
            KE_Assert(ptr != memStart, "Trying to pop from allocator start");
            union {
                void* voidptr;
                char* memptr;
                uint32_t* as_uint32_t;
            };

            voidptr = ptr;
            //read stack-offset
            memptr -= STACK_OFFSET;
            //reduce pointer by stack-offset
            KE_Assert(memptr - (*as_uint32_t) < memStart, "Offset invalidated! Offset: %d", *as_uint32_t);
            memptr -= (*as_uint32_t);
            stackTop = memptr;
        }
    };

} /* namespace ke */
#endif /* STACKALLOCATOR_H_ */
