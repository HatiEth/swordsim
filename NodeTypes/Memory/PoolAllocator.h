/*
 * PoolAllocator.h
 *
 *  Created on: Jul 12, 2013
 *      Author: Hati
 */

#ifndef POOLALLOCATOR_H_
#define POOLALLOCATOR_H_

#include <Utility.h>
#include "BucketAllocator.h"

template<size_t BucketSize>
class PoolAllocator {
    Bucket bucketBegin;
public:
    PoolAllocator(void* memBegin, void* memEnd) :
            bucketBegin(memBegin, memEnd, BucketSize) {
    }

    PoolAllocator(size_t size) {
        void* memStart = malloc(size);
        // initialize initial bucket, Initiaception
        bucketBegin(memStart, memStart + size, BucketSize);
    }

    void* allocate(size_t memSize = 0) {
//        KE_Assert(memSize < BucketSize, "Cannot allocate more than size of a single bucket! (No Multi bucket support)");
        // just call obtain on the initial bucket
        return bucketBegin.obtain();
    }

    void free(void* ptr) {
        // unable to free certain ptr in linear allocation
        bucketBegin.free(ptr);
    }

    void reset(void) {

    }
};

#endif /* POOLALLOCATOR_H_ */
