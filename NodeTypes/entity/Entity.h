/*
 * Entity.h
 *
 *  Created on: Nov 1, 2013
 *      Author: Hati
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include <Utility.h>
#include <components/Component.h>
#include <string.h>

typedef unsigned int EntityID;

// name - component map


struct EntityInfo {
	typedef eth::StringMap<Component*>::Type Components;
	EntityID _id;
	Components _components;

	template<typename C = Component>
	C get(const char* compName) {
		return (C*)_components[compName];
	}

	void add(const char* compName) {
		Component* comp = ComponentFactory::create(compName);
		if(_components.find(compName)!=_components.end()) {
			KE_CustomWarning("Overwriting %s at entity %u", compName, _id);
		}
		else {
			KE_Notify("Adding %s at entity %u", compName, _id);
		}
		_components[compName] = comp;
	}

	void remove(const char* compName) {
		KE_Assert(_components.find(compName)!=_components.end(), "Trying to remove unavailable component %s in %u", compName, _id);
		_components.erase(compName);
	}

	bool has(const char* compName) {
		return _components.find(compName)!=_components.end();
	}
};

struct EntityContainer {
	typedef std::tr1::unordered_map<EntityID, EntityInfo> Entities;
	Entities _entities;

	EntityInfo& entity(EntityID e) {
		return _entities[e];
	}



};


#endif /* ENTITY_H_ */
