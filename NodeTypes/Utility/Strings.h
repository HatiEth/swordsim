/*
 * Strings.h
 *
 *  Created on: Aug 16, 2013
 *      Author: Hati
 */

#ifndef STRINGS_H_
#define STRINGS_H_

#define STRINGIFY(x) (#x)

namespace eth {

/** @brief fast compares a string with a cmp value
 * @return true if str1==str2; otherwise false
 **/
inline bool fastStrEq(const char* str1, const char* str2) {
	for(;*str2;++str2) {
		if(*str1 != *str2) {
			return false;
		}
		++str1;
	}
	return true;
}
}

#endif /* STRINGS_H_ */
