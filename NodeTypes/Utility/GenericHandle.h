/*
 * GenericHandle.h
 *
 *  Created on: Nov 3, 2013
 *      Author: Hati
 */

#ifndef GENERICHANDLE_H_
#define GENERICHANDLE_H_
#include <stdint.h>
#include "ErrorHandling/ErrorHandling.h"
#include "CompileTimeFunctions.h"

template<size_t IndexSize, size_t RevSize>
struct GenericHandle {
	uint32_t index :IndexSize;
	uint32_t revision :RevSize;
};

/**
 * Free list working on generic handles, specifically does not destroy revision bits on free
 */
template<size_t IndexSize, size_t RevSize>
struct GenericHandleFreeList {
protected:
	typedef GenericHandle<IndexSize, RevSize> _Handle;
	typedef GenericHandleFreeList<IndexSize, RevSize> FreeList;
	FreeList* next;
public:

	GenericHandleFreeList(void* memPtr, size_t totalMemory) :
			next(nullptr) {
		union {
			void* as_void;
			FreeList* as_freeList;
			_Handle* as_handle;
		};
		as_void = memPtr;

		const size_t bucketSize = sizeof(_Handle);

		size_t buckets = totalMemory / bucketSize;
		next = as_freeList;

		_Handle* writer = as_handle;
		while (buckets > 0) {
			writer->index = sizeof(_Handle);
			writer->revision = 0;

			++writer;
			--buckets;
		}
	}

	GenericHandleFreeList(size_t memSize) :
			next(nullptr) {
		union {
			void* as_void;
			FreeList* as_freeList;
			_Handle* as_handle;
		};
		as_void = malloc(memSize);

		const size_t bucketSize = sizeof(_Handle);

		size_t buckets = memSize / bucketSize;
		next = as_freeList;
		KE_Assert(sizeof(_Handle) <= IndexSize / 8.0f,
				"IndexSize too to properly create initial free list!");
		_Handle* writer = as_handle;
		while (buckets > 0) {
			writer->index = sizeof(_Handle);
			writer->revision = 0;

			++writer;
			--buckets;
		}
	}

	GenericHandle<IndexSize, RevSize>* obtain();
	void free(GenericHandle<IndexSize, RevSize>* handle);
};

template<size_t IndexSize, size_t RevSize>
inline GenericHandle<IndexSize, RevSize>* GenericHandleFreeList<IndexSize,
		RevSize>::obtain() {
	typedef GenericHandle<IndexSize, RevSize> _Handle;
	typedef GenericHandleFreeList<IndexSize, RevSize> FreeList;

	union {
		FreeList* as_freelist;
		_Handle* as_handle;
	};
	as_freelist = next;

	_Handle* head = as_handle;
	next = static_cast<FreeList*>((void*) (size_t(as_freelist)
			+ size_t(as_freelist->next)));

	return head;
}

template<size_t IndexSize, size_t RevSize>
inline void GenericHandleFreeList<IndexSize, RevSize>::free(
		GenericHandle<IndexSize, RevSize>* handle) {
	typedef GenericHandle<IndexSize, RevSize> _Handle;
	typedef GenericHandleFreeList<IndexSize, RevSize> FreeList;

	union {
		FreeList* as_freelist;
		_Handle* as_handle;
	};
	as_freelist = next;

	_Handle* head = as_handle;
	size_t offset = (head - handle) * sizeof(_Handle);
	///@bug programs have to clean in creation order
//	if(!(offset < compiletime2PowNum<IndexSize>::value)) {
//		return;
//		KE_Notify("Offset(%u) to big for index value(%u)!", offset,
//			compiletime2PowNum<IndexSize>::value);
//	}
	KE_Assert(offset < compiletime2PowNum<IndexSize>::value,
			"Offset(%u) to big for index value(%u)!", offset,
			compiletime2PowNum<IndexSize>::value);

	handle->index = offset;
	as_handle = handle;

	next = as_freelist;
}

#endif /* GENERICHANDLE_H_ */
