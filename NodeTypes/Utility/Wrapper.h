/*
 * Wrapper.h
 *
 *  Created on: Jul 6, 2013
 *      Author: Hati
 */

#ifndef WRAPPER_H_
#define WRAPPER_H_
#include "ErrorHandling/ErrorHandling.h"

namespace eth {

    template<class T, class Derived>
    class Wrapper {
    protected:
        typedef T _wrapped_class;

        _wrapped_class* p;

        Derived* self() {
            return static_cast<Derived*>(this);
        }

        struct suffix_delete {
            Derived* _wrapper;
            suffix_delete(Derived* _w) :
                    _wrapper(_w) {

            }

            void operator()(_wrapped_class* p) {
                _wrapper->suffix();
            }
        };

    public:
        explicit Wrapper(_wrapped_class* p) :
                p(p) {
        }

        KECode prefix(_wrapped_class* p) {
            return KE_SUCCESS;
        }

        KECode suffix(_wrapped_class* p) {
            return KE_SUCCESS;
        }

    };

    template<template<typename > class _wrapper, typename T>
    _wrapper<T> make_wrapper(T* p) {
        return _wrapper<T>(p);
    }

} /* namespace ke */
#endif /* WRAPPER_H_ */
