/*
 * KEError.h
 *
 *  Created on: Jul 6, 2013
 *      Author: Hati
 */

#ifndef KEERROR_H_
#define KEERROR_H_

typedef unsigned int KECode;
#include "gen/ErrorCodes.h"
#include "KELogger.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>

#ifndef KE_MAX_ERROR_LENGTH
#define KE_MAX_ERROR_LENGTH 1024
#endif

#define KE_ERROR_MSG_LOOKUP(x) ___KE_GEN_ERROR_MSG_LOOKUP[x]



class __KEError {


	typedef char errMsg[KE_MAX_ERROR_LENGTH];
	static const char*& lastErrPtr() {
	    static const char* m_lastErrPtr = nullptr;
	    return m_lastErrPtr;
	}
	static char* lastError(int slot) {
	    static errMsg _lastError[2] = { "", "" };
	    return _lastError[slot];
	}

	static int& errBufferId() {
	    static int m_errBufferID = 0;
	    return m_errBufferID;
	}
	static int& lastErrorId() {
	    static int m_lastErrorID = 0;
	    return m_lastErrorID;
	}

	static char* loggerMemory() {
	    static char m_loggerMemory[1 << 8]; // 32 Byte should be more than enough
	    return m_loggerMemory;
	}


	static errMsg& lastNotify() {
	    static errMsg m_lastNotify;
	    return m_lastNotify;
	}
public:
	static KELogger*& logger() {
		    static KELogger* m_logger = nullptr;
		    return m_logger;
		}
    virtual ~__KEError() {

    }
    __KEError() {
    	if(logger()==nullptr) {
    		logger() = new (loggerMemory()) BasicLogger();
    	}
    }

    static const char* getLastError() {
        if (lastErrPtr() == nullptr)
            return "";
        return lastErrPtr();
    }

    static char* getLogMemory() {
        return loggerMemory();
    }

    KECode operator()(KECode errID, va_list args) {
        // other error than previous
        lastErrorId() = errID;

        const char* format = KE_ERROR_MSG_LOOKUP(errID);
        errBufferId() = (errBufferId() + 1) % 2;
        vsnprintf(lastError(errBufferId()), KE_MAX_ERROR_LENGTH - 1, format, args);

        const char* prevPointer = lastErrPtr();
        lastErrPtr() = lastError(errBufferId());
        if (logger() != nullptr && (prevPointer == nullptr || strcmp(lastErrPtr(), prevPointer) != 0)) {
            logger()->print(lastErrPtr());
        }
        return errID;
    }

    KECode customError(const char* format, va_list args) {
        errBufferId() = (errBufferId() + 1) % 2;
        vsnprintf(lastError(errBufferId()), KE_MAX_ERROR_LENGTH - 1, format, args);

        const char* prevPointer = lastErrPtr();
        lastErrPtr() = lastError(errBufferId());
        if (logger() != nullptr && (prevPointer == nullptr || strcmp(lastErrPtr(), prevPointer) != 0)) {
            logger()->print(lastErrPtr());
        }
        return KE_FAILURE;
    }

    KECode warning(KECode errID, va_list args) {
        // other error than previous
        const char* format = KE_ERROR_MSG_LOOKUP(errID);
        vsnprintf(lastNotify(),KE_MAX_ERROR_LENGTH-1,format,args);
        if(logger()!=nullptr) {
            logger()->print(lastNotify());
        }
        return errID;
    }

    KECode customWarning(const char* format, va_list args) {
        vsnprintf(lastNotify(),KE_MAX_ERROR_LENGTH-1,format,args);
        if(logger()!=nullptr) {
            logger()->print(lastNotify());
        }
        return KE_WARNING;
    }

    KECode notify(const char* format, va_list args) {
        vsnprintf(lastNotify(),KE_MAX_ERROR_LENGTH-1,format,args);
        if(logger()!=nullptr) {
            logger()->print(lastNotify());
        }
        return KE_SUCCESS;
    }
};

#define KE_GetError() __KEError::getLastError()

inline KECode ___KE_Error_Call(KECode errorCode, ...) {
    va_list args;
    va_start(args, errorCode);
    KECode r = __KEError()(errorCode, args);
    va_end(args);
    return r;
}

inline KECode ___KE_Error_Call(const char* format, KECode errId, ...) {
    va_list args;
    va_start(args, errId);
    __KEError().customError(format, args);
    va_end(args);
    return errId;
}

inline KECode ___KE_Warning_Call(KECode errorCode, ...) {
    va_list args;
    va_start(args, errorCode);

    KECode r = __KEError().warning(errorCode, args);
    va_end(args);
    return r;
}

inline KECode ___KE_Warning_Call(const char* format, KECode errId, ...) {
    va_list args;
    va_start(args, errId);

    __KEError().customWarning(format, args);
    va_end(args);
    return errId;
}

inline KECode ___KE_Notify_Call(const char* format, ...) {
    va_list args;
    va_start(args, format);
    KECode r = __KEError().notify(format, args);
    va_end(args);
    return r;
}

/** @file
 *** @addtogroup KEErrorSystem Error System
 *** @{
 **/
/**
 *** @def KE_Error(errorCode, ...)
 *** Error string message, uses pre-build message map
 **/
#ifdef __GNUG__
#define KE_Error(errorCode,args...) ___KE_Error_Call(errorCode,__FILE__,__LINE__,##args)
#define KE_CustomError(errorString, args...) ___KE_Error_Call("[%s:%d] " errorString, KE_FAILURE,__FILE__,__LINE__,##args)
#else
#define KE_Error(errorCode,...) ___KE_Error_Call(errorCode,__FILE__,__LINE__,__VA_ARGS__)
#define KE_CustomError(errorString, ...) ___KE_Error_Call(errorString, KE_FAILURE,__FILE__,__LINE__,__VA_ARGS__)
#endif

#ifdef __GNUG__
/**
 *** @def KE_Warning(errorCode,...)
 *** Sends a localized message, uses pre-build message map
 **/
#define KE_Warning(errorCode,args...) ___KE_Warning_Call(errorCode,__FILE__,__LINE__,##args)

#define KE_CustomWarning(errorString, args...) ___KE_Warning_Call("[%s:%d] " errorString, KE_WARNING_STR,__FILE__,__LINE__,##args)

/**
 * @def KE_WarningCond
 * Sends warning if condition is false
 */
#define KE_ConditionalWarning(condition, errorCode, args...) if(!(condition)) { ___KE_Warning_Call(errorCode, __FILE__, __LINE__, ##args); }

#else
#define KE_Warning(errorCode,...) ___KE_Warning_Call(errorCode,__FILE__,__LINE__,__VA_ARGS__)
#define KE_ConditionalWarning(condition, errorCode, ...) if(!(condition)) { ___KE_Warning_Call(errorCode, __FILE__, __LINE__, __VA_ARGS__); }
#define KE_CustomWarning(errorString, ...) ___KE_Warning_Call(errorString, KE_WARNING_STR,__FILE__,__LINE__,__VA_ARGS__)
#endif
/**
 *** @def KE_Notify(errorCode,...)
 *** Sends a non-locatable message, fully custom message
 **/
#ifdef __GNUG__
#define KE_Notify(errorCode,args...) ___KE_Notify_Call(errorCode, ##args)
#else
#define KE_Notify(errorCode,...) ___KE_Notify_Call(errorCode,__VA_ARGS__)
#endif

/**
 *** @def KE_ERROR_LOG(logger)
 *** Links the logger correctly to KE error system
 **/
#ifdef __GNUG__
#define KE_SetErrorLogger(x) __KEError::logger() = x
#else
#define KE_SetErrorLogger(x) __KEError::logger() = x
#endif
/**
 *** @def KE_ERROR_LOG_MEM
 *** Returns the memory area for the error system
 **/
#define KE_ERROR_LOG_MEM __KEError::getLogMemory()

/// @}

#endif /* KEERROR_H_ */
