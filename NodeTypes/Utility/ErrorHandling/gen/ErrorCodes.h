/*
 * ErrorCodes.h
 *
 *  Created on: Jul 6, 2013
 *      Author: Hati
 */

#ifndef ERRORCODES_H_
#define ERRORCODES_H_
#include "ErrorStrings.h"
#define KE_SUCCESS 0
#define KE_FAILURE 1
#define KE_WARNING 2
#define KE_FILE_NOT_FOUND 3
#define KE_OUT_OF_MEMORY 4
#define KE_INVALID_VALUE 5
#define KE_CANCELED 6
#define KE_NO_SUCH_VALUE 7
#define KE_SUBSYSTEM_ALREADY_INITIALIZED 8
#define KE_SUBSYSTEM_ALREADY_REGISTERED 9
#define KE_SUBSYSTEM_NOT_INITIALIZED 10
#define KE_SUBSYSTEM_NOT_INITIALIZED_NAMED 11
#define KE_UNABLE_TO_FIND 12
#define KE_UNABLE_TO_FIND_IN 13
#define KE_ALREADY_DEFINED 14
#define KE_ALREADY_DEFINED_IN 15
#define KE_UNEXPECTED_BEHAVIOR 16
#define KE_NOT_ENOUGH_MEMORY 17
#define KE_MEMORY_LEAK_STR 18
#define KE_IS_NULL 19
#define KE_FAILURE_STR 20
#define KE_FAILURE_STR_NUM 21
#define KE_FAILURE_STR_STR 22
#define KE_WARNING_STR 23
#define KE_WARNING_STR_STR 24
#define KE_ANGELSCRIPT 25
#define KE_MSG_MESSAGETYPE_STR 26
#define KE_MSG_MESSAGETYPE_STR_STR 27
#define KE_MSG_MESSAGETYPE_STR_STR_STR 28

#endif /* ERRORCODES_H_ */
