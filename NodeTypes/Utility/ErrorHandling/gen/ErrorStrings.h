#ifndef ERRORSTRINGS_H_
#define ERRORSTRINGS_H_
static const char* ___KE_GEN_ERROR_MSG_LOOKUP[] = {
"[%s:%d] ""Success",
"[%s:%d] ""Failure",
"[%s:%d] ""Warning",
"[%s:%d] ""File not found",
"[%s:%d] ""Not enough memory",
"[%s:%d] ""Invalid value",
"[%s:%d] ""Canceled",
"[%s:%d] ""No such value",
"[%s:%d] ""Subsystem is already initialized before registering",
"[%s:%d] ""Subsystem is already registered",
"[%s:%d] ""Subsystem is not initialized",
"[%s:%d] ""Subsystem %s is not initialized",
"[%s:%d] ""Unable to find %s",
"[%s:%d] ""Unable to find %s in %s",
"[%s:%d] ""%s is already defined",
"[%s:%d] ""%s is already defined in %s",
"[%s:%d] ""Unexpected behavior: %s",
"[%s:%d] ""%u memory required in %s",
"[%s:%d] ""Memory Leaked! %s",
"[%s:%d] ""%s is null",
"[%s:%d] ""Failure: %s",
"[%s:%d] ""Failure: %s %d",
"[%s:%d] ""Failure: %s %s",
"[%s:%d] ""Warning: %s",
"[%s:%d] ""Warning: %s %s",
"[%s:%d] ""[AngelScript:%s] %s (%d, %d) %s",
"[%s:%d] ""%s: %s",
"[%s:%d] ""%s: %s %s",
"%s: %s %s %s"
};
#endif /* ERRORSTRINGS_H_ */
