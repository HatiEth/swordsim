#ifndef KEASSERT_H_
#define KEASSERT_H_

#include "KEError.h"

/** @file  
 *** @addtogroup KEAssert Assertion
 *** @{
 **/
/** 
 *** 
 *** 
 *** @def KE_Assert(cond,message,...)
 *** Preprocessor flags:
 ***    NKEASSERT - to disable assertions (useful for tests)
 ***    KEASSERT_NOBREAK - just notifies without breakpoint
 *** @param cond condition for the assertion, if false the assertion is activating a breakpoint
 *** @param message message to be send to 
 *** @param ... additional parameters for message
 **/

/** 
 *** @def KE_BREAKPOINT
 *** Causes a breakpoint dependant on system and compiler 
 *** Currently win32 only - disabled in unit tests
 **/
/// @}
#define str(x) (#x)
#define xstr(x) str(x)



#if defined(NDEBUG) || defined(NKEASSERT)
	#define KE_Assert(cond,message,...) (void)nullptr
	#define KE_BREAKPOINT (void)nullptr
#else
	#ifndef KEASSERT_NOBREAK
		#ifdef _MSC_VER
			#include <intrin.h>
			#undef KE_BREAKPOINT

			#define KE_BREAKPOINT __debugbreak()
		#else
			#include <signal.h>
			#define KE_BREAKPOINT __builtin_trap()
		#endif
	#else
		#define KE_BREAKPOINT (void)nullptr
	#endif

	#ifdef __GNUG__
		#define KE_Assert(cond,message,args...) if(!(cond)) { KE_Notify("[%s:%d] Assertion '" #cond "' failed! " message,__FILE__, __LINE__,##args); KE_BREAKPOINT; }
	#else
		#define KE_Assert(cond,message,...) if(!(cond)) { KE_Notify("[%s:%d] Assertion '" #cond "' failed! " message,__FILE__, __LINE__,__VA_ARGS__); KE_BREAKPOINT; }
	#endif
#endif

#endif /* KEASSERT_H */
