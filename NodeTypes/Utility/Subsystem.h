/*
 * Subsystem.h
 *
 *  Created on: Jul 6, 2013
 *      Author: Hati
 */

#ifndef SUBSYSTEM_H_
#define SUBSYSTEM_H_

#include "Wrapper.h"
#include "Hashing/Hashing.h" 

class Subsystem;

/**	@file 
 *** @addtogroup Subsystem
 *** @{
 **/
/**	
 *** @def KE_GetSubsystemID(_system)
 *** Returns the proper ID of the Subsystem class
 **/
#define KE_GetSubsystemID(_system) _system::Subsystem_ID()
/**	
 *** @def KE_DECL_SYSTEM
 *** Declares Subsystem_ID in a subsystem
 *** Note to declare under public
 **/
//#define KE_DECL_SYSTEM static const char* Subsystem_ID
#define KE_DECL_SYSTEM(id) static const char*& Subsystem_ID() { static const char* _id = id; return _id; }

/**	
 *** @def KE_DEF_SYSTEM(_sys,id)
 *** Defines the Subsystem_ID with an internal id
 **/
#define KE_DEF_SYSTEM(_sys,id) const char* KE_GetSubsystemID(_sys) = id;

/**	@brief Wrapper class to allow easier debugging of Subsystems using the KE_Wrappers
 ***	
 **/
template<typename T>
class InitCheckWrapper: eth::Wrapper<T, InitCheckWrapper<T>> {
public:
    InitCheckWrapper(T* p) :
            eth::Wrapper<T, InitCheckWrapper<T>>(p) {
    }
    KECode prefix(T* p) {
        KE_Assert(p != nullptr, "Wrapped item equals nullptr!");
        if (!(p->isInitialized())) {
            KE_Assert(p->isInitialized(), "Subsystem not initialized! Memory %d", p);

        }
        return KE_SUCCESS;
    }

};

class Subsystem {
protected:
    friend class InitCheckWrapper<Subsystem> ;
    typedef eth::StringMap<Subsystem*>::Type AttachedSubsystems;

    Subsystem* m_parent;
    AttachedSubsystems m_subsystems;

    bool m_initialized;
public:
    KE_DECL_SYSTEM("nullsystem");
    Subsystem(Subsystem* parent = nullptr) :
            m_parent(parent), m_initialized(false) {
    }
    virtual ~Subsystem() {}

    template<class _system>
    _system* get(void) {
        AttachedSubsystems::iterator it;
        if ((it = m_subsystems.find(KE_GetSubsystemID(_system))) != m_subsystems.end()) {
            return dynamic_cast<_system*>(it->second);
            
        }
        return nullptr;
    }

    template<class _sys>
    InitCheckWrapper<_sys> get_s(void) {
        AttachedSubsystems::iterator sys;
        if ((sys = m_subsystems.find(KE_GetSubsystemID(_sys))) != m_subsystems.end() ) {
            return eth::make_wrapper<InitCheckWrapper>((_sys*)(sys->second));
        }
        return nullptr;
    }

    Subsystem* get(const char* name) {
        AttachedSubsystems::iterator sys;
        if ((sys = m_subsystems.find(name)) != m_subsystems.end()) {
            return sys->second;
        }
        return nullptr;
    }

    template<class _cast_to>
    _cast_to* get(const char* name) {
        AttachedSubsystems::iterator sys;
        if ((sys = m_subsystems.find(name)) != m_subsystems.end()) {
            return (_cast_to*) sys->second;
        }
        return nullptr;
    }

    template<class _system>
    _system* sibling(void) {
        if (m_parent != nullptr)
            return m_parent->get<_system>();
        return nullptr;
    }

    template<class _cast_to>
    _cast_to* sibling(const char* name) {
        if (m_parent != nullptr)
            return m_parent->get<_cast_to>(name);
        return nullptr;
    }

    template<class _sys>
    InitCheckWrapper<_sys> sibling_s(void) {
        if (m_parent != nullptr)
            return m_parent->get_s<_sys>();
        return nullptr;
    }

    KECode init(void) {
        return KE_SUCCESS;
    }

    KECode shutdown(void) {
        return KE_SUCCESS;
    }

    template<class _sys>
    KECode registerSubsystem(_sys* system) {
        if (system->m_initialized) {
            return KE_CustomError("Subsystem %s is already registered in %s!", KE_GetSubsystemID(_sys), this->Subsystem_ID());

        }
        if (system->init() == KE_SUCCESS) {
            system->m_initialized = true;
        }

        KE_Assert(system->m_initialized, "Subsystem %s not initialized in %s!", KE_GetSubsystemID(_sys), this->Subsystem_ID);
        if (this->get(KE_GetSubsystemID(_sys)) != nullptr) {
        	//FIXME: why cannot used template get<_sys> here?
            return KE_CustomError("Subsystem %s already registered in %s!", KE_GetSubsystemID(_sys), this->Subsystem_ID);
        }
        m_subsystems[KE_GetSubsystemID(_sys)] = system;
        system->m_parent = this;
        return KE_SUCCESS;
    }

};

#if defined(_DEBUG) || !defined(NDEBUG)
#define KE_SUBSYSTEM_CALL(_sys) (make_wrapper<InitCheckWrapper>(_sys))
#define KE_SUBSYSTEM_GET(target, children, sysType) InitCheckWrapper<sysType> var = KE_SUBSYSTEM_CALL(target)->get_s<sysType>()
#define KE_SUBSYSTEM_SIBLING(target, var, sysType) InitCheckWrapper<sysType> var = KE_SUBSYSTEM_CALL(target)->sibling_s<sysType>()
#else // Release
#define KE_SUBSYSTEM_CALL(_sys) (_sys)
#define KE_SUBSYSTEM_GET(target, var, sysType) sysType* var = KE_SUBSYSTEM_CALL(target)->get<sysType>()
#define KE_SUBSYSTEM_SIBLING(target, var, sysType) sysType* var = KE_SUBSYSTEM_CALL(target)->sibling<_sys>()
#endif // _DEBUG
#endif /* SUBSYSTEM_H_ */
