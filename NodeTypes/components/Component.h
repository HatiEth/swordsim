/*
 * Component.h
 *
 *  Created on: Nov 1, 2013
 *      Author: Hati
 */

#ifndef COMPONENT_H_
#define COMPONENT_H_
#include <memory.h>
#include <type_traits>
#include <Utility.h>

/**
 * POD Component structure
 */
struct Component {
};

template<typename C>
static inline void initComponent(C* comp) {
	memset(comp, 0, sizeof(C));
}

/**
 * @todo move constructor ? instead of new allocations
 */
struct ComponentFactory {
	typedef Component* (*CreatorFunc)(); ///< function pointer to default pod constructor
	typedef eth::StringMap<CreatorFunc>::Type CreatorMap; ///< hash map of name:constructor types

	/**
	 * Creates a new component through the factory by the given alias
	 * @param compAlias alias of the component to create
	 * @return the component created
	 */
	static Component* create(const char* compAlias) {
		KE_Assert(creators().find(compAlias) != creators().end(),
				"No Default template found for component %s!", compAlias);

		return creators()[compAlias]();
	}

	/**
	 * static access to all available creator functions
	 * @return
	 */
	static CreatorMap& creators() {
		static CreatorMap _creators;
		return _creators;
	}

	/**
	 * Meta-template for creating components
	 */
	template<typename C>
	struct ComponentCreator {
		/// compile time check if base type is Component
		static_assert(std::tr1::is_base_of<Component, C>::value, "Trying to register a non component to component factory!");
		/// default initialization of all components
		static Component* create() {
			C* c = new C();
			initComponent(c);
			return c;
		}
		/// register a specific alias for type C in Factory
		static CreatorFunc registerCreator(const char* creationAlias) {
			if (creators().find(creationAlias) != creators().end()) {
				KE_Notify("Overwriting creationAlias %s with %s",
						creators().find(creationAlias)->first, creationAlias);
			} else {
				KE_Notify("Registering creation alias %s", creationAlias);
			}

			return creators()[creationAlias] = create;
		}
		/// dummy to call templated register
		static CreatorFunc _internalCreator;
	};
};

/// Used to actually register a component with the factory
#define REGISTER_COMPONENT(ComponentType, ComponentAlias) template<> ComponentFactory::CreatorFunc ComponentFactory::ComponentCreator<ComponentType>::_internalCreator = ComponentFactory::ComponentCreator<ComponentType>::registerCreator(ComponentAlias)



//template<typename ComponentType>
//void func(const char* ComponentAlias) {
//	ComponentFactory::ComponentCreator<ComponentType>::_internalCreator = ComponentFactory::ComponentCreator<ComponentType>::registerCreator(ComponentAlias);
//}

#endif /* COMPONENT_H_ */
