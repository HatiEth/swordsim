/*
 * HealthComponent.h
 *
 *  Created on: 03.12.2013
 *      Author: Hati
 */

#ifndef HEALTHCOMPONENT_H_
#define HEALTHCOMPONENT_H_

#include "Component.h"

struct HealthComponent : public Component {
	int _value;
	int _maxValue;
};

REGISTER_COMPONENT(HealthComponent, "Health");

#endif /* HEALTHCOMPONENT_H_ */
