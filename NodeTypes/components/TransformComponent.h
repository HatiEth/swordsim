/*
 * PositionComponent.h
 *
 *  Created on: Dec 2, 2013
 *      Author: Hati
 */

#ifndef POSITIONCOMPONENT_H_
#define POSITIONCOMPONENT_H_
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "Component.h"

/**
 * @todo may rename to transform if more information is required
 */

struct TransformComponent : public Component {
	glm::fvec2 position;
	float rotation;
	glm::fvec2 scaling;
};

REGISTER_COMPONENT(TransformComponent, "Transform");


#endif /* POSITIONCOMPONENT_H_ */
