#version 330 compatibility

uniform sampler2D cutTexture; //external input 
uniform sampler2D prevTexture; //texture from previous time step

void main() {
	vec2 dy = vec2(0, 1.0f/600.0f);
	vec2 d = vec2(1.0f/800.0f, 1.0f/600.0f);


	vec4 finalColor = vec4(0,0,0,0);

	vec4 prevData = texture2D(cutTexture, gl_TexCoord[0].st).rgba;
	vec4 smearThere = texture2D(prevTexture,  clamp(gl_TexCoord[0].st + dy, 0.0f, 1.0f-dy.y)).rbga;


	gl_FragData[0] = smearThere + prevData;
}