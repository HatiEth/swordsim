#version 330 compatibility

uniform sampler2D uCutTexture;

void main() {
	vec4 color = texture2D(uCutTexture, gl_TexCoord[0].st);
	gl_FragData[0] = color;
}