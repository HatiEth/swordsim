#version 330 compatibility

uniform sampler2D cutSampler;
uniform sampler2D smearSampler;

void main() {
	vec4 cT = texture2D(cutSampler, gl_TexCoord[0].st);
	vec4 sT = texture2D(smearSampler, gl_TexCoord[0].st);

	//gl_FragColor = vec4(cT.r,sT.g - cT.r,cT.r,1);
	gl_FragColor = vec4(cT.r, cT.g, sT.r+sT.g+sT.b, 1); // for debug purposes
}