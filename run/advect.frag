#version 330 compatibility

uniform vec2 mousePosition;

uniform sampler2D particles;
uniform sampler2D vectorField;
uniform sampler2D cutSampler;

// tex : texture to apply kernel to
vec4 gaussKernel(sampler2D tex, vec2 texCoord, vec2 texDim) {
	vec4 kernelResult;
	vec2 dx = vec2(1.0f/texDim.x, 0);
	vec2 dy = vec2(0.0f, 1.0f/texDim.y);
	
	kernelResult = texture2D(tex, texCoord);
	kernelResult += texture2D(tex, texCoord+dx);
	kernelResult += texture2D(tex, texCoord-dx);
	kernelResult += texture2D(tex, texCoord+dy);
	kernelResult += texture2D(tex, texCoord-dy);

	kernelResult.rgb *= 0.2f;

	return kernelResult;
}

void main() {
	float splatterFactor = 2.34561f;
	vec2 scale = vec2(splatterFactor/800.0f, splatterFactor/600.0f);

	//vec2 mdir = step(vec2(400,300),mousePosition);
	//mdir = (mdir - vec2(0.5f, 0.5f)) * 2.0f;
	vec2 mdir = mousePosition - vec2(400,300);
	mdir *= vec2(1.0f/100.0f, 1.0f/100.0f);
	mdir = clamp(mdir, vec2(-1,-1), vec2(1,1));
	mdir.x *= -1;
	

	vec4 newParticle = texture2D(cutSampler, gl_TexCoord[0].st);

	//vec4 newParticle = gaussKernel(particles, gl_TexCoord[0].st, vec2(800,600));
	vec4 vector = texture2D(vectorField, gl_TexCoord[0].st);
	//vec4 vector = gaussKernel(vectorField, gl_TexCoord[0].st, vec2(800, 600));
	vec4 prevStep = texture2D(particles, gl_TexCoord[0].st);
	vec4 oldParticle = texture2D(particles, gl_TexCoord[0].st + (mdir * vector.xy * scale));
	//vec4 oldParticle = gaussKernel(particles, gl_TexCoord[0].st + (mdir * vector.xy * scale), vec2(800, 600));

	gl_FragData[0] = oldParticle+newParticle+prevStep;//+vec4(0,0,td,1.0f);
}