#version 330 compatibility


uniform mat4 uCamera;
uniform float uTransforms[5];
uniform sampler2D uSpriteSampler;
uniform vec2 uViewport;

in vec2 vTexCoord; // texture Coordinates by vertex shader

void main() {
	vec2 texDim = textureSize(uSpriteSampler, 0);
	vec2 texFix = vec2(0.5f/texDim.x, 0.5f/texDim.y);
	vec2 texCoordCorrection = vec2(vTexCoord.s + texFix.x, vTexCoord.t + texFix.y);
	vec4 texColor = texture2D(uSpriteSampler, texCoordCorrection);

	gl_FragColor = texColor;
}