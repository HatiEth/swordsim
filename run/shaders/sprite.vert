#version 330 compatibility

uniform mat4 uCamera;
//uniform vec2 uViewport;


// 0   , 1   , 2        , 3     , 4
// posx, posy, rotationZ, scaleX, scaleY
uniform float uTransforms[5]; 


out vec2 vTexCoord;

#define M_PI 3.1415926535897932384626433832795



void main() {
	float rotInRad = (uTransforms[2] * M_PI) / 180.0f; // left rotation = positive
	//float rotInRad = uTransforms[2];
	//vec2 fixedPos = vec2(uTransforms[0], uTransforms[1]);
	//vec2 pixelCorrection = vec2(2* 1.0f/uViewport.x, 2* 1.0f/uViewport.y);
	//fixedPos = (2.0f * fixedPos - 1) * pixelCorrection;


	mat4 modelMat = mat4(
		uTransforms[3] * cos(rotInRad), uTransforms[3] * sin(rotInRad), 0, 0,
		uTransforms[4]*-sin(rotInRad), uTransforms[4] * cos(rotInRad), 0, 0,
		0, 0, 1, 0,
		uTransforms[0], uTransforms[1], 0, 1
	);

/*
	mat4 modelMat = mat4(
		uTransforms[3] * cos(rotInRad), sin(rotInRad), 0, 0,
		-sin(rotInRad), uTransforms[4] * cos(rotInRad), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
*/
/*
	mat4 scaleMat = mat4(0);
	scaleMat[0][0] = uTransforms[3]; scaleMat[1][1] = uTransforms[4]; scaleMat[2][2] = 1.0f; scaleMat[3][3] = 1.0f;

	mat4 modelMat;
	modelMat[0].x    = cos(rotInRad);
	modelMat[0].y    = -sin(rotInRad);
	modelMat[0].z    = 0.0;
	modelMat[0].w    = 0.0;
	modelMat[1].x    = sin(rotInRad);
	modelMat[1].y    = cos(rotInRad);
	modelMat[1].z    = 0.0;
	modelMat[1].w    = 0.0;
	modelMat[2].x    = 0.0;
	modelMat[2].y    = 0.0;
	modelMat[2].z    = 1.0;
	modelMat[2].w    = 0.0;
	modelMat[3].x    = 0.0;
	modelMat[3].y    = 0.0;
	modelMat[3].z    = 0.0;
	modelMat[3].w    = 1.0;
	gl_Position = uCamera * scaleMat * modelMat * gl_Vertex;
*/
/*	*/
	gl_Position = uCamera * modelMat * gl_Vertex;
	// align vertex to pixel grid
	


	vTexCoord = gl_MultiTexCoord0.st;
}