#version 330 compatibility


uniform sampler2D uRainAdvect;
uniform sampler2D uEmitter; // noise map - handle like emitter


float fgauss(sampler2D tex, vec2 texCoord, vec2 texDim, int valueIdx) {
	float dx = 1.0f/texDim.x;
	float dy = 1.0f/texDim.y;
	vec2 dp[9] = vec2[](
		vec2(-dx,-dy), vec2(0, -dy), vec2(dx, -dy),
		vec2(-dx,0), vec2(0, 0), vec2(dx, 0),
		vec2(-dx,+dy), vec2(0, +dy), vec2(dx, +dy)
	);
	const float k = 16.0f;
	const float kernel[9] = float[](
		1.0f/k, 2.0f/k, 1.0f/k,
		2.0f/k, 4.0f/k, 2.0f/k,
		1.0f/k, 2.0f/k, 1.0f/k
	);
	float kernelResult;
	for(int i=0;i<9;++i) {
		kernelResult += texture2D(tex, texCoord + dp[i])[valueIdx] * kernel[i];
	}
	
	return kernelResult;
}

/* RainInfo
	r = raindrop
	g = 
	b = emitter, UNREADY
	a = lifetime
*/

void main() {
	float splatterFactor = 8.34561f; //2.34561f;
	vec2 scaledStep = vec2(1/800.0f, splatterFactor/600.0f);
	vec4 rawNoise = texture2D(uEmitter, gl_TexCoord[0].st);
	float firstLineMarker = 1- step(scaledStep.y, 1-gl_TexCoord[0].t); // If (1-texCoord) > scaledStep => 1; 0, otherwise ;; take first row

	/* Emitter part */
	vec4 emitterInfo = texture2D(uEmitter, gl_TexCoord[0].st); // Getinfo from emitter 
	float rainDroplet = 0; 
	float emitterReadiness = 0;

	emitterInfo.b = emitterInfo.b * firstLineMarker; // choose first line only, and factor the value
	float prevReadiness = texture2D(uRainAdvect, gl_TexCoord[0].st).b * firstLineMarker;
	emitterReadiness = (emitterInfo.b + prevReadiness); // addiere previous readyness und neue

	/* Emitting abfrage;; */
	float emitDrop = 0;
	/*
	float lowerEmitEdge = step(0.5f, emitterReadiness); //emitterReadiness > 0.5 => 1 ; 0 otherwise
	float upperEmitEdge = step(emitterReadiness, 1.0f); //emitterReadiness > 1.0 => 1 ; 0 otherwise;

	float emitDrop = lowerEmitEdge - upperEmitEdge; //erstelle drops zwischen 0.5 und 1.0
	float resetEmitter = 1 - (upperEmitEdge*lowerEmitEdge); //wenn lowerEmitEdge>0.5(1) && wenn upperEmitEdge>1(1)
	emitterReadiness = resetEmitter * firstLineMarker; // erste zeile: 0
	*/
	if(emitterReadiness>0.73f && emitterReadiness<1.0f) {
		emitDrop = 1;
	}
	if(emitterReadiness>1.0f) {
		emitterReadiness = 0.0f;
	}
	vec2 fetch = gl_TexCoord[0].st + scaledStep * vec2(-1,0.51);
	
	float currentDrop = texture2D(uRainAdvect, fetch).r; 

	gl_FragData[0] = vec4( emitDrop + currentDrop, 0, emitterReadiness, 1);
}