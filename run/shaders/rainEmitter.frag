#version 330 compatibility


uniform sampler2D uNoise;
uniform vec2 uShift;

void main() {
	float splatterFactor = 1.0f; //2.34561f;
	vec2 scaledStep = vec2(splatterFactor/800.0f, splatterFactor/600.0f);
	vec4 emitterInfo = texture2D(uEmitter, gl_TexCoord[0].st); 

	vec4 prevRaindroplet = texture2D(uRainAdvect, gl_TexCoord[0].st - scaledStep);


	gl_FragData[0] = newRaindroplet + emitterInfo;
}