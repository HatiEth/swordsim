#version 330 compatibility

uniform sampler2D uTexture;

vec3 rgaussKernel(sampler2D tex, vec2 texCoord, vec2 texDim) {
	float dx = 1.0f/texDim.x;
	float dy = 1.0f/texDim.y;
	vec2 dp[9] = vec2[](
		vec2(-dx,-dy), vec2(0, -dy), vec2(dx, -dy),
		vec2(-dx,0), vec2(0, 0), vec2(dx, 0),
		vec2(-dx,+dy), vec2(0, +dy), vec2(dx, +dy)
	);
	const float k = 16.0f;
	const float kernel[9] = float[](
		1.0f/k, 2.0f/k, 1.0f/k,
		2.0f/k, 4.0f/k, 2.0f/k,
		1.0f/k, 2.0f/k, 1.0f/k
	);

	vec3 kernelResult;
	for(int i=0;i<9;++i) {
		kernelResult += texture2D(tex, texCoord + dp[i]).rgb * kernel[i];
	}
	
/*	
	kernelResult = texture2D(tex, texCoord);
	kernelResult += texture2D(tex, texCoord+dx);
	kernelResult += texture2D(tex, texCoord-dx);
	kernelResult += texture2D(tex, texCoord+dy);
	kernelResult += texture2D(tex, texCoord-dy);

	kernelResult.rgb *= 0.2f;
*/
	return kernelResult;
}

vec4 gaussKernel(sampler2D tex, vec2 texCoord, vec2 texDim) {
	vec4 kernelResult;
	vec2 dx = vec2(1.0f/texDim.x, 0);
	vec2 dy = vec2(0.0f, 1.0f/texDim.y);
	
	kernelResult = texture2D(tex, texCoord);
	kernelResult += texture2D(tex, texCoord+dx);
	kernelResult += texture2D(tex, texCoord-dx);
	kernelResult += texture2D(tex, texCoord+dy);
	kernelResult += texture2D(tex, texCoord-dy);

	kernelResult.rgb *= 0.2f;

	return kernelResult;
}

vec4 gaussKerneli(sampler2D tex, vec2 texCoord, vec2 texDim, int steps) {
	vec4 kernelResult;
	vec2 dx = vec2(1.0f/texDim.x, 0);
	vec2 dy = vec2(0.0f, 1.0f/texDim.y);
	
	kernelResult = texture2D(tex, texCoord);
	for(int i=0;i<steps;++i) {
		kernelResult += texture2D(tex, texCoord+dx*i);
		kernelResult += texture2D(tex, texCoord-dx*i);
		kernelResult += texture2D(tex, texCoord+dy*i);
		kernelResult += texture2D(tex, texCoord-dy*i);
	}

	kernelResult.rgb *= 1.0f/(1+steps*4);

	return kernelResult;
}

void main() {	
	//vec4 color = texture2D(uTexture, gl_TexCoord[0].st);
	vec3 color = rgaussKernel(uTexture, gl_TexCoord[0].st, vec2(800, 600));
	gl_FragColor = vec4(color.r+color.g+color.b, 0, 0, 1);
}