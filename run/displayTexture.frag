#version 330 compatibility

uniform sampler2D uTexture;

vec3 vec3GaussKernel(sampler2D tex, vec2 texCoord, vec2 texDim) {
	float dx = 1.0f/texDim.x;
	float dy = 1.0f/texDim.y;
	vec2 dp[9] = vec2[](
		vec2(-dx,-dy), vec2(0, -dy), vec2(dx, -dy),
		vec2(-dx,0), vec2(0, 0), vec2(dx, 0),
		vec2(-dx,+dy), vec2(0, +dy), vec2(dx, +dy)
	);
	const float k = 16.0f;
	const float kernel[9] = float[](
		1.0f/k, 1.0f/k, 2.0f/k,
		1.0f/k, 4.0f/k, 2.0f/k,
		2.0f/k, 2.0f/k, 1.0f/k
	);

	vec3 kernelResult;
	for(int i=0;i<9;++i) {
		kernelResult += texture2D(tex, texCoord + dp[i]).rgb * kernel[i];
	}
	
	return kernelResult;
}

void main() {	
	//vec4 color = texture2D(uTexture, gl_TexCoord[0].st);
	vec2 texDim = textureSize(uTexture, 0);
	vec3 color = vec3GaussKernel(uTexture, gl_TexCoord[0].st, texDim);
	float alpha = texture2D(uTexture, gl_TexCoord[0].st).a;
	gl_FragColor = vec4(color.rrr, alpha);
}