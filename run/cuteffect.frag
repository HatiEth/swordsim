#version 330 compatibility

uniform sampler2D uCutTexture;
uniform sampler2D uPrevStep;
uniform sampler2D uNoise;

#define M_PI 3.1415926535897932384626433832795

vec3 vec3GaussKernel(sampler2D tex, vec2 texCoord, vec2 texDim) {
	float dx = 1.0f/texDim.x;
	float dy = 1.0f/texDim.y;
	vec2 dp[9] = vec2[](
		vec2(-dx,-dy), vec2(0, -dy), vec2(dx, -dy),
		vec2(-dx,0), vec2(0, 0), vec2(dx, 0),
		vec2(-dx,+dy), vec2(0, +dy), vec2(dx, +dy)
	);
	const float k = 16.0f;
	const float kernel[9] = float[](
		1.0f/k, 2.0f/k, 1.0f/k,
		2.0f/k, 4.0f/k, 2.0f/k,
		1.0f/k, 2.0f/k, 1.0f/k
	);

	vec3 kernelResult;
	for(int i=0;i<9;++i) {
		kernelResult += texture2D(tex, texCoord + dp[i]).rgb * kernel[i];
	}
	
	return kernelResult;
}

float fgauss(sampler2D tex, vec2 texCoord, vec2 texDim, int valueIdx) {
	float dx = 1.0f/texDim.x;
	float dy = 1.0f/texDim.y;
	vec2 dp[9] = vec2[](
		vec2(-dx,-dy), vec2(0, -dy), vec2(dx, -dy),
		vec2(-dx,0), vec2(0, 0), vec2(dx, 0),
		vec2(-dx,+dy), vec2(0, +dy), vec2(dx, +dy)
	);
	const float k = 16.0f;
	const float kernel[9] = float[](
		1.0f/k, 2.0f/k, 1.0f/k,
		2.0f/k, 4.0f/k, 2.0f/k,
		1.0f/k, 2.0f/k, 1.0f/k
	);
	float kernelResult;
	for(int i=0;i<9;++i) {
		kernelResult += texture2D(tex, texCoord + dp[i])[valueIdx] * kernel[i];
	}
	
	return kernelResult;
}

void main() {
	float splatterFactor = 3.34561f;
	vec2 scale = vec2(splatterFactor/800.0f, splatterFactor/600.0f);

	vec2 resolution = vec2(800.0f, 600.0f);
	vec2 resScale = (1.0f/resolution.x, 1.0f/resolution.y);
	vec4 cutValue = texture2D(uCutTexture, gl_TexCoord[0].st);
	
	vec4 noise = texture2D(uNoise, gl_TexCoord[0].st);
	
	vec4 controlValue = texture2D(uPrevStep,
	  gl_TexCoord[0].st);

	vec4 prevValue = texture2D(uPrevStep,
	  gl_TexCoord[0].st); // fix flickering, using old value + prevValue
	
	float yOffset = (1-(controlValue.x - (cutValue.x+0.5)))*noise.y;
	vec4 iteration = texture2D(uPrevStep,
	  clamp(gl_TexCoord[0].st - (vec2(noise.x, yOffset) * vec2(scale.x, 1/600.0f)),0 ,1));

	vec4 stepValue = iteration + prevValue;

	gl_FragData[0] = cutValue + stepValue;
}