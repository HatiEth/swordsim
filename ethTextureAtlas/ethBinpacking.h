/*
 * ethBinpacking.h
 *
 *  Created on: Feb 3, 2014
 *      Author: Hati
 */

#ifndef ETHBINPACKING_H_
#define ETHBINPACKING_H_

#include "ethTextureList.h"

#include "ethTextureAtlas.h"

namespace eth {

/**
 * 2D space where AABBs are place for textures from texture dictionary to get best space usage
 */
struct AABB {
	float _width, _height;
	float _x{0}, _y{0};
	bool _heightRestriction{false}, _widthRestriction{false};
	int _age{0};

	float area() const {
		return _width*_width + _height*_height;
	}
};
void drawAABB(AABB& aabb, const glm::vec4& color = glm::vec4(1, 0, 0, 1));

struct Package {
	AABB _aabb;
	Texture _texture;
};

struct PackedBin {
	std::list<Package> _packages;
	AABB packageAABB;
	void placeTextureAt(Texture& tex, AABB& aabb);
};

struct Binpacking {
	std::list<AABB> _aabbList;

	AABB packageAABB;

	std::list<AABB>::iterator findProperAABB(Texture& tex);
	PackedBin* binpack(TextureList& texList);

	enum Status {
		RequiresPrepare,
		Prepared,
		Done
	} _status {RequiresPrepare};

	PackedBin* _packedBin;

	std::list<eth::Texture>::iterator stepIt;

	TextureList* _texList;
	void prepare(TextureList* texList);
	bool step();

	void drawAABBList();
};



void generateAtlas(PackedBin& pkgbin, TextureAtlas& _outAtlas);
void drawPackedBin(PackedBin& pkgbin);

} /* namespace eth */
#endif /* ETHBINPACKING_H_ */
