/*
 * File.h
 *
 *  Created on: Jul 7, 2013
 *      Author: Hati
 */

#ifndef FILE_H_
#define FILE_H_
#include <fstream>
#include "../ErrorHandling/ErrorHandling.h"
#include <Memory.h>

namespace eth {
    struct File {
        const char* path;
        char* content;
        size_t length;
    };

    inline KECode readFile(const char* filePath, File& f) {
        std::fstream fs(filePath, std::ios_base::in | std::ios_base::binary);
        if (!fs) {
            return KE_Error(KE_FILE_NOT_FOUND, filePath);
        }
        size_t pathLength = strlen(filePath);
        f.path = new char[pathLength+1];
        memcpy((void*)f.path, filePath,pathLength);

        fs.seekg(0, fs.end);
        f.length = (size_t) fs.tellg();
        fs.seekg(0, fs.beg);

        f.content = new char[f.length + 1];
        f.content[f.length] = '\0';

        fs.read(f.content, f.length);

        fs.close();

        return KE_SUCCESS;
    }

    /// Note that buffer requires size of filePath + filesContent
    inline KECode readFile(const char* filePath, File& f, char* buffer, size_t bufferLength) {
        std::fstream fs(filePath, std::ios_base::in | std::ios_base::binary);
        if (!fs) {
            return KE_Error(KE_FILE_NOT_FOUND, filePath);
        }
        LinearAllocator linear(buffer,buffer+bufferLength);
        
        size_t pathLength = strlen(filePath);
        f.path = new (linear.allocate(pathLength+1)) char[pathLength+1];
        memcpy((void*)f.path, filePath,pathLength);
        
        fs.seekg(0, fs.end);
        f.length = (size_t) fs.tellg();
        KE_Assert(f.length+pathLength < bufferLength, "Buffer size exceeded! Note that buffer requires size of filePath + filesContent"); 

        fs.seekg(0, fs.beg);

        f.content = new (linear.allocate(f.length+1)) char[f.length+1];
        f.content[f.length] = '\0';

        fs.read(f.content, f.length);

        fs.close();

        return KE_SUCCESS;
    }

}

#endif /* FILE_H_ */
