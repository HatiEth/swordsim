/*
 * CompileTimeFunctions.h
 *
 *  Created on: Nov 3, 2013
 *      Author: Hati
 */

#ifndef COMPILETIMEFUNCTIONS_H_
#define COMPILETIMEFUNCTIONS_H_


/**
 * Resolves to 2^n at compile-time
 * @tparam num exponent
 */
template<size_t num>
struct compiletime2PowNum {
	/**
	 * Actual value of 2^num
	 */
	enum {
		value = 2*compiletime2PowNum<num-1>::value
	};
};

template<>
struct compiletime2PowNum<0> {
	enum {
		value = 1
	};
};



#endif /* COMPILETIMEFUNCTIONS_H_ */
