#include "KELogger.h"

KELogger& operator<<(KELogger& log, const char* str) {
    log.write(str);
    return log;
}
