/*
 * main.cpp
 *
 *  Created on: Feb 2, 2014
 *      Author: Hati
 */

/**
 * Tool to generate texture atlases
 */

/**
 *
 */
#include "ethBinpacking.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <Utility.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

GLFWwindow* g_window;
bool showContextWindow { false };

void error_callback(int errCode, const char* errMsg) {
	KE_CustomError("Glfw Error(%d): %s", errCode, errMsg);
}

void resize_callback(GLFWwindow* win, int w, int h) {
	glViewport(0, 0, w, h);
	glm::mat4 orthoMat = glm::ortho<float>(0, w, h, 0, -1, 1000);

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(orthoMat));
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
}

KECode initializeGL() {
	if (!glfwInit())
		return KE_Error(KE_FAILURE_STR, "Unable to initialize glfw!");

	KE_Notify("Initialized glfw%s!", glfwGetVersionString());

	glfwSetErrorCallback(error_callback);
//	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_VISIBLE, showContextWindow ? GL_TRUE : GL_FALSE);
	g_window = glfwCreateWindow(800, 600, "ethTextureAtlas Generator View",
	NULL, NULL);

	KE_Notify("Using OpenGL%d.%d",
			glfwGetWindowAttrib(g_window, GLFW_CONTEXT_VERSION_MAJOR),
			glfwGetWindowAttrib(g_window, GLFW_CONTEXT_VERSION_MINOR));

	if (!g_window) {
		glfwTerminate();
		return KE_CustomError("Failure while creating opengl context! Internal glfw error");
	}

	glfwSetFramebufferSizeCallback(g_window, resize_callback);

	glfwMakeContextCurrent(g_window);

	glewExperimental = true;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		return KE_CustomError("glew initialization failed: %s", glewGetErrorString(err));
	}
	KE_Notify("Initialized glew!");


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	return KE_SUCCESS;
}



int main(int argc, char** argv) {
	KE_Notify("Starting ethTextureAtlas generation with arguments: ");
	std::list<std::string> fileList;

	eth::TextureList textureList;
	std::string tafFilePath;


	for (int i = 0; i < argc; ++i) {
		KE_Notify("    arg[%d]: %s", i, argv[i]);
		if (strcmp(argv[i], "--visible")==0) {
			showContextWindow = true;
		}
		if (strstr(argv[i], ".png")!=NULL) { // add files here
			fileList.push_back(argv[i]);
			KE_Notify("Adding file to atlas: %s", argv[i]);
		}
	}

	if (initializeGL() != KE_SUCCESS) {
		return KE_CustomError("Failed GL initialization %s", KE_GetError());
	}

	// read texture atlas generation - file
	// bin-pack texture atlas
	// write finished atlas to disk
	// render while bin-packing to show progress
	auto it = fileList.begin();
	for(;it!=fileList.end();++it) {
		eth::Texture tex;
		KE_Notify("Creating texture for file: %s", it->c_str());
		eth::loadTextureFromPNG(it->c_str(), tex);
		textureList.addTexture(tex);
	}
	textureList.sort();

	glm::mat4 orthoMat = glm::ortho<float>(0, 800, 600, 0, -1, 1000);

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(orthoMat));
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	eth::Binpacking bin;
//	eth::PackedBin* package = bin.binpack(textureList);
	glViewport(0, 0, 800, 600);
	auto texIt = textureList._textures.begin();


	FloatTimer timer = FloatTimer(1 / 60.0f);

	glfwSetTime(0.0);

	bin.prepare(&textureList);
	eth::PackedBin* package = bin._packedBin;

	eth::TextureAtlas texAtlas;

	bool runBinpacker=true;

	while(!glfwWindowShouldClose(g_window)) {
		glfwPollEvents();
		glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);

		if(showContextWindow) {

			timer = (float) glfwGetTime();

			timer += timer.elapsedTime;
			while (timer.timeAccumulator > timer.updateFrequency) {
				timer -= timer.updateFrequency;
				if(runBinpacker && !bin.step()) {
					runBinpacker = false;
					eth::generateAtlas(*package, texAtlas);
					eth::saveTextureAtlas(texAtlas, "default");
				}
			}
//			if(texIt!=textureList._textures.end()) {
//				eth::drawTextureAt(*texIt, glm::vec2(400,300));
////				++texIt;
//			}
//			else {
//				texIt = textureList._textures.begin();
//			}
			if(runBinpacker) {
				bin.drawAABBList();
				eth::drawPackedBin(*package);
			}
			else {

				eth::drawAABB(package->packageAABB, glm::vec4(0, 1, 0, 1));
				eth::drawTextureAt(texAtlas._tex, glm::vec2(0,0));
			}

		}

		glfwSwapBuffers(g_window);
	}

	return 0;
}

