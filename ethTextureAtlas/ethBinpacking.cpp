/*
 * ethBinpacking.cpp
 *
 *  Created on: Feb 3, 2014
 *      Author: Hati
 */

#include "ethBinpacking.h"
#include <algorithm>
#include <glm/ext.hpp>

bool compareAABBArea(const eth::AABB& aabb1, const eth::AABB& aabb2) {
	const float area1 = (aabb1._height * aabb1._height)
			+ (aabb1._width * aabb1._width);
	const float area2 = (aabb2._height * aabb2._height)
			+ (aabb2._width * aabb2._width);

	return (area1 < area2); // oldest first; if age equal - smallest first
}

void removeRedundantAABB(std::list<eth::AABB>& aabbList) {
	auto it = aabbList.begin();
	size_t startCount = aabbList.size();

	for (; it != aabbList.end(); ++it) {
		eth::AABB& possibleSmallest = *it;
		auto remIt =
				std::remove_if(aabbList.begin(), aabbList.end(),
						[possibleSmallest](eth::AABB& a) {
							if(a._x == possibleSmallest._x && a._y == possibleSmallest._y) {
								if(a.area() <= possibleSmallest.area()) {
									return false;
								}
								else {
									return true;
								}
							}
							else {
								return false;
							}
						});
		aabbList.erase(remIt, aabbList.end());
	}

	size_t removeCount = startCount - aabbList.size();
	KE_Notify("[removedRedundantAABBs] previous: %u | now: %u | removed: %u",
			startCount, aabbList.size(), removeCount);

}

namespace eth {

std::list<AABB>::iterator Binpacking::findProperAABB(Texture& tex) {
	auto it = _aabbList.begin();
	for (; it != _aabbList.end(); ++it) {
		if (tex._info._height <= it->_height
				&& tex._info._width <= it->_width) {
			return it;
		}
	}
	// none found, look for unrestricted and try to adjust
	it = _aabbList.begin();
	for (; it != _aabbList.end(); ++it) {
		bool A, B;
		float newHeight = it->_height, newWidth = it->_width;
		if ((A=false)//(!it->_heightRestriction && tex._info._height <= it->_height))
				|| (B=(!it->_heightRestriction && tex._info._height > it->_height))) { // height modifiable
//			float tmp = it->_height;
//			it->_height = tex._info._height;
//			packageAABB._height += it->_height - tmp;
			newHeight = tex._info._height;
			KE_Notify("resize height (%s, %s)", A?"true":"false", B?"true":"false");
		}
		if ((A=false)//(!it->_widthRestriction && tex._info._width <= it->_width))
				|| (B=(!it->_widthRestriction && tex._info._width > it->_width))) { // width modifiable
//			float tmp = it->_width;
//			it->_width = tex._info._width;
//			packageAABB._width += it->_width - tmp;
			newWidth = tex._info._width;
			KE_Notify("resize width (%s, %s)", A?"true":"false", B?"true":"false");
		} // TODO: unresize if does not match; first determinate if resize will fit
		if (tex._info._height <= newHeight && tex._info._width <= newWidth) {
			float sumWidth = newWidth - it->_width;
			float sumHeight = newHeight - it->_height;
			KE_Notify("Resized by (%3.2f, %3.2f) from (%3.2f, %3.2f) to (%3.2f, %3.2f)", sumWidth, sumHeight, packageAABB._width, packageAABB._height, packageAABB._width+sumWidth, packageAABB._height+sumHeight);
			packageAABB._width += sumWidth;
			packageAABB._height += sumHeight;

			it->_height = newHeight;
			it->_width = newWidth;
			break;
		}
	}

	return it;
}

PackedBin* Binpacking::binpack(TextureList& texList) {
	texList.sort();
	_aabbList.clear();
	PackedBin* bin = new PackedBin { };
	packageAABB._height = packageAABB._width = 0;

	auto it = texList._textures.begin();
	if (it != texList._textures.end()) {
		AABB defaultAABB;
		packageAABB._x = packageAABB._y = defaultAABB._x = defaultAABB._y = 0;
		packageAABB._width = defaultAABB._width = 0;
		packageAABB._height = defaultAABB._height = 0;
		defaultAABB._heightRestriction = defaultAABB._widthRestriction = false;

		_aabbList.push_back(defaultAABB);

		for (; it != texList._textures.end(); ++it) {
			KE_Notify("Atlassing %s", it->_info._path.c_str());
			std::list<AABB>::iterator aabbIt = findProperAABB(*it);
			if (aabbIt == _aabbList.end()) {
				KE_CustomError(
						"Something went incredible wrong, check code here..");
				return nullptr;
			}

			bin->placeTextureAt(*it, *aabbIt);
			AABB& currentAABB = *aabbIt;

			TextureInfo& info = it->_info;
			if (info._height < info._width) {
				AABB downAABB;
				downAABB._x = currentAABB._x;
				downAABB._y = currentAABB._y + info._height;
				downAABB._height = currentAABB._height - info._height;
				downAABB._width = currentAABB._width;
				downAABB._heightRestriction = currentAABB._heightRestriction;
				downAABB._widthRestriction = currentAABB._widthRestriction;
				downAABB._age = currentAABB._age + 1;
				_aabbList.push_back(downAABB);

				AABB rightAABB;
				rightAABB._x = currentAABB._x + info._width;
				rightAABB._y = currentAABB._y;
				rightAABB._width = currentAABB._width - info._width;
				rightAABB._height = info._height;
				rightAABB._heightRestriction = true;
				rightAABB._widthRestriction = currentAABB._widthRestriction;
				rightAABB._age = currentAABB._age + 1;
				_aabbList.push_back(rightAABB);
			} else { // height >= width
				AABB downAABB;
				downAABB._x = currentAABB._x;
				downAABB._y = currentAABB._y + info._height;
				downAABB._width = info._width;
				downAABB._height = currentAABB._height - info._height;
				downAABB._heightRestriction = currentAABB._heightRestriction;
				downAABB._widthRestriction = true;
				downAABB._age = currentAABB._age + 1;
				_aabbList.push_back(downAABB);

				AABB rightAABB;
				rightAABB._x = currentAABB._x + info._width;
				rightAABB._y = currentAABB._y;
				rightAABB._width = currentAABB._width - info._width;
				rightAABB._height = currentAABB._height;
				rightAABB._heightRestriction = currentAABB._heightRestriction;
				rightAABB._widthRestriction = currentAABB._widthRestriction;
				rightAABB._age = currentAABB._age + 1;
				_aabbList.push_back(rightAABB);
			}
			currentAABB._height = info._height;
			currentAABB._width = info._width;
			_aabbList.erase(aabbIt);
			//			removeRedundantAABB(_aabbList);
			_aabbList.sort(compareAABBArea);
		}
	}
	bin->packageAABB = packageAABB;
	return bin;
}

void PackedBin::placeTextureAt(Texture& tex, AABB& aabb) {
	Package pkg;
	pkg._texture = tex;
	pkg._aabb = aabb;
	pkg._aabb._height = tex._info._height;
	pkg._aabb._width = tex._info._width;

	this->_packages.push_back(pkg);
}

void drawAABB(AABB& aabb, const glm::vec4& color/* = glm::vec4(1, 0, 0, 1)*/) {
	glBindTexture(GL_TEXTURE_2D, 0);
	glLoadIdentity();
	glTranslatef(aabb._x, aabb._y, 0);
	glColor4f(color.r, color.g, color.b, color.a);
	glBegin(GL_LINE_LOOP);
	{
		glVertex2f(0, 0);
		glVertex2f(0, aabb._height);
		glVertex2f(aabb._width, aabb._height);
		glVertex2f(aabb._width, 0);
	}
	glEnd();
	glColor4f(1, 1, 1, 1);
	glBindTexture(GL_TEXTURE_2D, 0);
}


void generateAtlas(PackedBin& pkgbin, TextureAtlas& outAtlas) {
	/**
	 * Generate fbo with single texture & depth+stencil buffer here
	 */
	glGenTextures(1, &outAtlas._renderTarget.textureId);
	glBindTexture(GL_TEXTURE_2D, outAtlas._renderTarget.textureId);
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, pkgbin.packageAABB._width, pkgbin.packageAABB._height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glBindTexture(GL_TEXTURE_2D, 0);


	glGenRenderbuffers(1, &outAtlas._renderTarget.depthStencilId);
	glBindRenderbuffer(GL_RENDERBUFFER, outAtlas._renderTarget.depthStencilId);
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, pkgbin.packageAABB._width, pkgbin.packageAABB._height);
	}

	glGenFramebuffers(1, &outAtlas._renderTarget.fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, outAtlas._renderTarget.fboId);
	{

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, outAtlas._renderTarget.textureId, 0);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
		GL_RENDERBUFFER, outAtlas._renderTarget.depthStencilId);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		KE_Assert(status == GL_FRAMEBUFFER_COMPLETE, "%s",
				glewGetErrorString(status));

		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	const float atlasWidth = pkgbin.packageAABB._width;
	const float atlasWidthFactor = 1.0f/atlasWidth;
	const float atlasHeight = pkgbin.packageAABB._height;
	const float atlasHeightFactor = 1.0f/atlasHeight;

	const GLenum drawBuffers[] = {
		GL_COLOR_ATTACHMENT0 };


	glBindFramebuffer(GL_FRAMEBUFFER, outAtlas._renderTarget.fboId);
	glActiveTexture(GL_TEXTURE0);
	glDrawBuffers(1, &drawBuffers[0]);
	{
		glClear(GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, atlasWidth, atlasHeight);

		glm::mat4 orthoMat = glm::ortho<float>(0, atlasWidth, atlasHeight, 0, -1, 1000);

		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(glm::value_ptr(orthoMat));
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();


		auto it = pkgbin._packages.begin();
		for(;it!=pkgbin._packages.end();++it) {
			UVCoordinates uv;
			uv._s = it->_aabb._x * atlasWidthFactor;
			uv._t = it->_aabb._y * atlasHeightFactor;
			uv._w = it->_aabb._width * atlasWidthFactor;
			uv._h = it->_aabb._height * atlasHeightFactor;

			size_t dirIdx = it->_texture._info._path.find_last_of("/\\");
			if(dirIdx!=std::string::npos) {
				KE_Notify("Shortening hashmap id to: %s", it->_texture._info._path.substr(dirIdx+1).c_str());
				std::string s = it->_texture._info._path.substr(dirIdx+1);
				char* str = new char[s.length()+1];
				strcpy(str, s.c_str());
				outAtlas._uvMap[str] = uv;
			}
			else {
				KE_Notify("No dir given, using full path %s", it->_texture._info._path.c_str());
				outAtlas._uvMap[it->_texture._info._path.c_str()] = uv;
			}


			drawTextureAt(it->_texture, glm::vec2(it->_aabb._x, it->_aabb._y));

		}

	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	outAtlas._tex._texId = outAtlas._renderTarget.textureId;
	outAtlas._tex._info._internalFormat = GL_RGBA8;
	outAtlas._tex._info._internalType = GL_UNSIGNED_BYTE;
	outAtlas._tex._info._width = atlasWidth;
	outAtlas._tex._info._height = atlasHeight;
}

void drawPackedBin(PackedBin& pkgbin) {
	AABB screen;
	auto it = pkgbin._packages.begin();
	drawAABB(pkgbin.packageAABB, glm::vec4(0, 1, 0, 1));
	for (; it != pkgbin._packages.end(); ++it) {
		drawAABB(it->_aabb);
		drawTextureAt(it->_texture, glm::vec2(it->_aabb._x, it->_aabb._y));
	}
}


//TODO: parameter control about maxWidth, Height
//TODO: output if texture atlas is full
void Binpacking::prepare(TextureList* texList) {
	this->_texList = texList;
	this->_texList->sort();
	_packedBin = new PackedBin { };
	packageAABB._height = packageAABB._width = 0;

	auto it = _texList->_textures.begin();
	if (it != _texList->_textures.end()) {
		AABB defaultAABB;
		packageAABB._x = packageAABB._y = defaultAABB._x = defaultAABB._y = 0;
		packageAABB._width = defaultAABB._width = 800;//it->_info._width *2;
		packageAABB._height = defaultAABB._height = 600;//it->_info._height *2;
		defaultAABB._heightRestriction = defaultAABB._widthRestriction = true;
		_aabbList.push_back(defaultAABB);

		stepIt = _texList->_textures.begin();

		_status = Prepared;
	}

}

bool Binpacking::step() {
	if (_status == RequiresPrepare) {
		KE_Notify("Failed to step - binpacker not prepared");
		return false;
	}

	KE_Notify("Atlassing %s", stepIt->_info._path.c_str());
	std::list<AABB>::iterator aabbIt = findProperAABB(*stepIt);
	if (aabbIt == _aabbList.end()) {
		KE_CustomError("Something went incredible wrong, check code here..");
		return nullptr;
	}

	_packedBin->placeTextureAt(*stepIt, *aabbIt);
	AABB& currentAABB = *aabbIt;

	TextureInfo& info = stepIt->_info;
	if (info._height < info._width) {
		AABB downAABB;
		downAABB._x = currentAABB._x;
		downAABB._y = currentAABB._y + info._height;
		downAABB._height = currentAABB._height - info._height;
		downAABB._width = currentAABB._width;
		downAABB._heightRestriction = currentAABB._heightRestriction;
		downAABB._widthRestriction = currentAABB._widthRestriction;
		downAABB._age = currentAABB._age + 1;
		_aabbList.push_back(downAABB);

		AABB rightAABB;
		rightAABB._x = currentAABB._x + info._width;
		rightAABB._y = currentAABB._y;
		rightAABB._width = currentAABB._width - info._width;
		rightAABB._height = info._height;
		rightAABB._heightRestriction = true;
		rightAABB._widthRestriction = currentAABB._widthRestriction;
		rightAABB._age = currentAABB._age + 1;
		_aabbList.push_back(rightAABB);

		KE_Notify("New aabbs according to height<width");
	} else { // height >= width
		AABB downAABB;
		downAABB._x = currentAABB._x;
		downAABB._y = currentAABB._y + info._height;
		downAABB._width = info._width;
		downAABB._height = currentAABB._height - info._height;

		downAABB._heightRestriction = currentAABB._heightRestriction;
		downAABB._widthRestriction = true;
		downAABB._age = currentAABB._age + 1;
		_aabbList.push_back(downAABB);

		AABB rightAABB;
		rightAABB._x = currentAABB._x + info._width;
		rightAABB._y = currentAABB._y;
		rightAABB._width = currentAABB._width - info._width;
		rightAABB._height = currentAABB._height;
		rightAABB._heightRestriction = currentAABB._heightRestriction;
		rightAABB._widthRestriction = currentAABB._widthRestriction;
		rightAABB._age = currentAABB._age + 1;
		_aabbList.push_back(rightAABB);
		KE_Notify("New aabbs according to height>=width");
	}
	_aabbList.erase(aabbIt);
	//			removeRedundantAABB(_aabbList);
	_aabbList.sort(compareAABBArea);
	_packedBin->packageAABB = packageAABB;

	++stepIt;
	if(stepIt==_texList->_textures.end()) {
		KE_Notify("Step finished - binpacker done.");
		_status = Done;
		return false;
	}


	return true;
}

void Binpacking::drawAABBList() {
	auto it = _aabbList.begin();
	for (; it != _aabbList.end(); ++it) {
		drawAABB(*it, glm::vec4(0,0,1,1));

	}
}

} /* namespace eth */

