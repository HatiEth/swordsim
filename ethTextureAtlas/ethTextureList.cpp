/*
 * ethTextureList.cpp
 *
 *  Created on: Feb 3, 2014
 *      Author: Hati
 */

#include "ethTextureList.h"

/**
 * Compares area required by texture
 * @return
 */
bool compareTextureArea(const eth::Texture& tex1, const eth::Texture& tex2) {
//	return (tex1._info._height>tex2._info._height) && (tex1._info._width>tex2._info._width); // biggest first
	const float tex1Area = (tex1._info._height*tex1._info._height ) + (tex1._info._width*tex1._info._width);
	const float tex2Area = (tex2._info._height*tex2._info._height ) + (tex2._info._width*tex2._info._width);
	return tex1Area>tex2Area; // biggest first
}

namespace eth {
void TextureList::addTexture(Texture& tex) {
	this->_textures.push_back(tex);
}
void eth::TextureList::sort() {
	this->_textures.sort(compareTextureArea);
}

} /* namespace eth */

