/*
 * ethTextureAtlas.cpp
 *
 *  Created on: Feb 3, 2014
 *      Author: Hati
 */

#include "ethTextureAtlas.h"
#include "ethTexture.h"
#include <fstream>

void loadMetafromFile(const char* metaPath, eth::TextureAtlas& atlas) {


}

std::ostream& operator<< (std::ostream& ostr, const eth::UVCoordinates& uv) {
//	ostr.write(reinterpret_cast<const char*>(&uv), sizeof(eth::UVCoordinates));
	ostr << uv._s << " " << uv._t << " " << uv._w << " " << uv._h;



	return ostr;
}


void saveMetaToFile(const eth::TextureAtlas& atlas, const char* metaPath) {
	std::ofstream ostr;
	ostr.open(metaPath, std::ofstream::binary); //|std::ofstream::out

	if(!ostr.is_open()) {
		KE_CustomError("Cannot open %s to write metafile, please try again!", metaPath);
		return;
	}

	auto it = atlas._uvMap.begin();
	for(;it!=atlas._uvMap.end();++it) {
		ostr << it->first << " " << it->second << "\n";
	}

	ostr.flush();

	ostr.close();
}

namespace eth {

void loadTextureAtlas(const char* texPath, const char* metaPath, TextureAtlas& atlas) {
	loadTextureFromPNG(texPath, atlas._tex);

	loadMetafromFile(metaPath, atlas);
}

void saveTextureAtlas(const TextureAtlas& atlas, const char* fileName) {
	const std::string fN = std::string(fileName);
	std::string pngPath = fN + ".png";
	std::string metaPath = fN + ".dtam";

//	saveAtlasToPNG(atlas, pngPath.c_str());
	saveAtlasToPNG(atlas, pngPath.c_str());


	saveMetaToFile(atlas, metaPath.c_str());
}

} /* namespace eth */
