/*
 * ethTextureAtlas.h
 *
 *  Created on: Feb 3, 2014
 *      Author: Hati
 */

#ifndef ETHTEXTUREATLAS_H_
#define ETHTEXTUREATLAS_H_

#include "ethTexture.h"
#include <Utility.h>

namespace eth {

struct UVCoordinates {
	float _s, _t; ///< top left uv-coordinates
	float _w, _h; ///< dimension uv mapped
};


struct SingleTextureFBO {
	GLuint fboId;
	GLuint textureId;
	GLuint depthStencilId;
};

struct TextureAtlas {
	Texture _tex;
	using UVMap = StringMap<UVCoordinates>::Type;
	UVMap _uvMap;
	SingleTextureFBO _renderTarget;
};


void loadTextureAtlas(const char* texPath, const char* metaPath, TextureAtlas& atlas);
void saveTextureAtlas(const TextureAtlas& atlas, const char* fileName); ///< creates png with fileName and metafile with <fileName>.dtam




} /* namespace eth */

#endif /* ETHTEXTUREATLAS_H_ */
