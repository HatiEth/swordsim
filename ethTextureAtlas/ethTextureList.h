/*
 * ethTextureList.h
 *
 *  Created on: Feb 2, 2014
 *      Author: Hati
 */

#ifndef ETHTEXTURELIST_H_
#define ETHTEXTURELIST_H_

#include "ethTexture.h"
#include <Utility.h>
#include <list>

namespace eth {
struct TextureList {
	std::list<eth::Texture> _textures;

	void addTexture(eth::Texture& tex);
	void sort();
};



}
/* namespace eth */


#endif /* ETHTEXTUREATLAS_H_ */
