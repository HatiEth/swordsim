/*
 * ethTexture.h
 *
 *  Created on: Feb 2, 2014
 *      Author: Hati
 */

#ifndef ETHTEXTURE_H_
#define ETHTEXTURE_H_

#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>


namespace eth {

struct TextureInfo {
	std::string _path;
	size_t _width, _height;
	GLint _internalFormat;
	GLint _internalType;
};

struct Texture {
	GLuint _texId;
	TextureInfo _info;
};

///@todo fix textures are only synonym for GLuint
struct MetaTexture {
	Texture _texture;
	TextureInfo _info;
};



void loadTextureFromPNG(const char* path, Texture& tex);

struct TextureAtlas;
void saveAtlasToPNG(const TextureAtlas& atlas, const char* path);
void drawTextureAt(const Texture& tex, const glm::vec2& position);

} /* namespace eth */
#endif /* ETHTEXTURE_H_ */
