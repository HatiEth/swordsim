/*
 * ethTexture.cpp
 *
 *  Created on: Feb 2, 2014
 *      Author: Hati
 */

#include "ethTexture.h"
#include <libpng/png.h>
#include <zlib/zlib.h>
#include <Utility.h>
#include "ethTextureAtlas.h"

eth::Texture createTexture(const eth::TextureInfo& texInfo) {
	eth::Texture tex;
	tex._info = texInfo;

	glGenTextures(1, &tex._texId);
	glBindTexture(GL_TEXTURE_2D, tex._texId);
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, texInfo._internalFormat, texInfo._width,
				texInfo._height, 0,
				GL_RGBA, texInfo._internalType, NULL);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	return tex;
}

void setTextureData(eth::Texture& tex, char* texData) {
	KE_Assert(tex._texId != 0, "TextureId invalid!");
	glBindTexture(GL_TEXTURE_2D, tex._texId);
	glTexImage2D(GL_TEXTURE_2D, 0, tex._info._internalFormat, tex._info._width, tex._info._height, 0,
	GL_RGBA, tex._info._internalType, texData);
	glBindTexture(GL_TEXTURE_2D, 0);
}


namespace eth {



void loadTextureFromPNG(const char* path, Texture& texture) {
	png_image image;
	memset(&image, 0, sizeof(png_image));
	image.version = PNG_IMAGE_VERSION;
	int err = !png_image_begin_read_from_file(&image, path);


	if(err || image.warning_or_error != 0) {
		// error
		KE_CustomError("png_error: %s ", image.message);
	}

	image.format = PNG_FORMAT_RGBA; // encapsulate in external pod

	char* pngData = new char[PNG_IMAGE_SIZE(image)];

	err = !png_image_finish_read(&image, NULL, pngData, -PNG_IMAGE_ROW_STRIDE(image), NULL);
	if(err || image.warning_or_error != 0) {
		// error
		KE_CustomError("png_error: %s ", image.message);
	}

	// create texture here
	TextureInfo texInfo;
	texInfo._path = path;
	texInfo._width = image.width;
	texInfo._height = image.height;
	texInfo._internalFormat = GL_RGBA8;
	texInfo._internalType = GL_UNSIGNED_BYTE;

	texture = createTexture(texInfo);
	setTextureData(texture, pngData);

	delete [] pngData;
}

void saveAtlasToPNG(const TextureAtlas& atlas, const char* path) {

	png_image image;
	memset(&image, 0, sizeof(png_image));
	image.version = PNG_IMAGE_VERSION;
	image.width = atlas._tex._info._width;
	image.height = atlas._tex._info._height;
	image.format = PNG_FORMAT_RGBA;
	image.colormap_entries = 256;




	glBindFramebuffer(GL_FRAMEBUFFER, atlas._renderTarget.fboId);
	{
		char* imgData = new char[PNG_IMAGE_SIZE(image)];

	//	glGetTexImage(GL_TEXTURE_2D,  0,  tex._info._internalFormat,  tex._info._internalType,  (GLvoid*)imgData);
		glReadPixels(0,  0,  atlas._tex._info._width,  atlas._tex._info._height,  GL_RGBA,  atlas._tex._info._internalType,  imgData);

		int err = !png_image_write_to_file(&image, path, 0, imgData, -PNG_IMAGE_ROW_STRIDE(image), NULL);


		if(err || image.warning_or_error != 0) {
			KE_CustomError("png_error: %s ", image.message);
		}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void drawTextureAt(const Texture& tex, const glm::vec2& position) {
	glBindTexture(GL_TEXTURE_2D, tex._texId);
	glLoadIdentity();
	glTranslatef(position.x, position.y, 0);
	glBegin(GL_TRIANGLE_STRIP);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(0, tex._info._height);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(0, 0);
		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(tex._info._width, tex._info._height);
		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(tex._info._width, 0);
	}
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}

} /* namespace eth */


